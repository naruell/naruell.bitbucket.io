class lifeworld{
  constructor(worldSize)
  {
    this.size = worldSize;
    this.X = width/this.size;
    this.Y = height/this.size;
    this.world = new Array(this.X);
    
    // State 0:Stop, 1:running, 2:editing mode
    this.State = 0;
    
    for(let i = 0; i < this.X; i++)
    {
      this.world[i] = new Array((height-InterfaceSize)/this.size);
    }
    
    this.after = new Array(this.X);
    for(let i = 0; i < this.X; i++)
    {
      this.after[i] = new Array((height-InterfaceSize)/this.size);
    }
  }
  
  generate(mode)
  {
    if(mode == 'R')
    {
      for(let i = 0; i < this.world.length; i++)
      {
        for(let j = 0; j < this.world[0].length; j++)
        {
          if(i==0 || j==0 || i==this.world.length-1 || j==this.world[0].length-1) 
            this.world[i][j]=0;
          else 
            this.world[i][j] = round(random(0,1));
        }
      }
    }
    else
    {
      for(let i=0; i<this.world.length; i++)
      {
        for(let j=0; j<this.world[0].length; j++)
        {
          if(i==0 || j==0 || i==this.world.length-1 || j==this.world[0].length-1) 
          {
            this.world[i][j]=0;
            this.after[i][j]=0;
          }
          else 
          {
            this.world[i][j]=0;
            this.after[i][j]=0;
          }
        }
      }
    }
  }
  
  drawcell()
  {
    push();
    stroke(120);
    strokeWeight(2);
    for(let i=1; i<this.world.length-1; i++)
    {
      for(let j=1; j<this.world[0].length-1; j++)
      {
        let realX = i*this.size;
        let realY = j*this.size;
        if(this.world[i][j] == 1)
        {
          fill(0);
          square(realX,realY,this.size);
        }
        else 
        {
          fill(255);
          square(realX,realY,this.size);
        }
      }
    }
    pop();
  }
  
  lifecheck()
  {
    for(let i=1; i<this.world.length-1; i++)
    {
      for(let j=1; j<this.world[0].length-1; j++)
      {
        let near = 0;
        for(let k=-1; k<2; k++)
        {
          for(let l=-1; l<2; l++)
          {
            if(k==0 && l==0) {}
            else if(this.world[i+k][j+l] == 1) {near++}
          }
        }
        if(this.world[i][j] == 1)
        {
          if(near == 2 || near == 3) {this.after[i][j] = 1}
          else {this.after[i][j] = 0}
        }
        else if(this.world[i][j] == 0)
        {
          if(near == 3) {this.after[i][j] = 1}
          else {this.after[i][j] = 0}
        }
      }
    }
    let change = this.world;
    this.world = this.after;
    this.after = change; 
  }
  
  state_check()
  {
    switch(this.State)
    {
      case 0: frameRate(int(frame)); this.drawcell(); break;
      case 1: frameRate(int(frame)); this.lifecheck(); this.drawcell(); break;
      case 2: frameRate(60); this.drawcell(); this.editmode(); break;
    }
  }
 
  editmode()
  {
    let MX = floor(mouseX / this.size);
    let MY = floor(mouseY / this.size);
  
    push();
    fill(250,0,0,170);
    textSize(50);
    text("<< EDIT MODE >>",20,50);
    text("Left click : Make Cell",20,100);
    text("Center click : Remove Cell",20,150);
    pop();
  
    if(mouseIsPressed && mouseButton === LEFT) 
    {
      if(MX >= 1 && MX <= this.X-1) {this.world[MX][MY] =1;}        
    }
    else if(mouseIsPressed && mouseButton === CENTER)
    {
      if(MX >= 1 && MX <= this.X-1) {this.world[MX][MY] =0;}
    }
  }
  
  drawpattern(patterns,mode)
  {
    let pnum = patterns.length;
    let rand = floor(random(pnum));
    let x = patterns[rand].s[0];
    let y = patterns[rand].s[1];
  
    let MX = floor(mouseX / this.size);
    let MY = floor(mouseY / this.size);
    
    let time = 0;
    
    if(mode == 'M')
    {
      for(let i = MX; i < x+MX; i++)
      {
        for(let j = MY; j < y+MY; j++)
        {
          this.world[i][j] = patterns[rand].p[time];
          time++;
        }
      }
    }
    
    else if(mode == 'R')
    {
      let RX = floor(random(0,this.world.length));
      let RY = floor(random(0,this.world[0].length));
      
      for(let i = RX; i < x+RX; i++)
      {
        for(let j = RY; j < y+RY; j++)
        {
          this.world[i][j] = patterns[rand].p[time];
          time++;
        }
      }
    }
  }
}