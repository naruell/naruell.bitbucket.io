// written by Duhwan.Kim
// Assignment Game of Life
// Course CS099
// Spring 2019

let Life;
const InterfaceSize = 140;
const start = 20;

let frame = 10;

function setup() {
  createCanvas(1200, 800);
  Life = new lifeworld(20);
  Life.generate('R');
}

function draw() {
  background(220);
  interface();
  Life.state_check();
}

// Show buttons under the world
function interface()
{
  Cy = height-InterfaceSize/2;
  push();
  rectMode(CENTER);
  textAlign(CENTER);
  textSize(28);
  
  // start
  fill('green');
  rect(100,Cy,120,80);
  fill(0);
  text('Start\n(key S)',100,Cy);
  
  
  // pause
  fill('red');
  rect(300,Cy,120,80);
  fill(0);
  text('Pause\n(key P)',300,Cy);
  
  
  // edit
  fill(255);
  rect(500,Cy,120,80);
  fill(0);
  text('Edit\n(key E)',500,Cy);
  
  // clear
  fill(255);
  rect(650,Cy,120,80);
  fill(0);
  text('Clear\n(key C)',650,Cy);
  
  // randon
  fill(255);
  rect(800,Cy,120,80);
  fill(0);
  text('Randon\n(key R)',800,Cy);
  
  // randon
  fill(255,255,0);
  rect(950,Cy,120,80);
  fill(0);
  text('Frame\nSet',950,Cy);
  text("frame:"+floor(frameRate()),width-80,height-120);
  pop();
}

// change states if button was clicked
function mouseReleased()
{
  if(mouseX>=40 && mouseX<=160 && mouseY>=Cy-40 && mouseY<=Cy+40) {Life.State = 1;}
  if(mouseX>=240 && mouseX<=360 && mouseY>=Cy-40 && mouseY<=Cy+40) {Life.State = 0;}
  if(mouseX>=440 && mouseX<=560 && mouseY>=Cy-40 && mouseY<=Cy+40) {Life.State = 2;}
  if(mouseX>=590 && mouseX<=710 && mouseY>=Cy-40 && mouseY<=Cy+40) {Life.generate();}
  if(mouseX>=740 && mouseX<=860 && mouseY>=Cy-40 && mouseY<=Cy+40) {Life.generate('R');}
  if(mouseX>=890 && mouseX<=1010 && mouseY>=Cy-40 && mouseY<=Cy+40){frame = prompt("Input Number to Setting Frame!"); frameRate(int(frame));} 
  if(Life.State != 2 && mouseButton === LEFT) {Life.drawpattern(Oscillators,'M')}
  if(Life.State != 2 && mouseButton === CENTER) {Life.drawpattern(Spaceships,'M')}
}

function keyPressed()
{
  if(key == 's' || key == 'S') {Life.State = 1;}
  if(key == 'p' || key == 'P') {Life.State = 0;}
  if(key == 'e' || key == 'E') {Life.State = 2;}
  if(key == 'r' || key == 'R') {Life.generate('R');}
  if(key == 'c' || key == 'C') {Life.generate();}
  if(key == ' ') {Life.drawpattern(Still_Lifes,'R')}
}
