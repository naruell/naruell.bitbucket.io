// wriiten by Duhwan.Kim
// Assignment Create a Vec2 Class
// Course CS099
// Spring 2019

const s = 2;
const s2 = 3;

let V1_vector;
let V2_vector;
let A_vector;

let V3_vector;
let V4_vector;
let B_vector;

let Mouse_vector;

function setup() {
  createCanvas(600, 400);
  textSize(15);
  fill(255);
  strokeWeight(2);
  stroke('gray');
  
  V1_vector = new Vec2(50,-10);
  V1_vector.multiplyBy(s);
  V2_vector = new Vec2(20,-50);
  A_vector = V1_vector.add(V2_vector);

  V3_vector = new Vec2(100,-190);
  V3_vector.divideBy(s2);
  V4_vector = new Vec2(20,40);
  B_vector = V3_vector.subtract(V4_vector);
  
  Mouse_vector = new Vec2(0,0);
}

function draw() {
  background(0);
  
  // Question Num.1
  text("Question Num.1",30,150-130);
  drawGrid(30,150,120);
  drawVector(30+V1_vector.x,150+ V1_vector.y,V2_vector,'Pos','blue');
  drawVector(30,150,V1_vector,'Pos','red');
  drawVector(30,150,A_vector,'Pos','gray');
  push();
  stroke('red');
  text("s : "+s,30,180);
  text("Length : "+nf(V1_vector.getLength(),0,2),30,200);
  text("Radian Angle : "+nf(-V1_vector.getAngle(),0,2),30,220);
  
  stroke('blue');
  text("Length : "+nf(V2_vector.getLength(),0,2),30,260);
  text("Radian Angle : "+nf(-V2_vector.getAngle(),0,2),30,280);
  
  stroke('gray');
  text("Length : "+nf(-A_vector.getLength(),0,2),30,320);
  text("Radian Angle : "+nf(-A_vector.getAngle(),0,2),30,340);
  pop();
  
  // Question Num.2
  text("Question Num.2",230,150-130);
  drawGrid(230,150,120);
  drawVector(230+V3_vector.x,150+ V3_vector.y,V4_vector,'Neg','blue');
  text("original v3 vector",280,110);
  drawVector(230+V3_vector.x,150+ V3_vector.y,V4_vector,'Pos','skyblue');
  text("-v3 vector",260,70);
  drawVector(230,150,V3_vector,'Pos','red');
  drawVector(230,150,B_vector,'Pos','gray');
  push();
  stroke('red');
  text("1/s : "+"1/"+s2,230,180);
  text("Length : "+nf(V3_vector.getLength(),0,2),230,200);
  text("Radian Angle : "+nf(-V3_vector.getAngle(),0,2),230,220);
  
  stroke('blue');
  text("Length : "+nf(V4_vector.getLength(),0,2),230,260);
  text("Radian Angle : "+nf(PI-V4_vector.getAngle(),0,2),230,280);
  
  stroke('gray');
  text("Length : "+nf(B_vector.getLength(),0,2),230,320);
  text("Radian Angle : "+nf(-B_vector.getAngle(),0,2),230,340);
  pop();
  
  
  // Question Num.3
  text("Question Num.3",430,150-130);
  drawGrid(430,150,120);
  let Xangle = map(mouseX,0,width,0,2*PI);
  let Ylength = map(mouseY,0,height,0,sqrt(pow(width,2)+pow(height,2)));
  push(); noFill(); strokeWeight(0.8); arc(430,150,30,30,Xangle,0); pop();
  Mouse_vector.setAngle(Xangle);
  Mouse_vector.setLength(Ylength);
  drawVector(430,150,Mouse_vector,'Pos','green');
  push();
  stroke('green');
  text("Length : "+nf(Mouse_vector.getLength(),0,2),430,200);
  text("Radian Angle : "+nf(-Mouse_vector.getAngle(),0,2),430,220);

  pop();
  
}
