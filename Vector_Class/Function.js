function drawVector(tranX,tranY,Vec,Dir,color)
{
  push();
  let InpVecX = Vec.x;
  let InpVecY = Vec.y;
  let InpVecA = Vec.getAngle();
  
  if(Dir == 'Neg')
  {
    InpVecX *= -1;
    InpVecY *= -1;
    InpVecA += PI;
  }
  
  strokeWeight(1);
  stroke(color);
  fill(color);
  
  // Arrow
  translate(tranX,tranY);
  line(0,0,InpVecX,InpVecY);
  
  // Arrow Head
  translate(InpVecX,InpVecY);
  rotate(InpVecA);
  beginShape();
  vertex(0,0);
  vertex(-8,-3);
  vertex(-8,3);
  endShape(CLOSE);
  pop();
  
  push();
  translate(tranX,tranY);
  fill(255);
  stroke(color);
  strokeWeight(1.4);
  circle(0,0,3);
  pop();
}

function drawGrid(x,y,l)
{
  push();
  stroke(255);
  fill(0);
  line(x,y, x,y-l); line(x,y, x+l,y);
  text('x',x+l-10,y+10); text('y',x-10,y-l+10);
  pop();
}