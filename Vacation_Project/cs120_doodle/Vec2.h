#pragma once
struct Vec2 {
	float x, y;

	float getAngle();
	void setAngle(float angle);

	float getLength();
	void setLength(float length);

	Vec2 get();

	Vec2 operator+(Vec2 v);
	Vec2 operator-(Vec2 v);
	Vec2 operator*(float scalar);
	Vec2 operator/(float scalar);
	Vec2& operator+=(Vec2 v);
	Vec2& operator-=(Vec2 v);
	Vec2& operator*=(float scalar);
	Vec2& operator/=(float scalar);

	float dot(Vec2 v);
};

float getDistanceBetweenDots(Vec2 v1, Vec2 v2);
float getDistanceBetweenDotsSquared(Vec2 v1, Vec2 v2);
float getDistanceBetweenDotAndLineSegment(Vec2 p, Vec2 v, Vec2 w);
float getDistanceBetweenDotAndLine(Vec2 p, Vec2 v, Vec2 w);