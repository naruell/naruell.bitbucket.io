#pragma once

/*
 * Particle, Effect, System 틀 잡기
 * setup, update, draw, run 개념, 틀 잡기
 * 간단한 분수 이펙트 만들기
 * 복잡한 멤버 추가하기(질량, 자력 등등), 확장성의 폭 넓히기
 * time related varialbe 넣기
 * update, setup 통째로 수정하기 (pshowedTime, showedTime, duration 설정, 업데이트하기)
 * 모든 파티클 소멸 여부를 수명(duration)과 endCondtion으로 통일하기
 * 모든 시간 관련 함수를 doodle::ElapsedTime 로 통일하기(나중에 시간 보정치를 추가해야됨)
 * 클릭 이펙트 만들기 (섀버 클릭 이펙트)
 * ImageConvolution 참고해서 이미지 파티클화하기
 * n픽셀 단위로 파티클 바꾸기 (1x1 픽셀, 2x2 픽셀, 3x3 픽셀)
 * 이미지 파티클 흩날리기
 * 프레임 단위로 돌아가는 모든 이펙트 바꾸기 (Delta time = pshowedTime - showedTime 로 통일하기)
 * 파티클 멤버 재정리하기
 * >> Effect Interface 추가하기
 *	ㄴ Particles 벡터를 부모 클래스에서 통일하기
 * 120 * (showedTime - pshowedTime) 수정하기
 * Time Line 추가하기
 * generateXXX 정리하기
 * Effect System 에 외력 추가하기
 */