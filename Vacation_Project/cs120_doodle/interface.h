#pragma once
#include <doodle/drawing.hpp>
#include "ParticleSystem.h"
#include "testEffect.h"
#include "click1Effect.h"
#include "image1Effect.h"

class InfoInterface
{
public:
	void run();
	void setup();
	void update();
	void draw();

	void setEffectType(std::string EffectTo);
	std::string getEffectType();

private:
	bool isSetup{ false };

	float positionX{ 0 };
	float positionY{ 0 };
	float fontSize{ 20.0f };

	std::string curEffectType{ "TestEffect" };
	int lastEffectIndex{ -1 };
	int EffectCount{ 0 };
	int ParticleCount{ 0 };
};

extern InfoInterface infoInterface;

/* 
 * InfoInterface 의 기능
 * 현재 파티클 종류, 실행되고 있는(alive한) 이펙트 갯수, 실행되고 있는(alive한) 파티클 갯수 표시
 * 이펙트 변경(특정 키를 눌러서), 초기화, 최근 이펙트 삭제(되돌리기)
 * 배경 색상 또는 이미지 변경
 */

// generateEffect 할 때 lastEffectIndex 업데이트하기