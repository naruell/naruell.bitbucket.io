#pragma once
#include "ParticleSystem.h"

struct TestParticle_circle : Particle
{
	virtual void setup() override;
	virtual void update() override;
	virtual void draw() override;

	TestParticle_circle(Vec2 pos) : Particle("TestParticle_circle", pos, Vec2{ doodle::random(-1.0f, 1.0f), doodle::random(0.0f, 1.0f) }, Vec2{ 0.0f, -0.05f }, 255.0f) {};
};

struct TestParticle_rect : Particle
{
	virtual void setup() override;
	virtual void update() override;
	virtual void draw() override;

	TestParticle_rect(Vec2 pos) : Particle("TestParticle_rect", pos, Vec2{ doodle::random(-1.0f, 1.0f), doodle::random(0.0f, 1.0f) }, Vec2{ 0.0f, -0.05f }, 255.0f) {};
};

class TestEffect : public Effect
{
public:
	virtual void generateParticle() override;
	virtual void setup() override;
	virtual void run() override;

	TestEffect(Vec2 origin) : Effect(origin) {};
	~TestEffect();
};