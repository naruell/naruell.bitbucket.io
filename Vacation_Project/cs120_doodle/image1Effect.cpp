#include <iostream>
#include <doodle/noise.hpp>
#include "image1Effect.h"

void pixelParticle::setup()
{
	generatedTime = doodle::ElapsedTime;
	noiseSeedX = static_cast<float>(doodle::random(0, 3000));
	noiseSeedY = static_cast<float>(doodle::random(0, 3000));
	isSetup = true;
}

void pixelParticle::update()
{
	if (!isSetup) { setup(); }
	pshowedTime = showedTime;
	showedTime = doodle::ElapsedTime - generatedTime;
	noiseSeedX += doodle::DeltaTime;
	noiseSeedY += doodle::DeltaTime;
	if (fillColor.alpha == 0) { endCondition = true; }
}

void pixelParticle::draw()
{
	if (subtype == "nullPixel") { return; }
	doodle::push_settings();

	/*int alpha = 255 - static_cast<int>(showedTime * 190.0f);
	if (alpha <= 0) { alpha = 0; }
	fillColor.alpha = static_cast<doodle::Color4ub::unsigned_byte>(alpha);*/

	doodle::set_fill_color(fillColor);
	doodle::no_outline();
	doodle::draw_rectangle(position.x, position.y, static_cast<float>(pixelSize), static_cast<float>(pixelSize));

	doodle::pop_settings();
}

void image1Effect::generateParticle()
{
	doodle::Image image{};
	if (const bool loaded = image.LoadFromPNG(imagePath); !loaded)
	{
		std::cerr << "failed to load " << imagePath << "\n";
		return;
	}

	imageWidth = image.GetWidth();
	imageHeight = image.GetHeight();
	const doodle::Color4ub* p_colors = image.GetPixelsPointer();
	for (int y = 0; y < imageHeight; y += pixelSize)
	{
		for (int x = 0; x < imageWidth; x += pixelSize)
		{
			if (p_colors[x + imageWidth * y].alpha == 0)
			{
				Particles.push_back(new pixelParticle());
			}
			else
			{
				Particles.push_back(new pixelParticle( x + imageWidth * y, Vec2{ origin.x + x, origin.y - y }, pixelSize ));
				Particles[Particles.size() - 1]->fillColor = p_colors[x + imageWidth * y];
			}
		}
	}
}

void image1Effect::setup()
{
	generatedTime = doodle::ElapsedTime;
	if (duration != -1) { endTime = generatedTime + duration; }
	generateParticle();
	EffectrangeX = static_cast<float>(imageWidth);
	EffectrangeY = 0;
	isSetup = true;
}

void image1Effect::run()
{
	if (!isSetup) { setup(); }
	pshowedTime = showedTime;
	showedTime = doodle::ElapsedTime - generatedTime;

	if (getParticlesSize() == 0)
	{
		endCondition = true;
	}

	EffectrangeX = map(showedTime, 8.0f, 10.2f, static_cast<float>(imageWidth), 0);	// 사라지게 하는 경계선

	for (Particle* p : Particles)
	{
		pixelParticle* pp = dynamic_cast<pixelParticle*>(p);
		if (pp && pp->particleIndex % imageWidth >= EffectrangeX)
		{
			pp->acceleration = Vec2{ 2, 2 };

			float n1 = doodle::noise(pp->noiseSeedX);
			float n2 = doodle::noise(pp->noiseSeedY);
			pp->position.x += map(n1, 0, 1, -1, 3) * 120 * (showedTime - pshowedTime);	// 흩날리는 방향
			pp->position.y += map(n2, 0, 1, -1, 2) * 120 * (showedTime - pshowedTime);

			float alpha = map(pp->particleIndex % imageWidth - EffectrangeX, 0.0f, 500.0f, 255, 0);
			if (alpha <= 0) { alpha = 0; }
			pp->fillColor.alpha = static_cast<doodle::Color4ub::unsigned_byte>(alpha);
		}
	}

	for (int i = static_cast<int>(Particles.size()) - 1; i >= 0; --i)
	{
		Particles[i]->run();

		if (Particles[i]->isEnded())
		{
			Particles.erase(Particles.begin() + i);
		}
	}
}

image1Effect::~image1Effect()
{
	for (Particle* p : Particles)
	{
		delete p;
	}
}