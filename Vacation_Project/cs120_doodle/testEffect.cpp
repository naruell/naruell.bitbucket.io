#include "testEffect.h"

void TestParticle_circle::setup()
{
	generatedTime = doodle::ElapsedTime;
	if (duration != -1) { endTime = generatedTime + duration; }

	fillColor = doodle::Color4ub{ 255, 0, 0, 255 };
	outlineColor = doodle::Color4ub{ 0, 0, 0, 255 };
	size_w = 12.0f;
	size_h = 12.0f;
	isSetup = true;
}

void TestParticle_circle::update()
{
	if (!isSetup) { setup(); }
	pshowedTime = showedTime;
	showedTime = doodle::ElapsedTime - generatedTime;
	velocity += acceleration * 120 * (showedTime - pshowedTime);
	position += velocity * 120 * (showedTime - pshowedTime);

	if (255 - showedTime * 190.0f <= 0) { endCondition = true; }
}

void TestParticle_circle::draw()
{
	doodle::push_settings();
	int alpha = 255 - static_cast<int>(showedTime * 190.0f);
	if (alpha <= 0) { alpha = 0; }

	outlineColor.alpha = static_cast<doodle::Color4ub::unsigned_byte>(alpha);
	fillColor.alpha = static_cast<doodle::Color4ub::unsigned_byte>(alpha);
	doodle::set_outline_color(outlineColor);
	doodle::set_fill_color(fillColor);

	doodle::draw_ellipse(position.x, position.y, size_w, size_h);
	doodle::pop_settings();
}

void TestParticle_rect::setup()
{
	generatedTime = doodle::ElapsedTime;
	if (duration != -1) { endTime = generatedTime + duration; }

	fillColor = doodle::Color4ub{ 0, 0, 255, 255 };
	outlineColor = doodle::Color4ub{ 0, 0, 0, 255 };
	size_w = 12.0f;
	size_h = 12.0f;
	isSetup = true;
}

void TestParticle_rect::update()
{
	if (!isSetup) { setup(); }
	pshowedTime = showedTime;
	showedTime = doodle::ElapsedTime - generatedTime;
	velocity += acceleration * 120 * (showedTime - pshowedTime);
	position += velocity * 120 * (showedTime - pshowedTime);

	if (255 - showedTime * 190.0f <= 0) { endCondition = true; }
}

void TestParticle_rect::draw()
{
	doodle::push_settings();
	doodle::set_rectangle_mode(doodle::RectMode::Center);

	int alpha = 255 - static_cast<int>(showedTime * 190.0f);
	if (alpha <= 0) { alpha = 0; }
	outlineColor.alpha = static_cast<doodle::Color4ub::unsigned_byte>(alpha);
	fillColor.alpha = static_cast<doodle::Color4ub::unsigned_byte>(alpha);

	doodle::set_outline_color(outlineColor);
	doodle::set_fill_color(fillColor);

	doodle::apply_translate(position.x, position.y);
	float theta = map(position.x, 0.0f, static_cast<float>(doodle::Width), 0.0f, doodle::TWO_PI * 2);
	doodle::apply_rotate(theta);
	doodle::draw_rectangle(0, 0, size_w, size_h);
	doodle::pop_settings();
}

void TestEffect::generateParticle()
{
	int particleSeed = doodle::random(0, 2);
	switch (particleSeed)
	{
	case 0:
		Particles.push_back(new TestParticle_circle(origin));
		break;

	case 1:
		Particles.push_back(new TestParticle_rect(origin));
		break;
	}
}

void TestEffect::setup()
{
	generatedTime = doodle::ElapsedTime;
	if (duration != -1) {endTime = generatedTime + duration;}

	isSetup = true;
}

void TestEffect::run()
{
	if (!isSetup) { setup(); }
	pshowedTime = showedTime;
	showedTime = doodle::ElapsedTime - generatedTime;

	for (int i = static_cast<int>(Particles.size()) - 1; i >= 0; --i)
	{
		Particles[i]->run();

		if (Particles[i]->isEnded())
		{
			Particles.erase(Particles.begin() + i);
		}
	}

	generateParticle();
}

TestEffect::~TestEffect()
{
	for (Particle* p : Particles)
	{
		delete p;
	}
}