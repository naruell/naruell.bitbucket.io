#pragma once
#include <string>
#include <vector>
#include <fstream>
#include <doodle/environment.hpp>
#include <doodle/angle.hpp>
#include <doodle/random.hpp>
#include <doodle/color.hpp>
#include <doodle/drawing.hpp>
#include "Vec2.h"
#include "Helper.h"

struct Particle
{
	/**
	 * \essential code guideline
	 * \code
	 *	void setup()
	 *	{
	 *		generatedTime = doodle::ElapsedTime;
	 *		if(duration != -1) {endTime = generatedTime + duration;}
	 *		
	 *		// setup code 
	 *	
	 *		isSetup = true;
	 *	}
	 */
	virtual void setup() = 0;
	
	/**
	 * \essential code guideline
	 * \code 
	 *	void update()
	 *	{
	 *		if (!isSetup) { setup(); }
	 *		pshowedTime = showedTime;
	 *		showedTime = doodle::ElapsedTime - generatedTime;
	 *	
	 *		// update code here
	 *		// If there are no time limit, then you have to set endCondtion true to remove particle
	 *	}
	 */
	virtual void update() = 0;

	/**
	 * \essential code guideline
	 * \code 
	 * void draw()
	 *	{
	 *		doodle::push_settings();
	 *	
	 *		// draw code here
	 *	
	 *		doodle::pop_settings();
	 *	}
	 */
	virtual void draw() = 0;
	void run();
	bool isEnded();	// 파티클이 사라져야 할 지 여부 ((재생시간 >= 수명) 혹은 (endCondition이 true 일 때))

	bool isSetup{ false };
	std::string subtype;

	Vec2 position;
	Vec2 velocity;
	Vec2 acceleration;

	float mass{ 1 };				// 질량
	bool isInfbyForce{ false };

	float magnetism{ 0 };			//자성
	float Ferromagnetic{ 0 };		// 강자성
	bool isInfbyMagnet{ false };

	float elasticity{ 1 };			// 탄성
	bool isInfbyCollision{ false };

	float generatedTime{ 0 };
	float endTime{ 0 };
	float showedTime{ 0 };
	float pshowedTime{ 0 };
	float duration{ -1 };
	bool endCondition{ false };

	float size_w{ 0 };
	float size_h{ 0 };
	doodle::Color4ub fillColor{0, 0, 0, 0};
	doodle::Color4ub outlineColor{0, 0, 0, 0};
	
	Particle(std::string subtype, Vec2 pos, Vec2 vel, Vec2 acc) : subtype(subtype), position(pos), velocity(vel), acceleration(acc) {};
	Particle(std::string subtype, Vec2 pos, Vec2 vel, Vec2 acc, float dur) : subtype(subtype), position(pos), velocity(vel), acceleration(acc), duration(dur) {};
};

class Effect
{
public:
	virtual void generateParticle() = 0;
	virtual void setup() = 0;
	virtual void run() = 0;

	bool isEnded();

	std::string getType();
	void setType(std::string typeTo);

	void remove();
	int getParticlesSize();

	Effect(Vec2 origin) : origin(origin) {}
	virtual ~Effect();

protected:
	Vec2 origin;
	std::vector<Particle*> Particles;	// Particle 구조체가 순수 가상이므로 포인터 값으로 받음.
	std::string type;

	bool isSetup{ false };

	float generatedTime{ 0 };
	float endTime{ 0 };
	float showedTime{ 0 };
	float pshowedTime{ 0 };
	float duration{ -1 };
	bool endCondition{ false };
};

class EffectSystem
{
public:
	void run();

	// 이펙트 생성 명령어
	void generateClick1Effect();
	void generateTestEffect(Vec2 pos);
	void generateImage1Effect(Vec2 pos, std::string filePath, int pixelSize);

	std::vector<Effect*> Effects;

	~EffectSystem();
private:
};

/*	#### GUIDLINES FOR OVERRIDING FUNCTIONS ####
 *	
 *	 ## Particle ##
 *	
 *	void setup()
 *	{
 *		generatedTime = doodle::ElapsedTime;
 *		if(duration != -1) {endTime = generatedTime + duration;}
 *		
 *		// setup code here
 *	
 *		isSetup = true;
 *	}
 *	
 *	void update()
 *	{
 *		if (!isSetup) { setup(); }
 *		pshowedTime = showedTime;
 *		showedTime = doodle::ElapsedTime - generatedTime;
 *	
 *		// update code here
 *		// If there are no time limit, then you have to set endCondtion true to remove particle
 *	}
 *	
 *	void draw()
 *	{
 *		doodle::push_settings();
 *	
 *		// draw code here
 *	
 *		doodle::pop_settings();
 *	}
 */

extern EffectSystem effectSystem;