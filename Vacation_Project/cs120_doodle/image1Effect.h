#pragma once
#include "ParticleSystem.h"
#include <doodle/color.hpp>
#include <doodle/image.hpp>

struct pixelParticle : public Particle
{
	virtual void setup() override;
	virtual void update() override;
	virtual void draw() override;

	int pixelSize{ 2 };
	int particleIndex{ -1 };
	float noiseSeedX{ 0 };
	float noiseSeedY{ 0 };

	pixelParticle() : Particle("nullPixel", Vec2{0, 0}, Vec2{ 0, 0 }, Vec2{ 0, 0 }) {};
	pixelParticle(int particleIndex, Vec2 pos, int pixelSize) : particleIndex{ particleIndex }, pixelSize{ pixelSize }, Particle("pixelParticle", pos, Vec2{ 0, 0 }, Vec2{ 0, 0 }) {};
};

class image1Effect : public Effect
{
public:
	virtual void generateParticle() override;
	virtual void setup() override;
	virtual void run() override;

	image1Effect(Vec2 origin, const std::string& imagePath) : imagePath{ imagePath }, Effect(origin) {};
	image1Effect(Vec2 origin, const std::string& imagePath, int pixelSize) : pixelSize{ pixelSize }, imagePath{ imagePath }, Effect(origin) {};
	~image1Effect();

private:
	std::string imagePath;

	int pixelSize{ 2 };

	int imageWidth{ 0 };
	int imageHeight{ 0 };

	float EffectrangeX{ -1 };
	float EffectrangeY{ -1 };
};