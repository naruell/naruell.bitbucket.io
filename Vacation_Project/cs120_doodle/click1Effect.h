#pragma once
#include "ParticleSystem.h"
#include <doodle/input.hpp>

struct Click1Circle : public Particle
{
	virtual void setup() override;
	virtual void update() override;
	virtual void draw() override;
	
	float outlineWidth{ 0 };

	Click1Circle(Vec2 pos) : Particle("Click1Circle", pos, Vec2{ 0.0f, 0.0f }, Vec2{ 0.0f, 0.0f }) {};
};

struct Click1Line : public Particle
{
	virtual void setup() override;
	virtual void update() override;
	virtual void draw() override;

	float angle;
	float length;
	Vec2 outPos{ 0, 0 };
	Click1Line(Vec2 pos, float angle) : angle(angle), length(static_cast<float>(doodle::random(40, 100))), Particle("Click1Line", pos, Vec2{ 0.0f, 0.0f }, Vec2{ 0.0f, 0.0f }) {};
};

struct Click1Twinkle : public Particle
{
	virtual void setup() override;
	virtual void update() override;
	virtual void draw() override;

	Click1Twinkle(Vec2 pos) : Particle("Click1Twinkle", pos, Vec2{ 0.0f, 0.0f }, Vec2{ 0.0f, 0.0f }) {};
};

class Click1Effect : public Effect
{
public:
	virtual void generateParticle() override;
	void generateTwinkle(Vec2 pos);
	virtual void setup() override;
	virtual void run() override;

	Click1Effect(Vec2 origin) : Effect(origin) {};
	~Click1Effect();
};