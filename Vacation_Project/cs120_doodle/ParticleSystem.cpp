#include "ParticleSystem.h"

#include "click1Effect.h"
#include "testEffect.h"
#include "image1Effect.h"

bool Effect::isEnded()
{
	if (duration != -1 && (showedTime >= duration)) { return true; }
	if (endCondition == true) { return true; }

	return false;
}

std::string Effect::getType()
{
	return type;
}

void Effect::setType(std::string typeTo)
{
	type = typeTo;
}

void Effect::remove()
{
	for (Particle* p : Particles)
	{
		delete p;
	}
}

int Effect::getParticlesSize()
{
	return static_cast<int>(Particles.size());
}

Effect::~Effect()
{
	for (Particle* p : Particles)
	{
		delete p;
	}
}

void EffectSystem::run()
{
	for (int i = static_cast<int>(Effects.size()) - 1; i >= 0; --i)
	{
		Effects[i]->run();
		
		if (Effects[i]->isEnded())
		{
			Effects.erase(Effects.begin() + i);
		}
	}
}

void EffectSystem::generateClick1Effect()
{
	float mouseX = static_cast<float>(doodle::get_mouse_x());
	float mouseY = static_cast<float>(doodle::get_mouse_y());
	Effects.push_back(new Click1Effect(Vec2{mouseX, mouseY}));
	Effects[Effects.size() - 1]->generateParticle();
}

void EffectSystem::generateTestEffect(Vec2 pos)
{
	Effects.push_back(new TestEffect(pos));
}

void EffectSystem::generateImage1Effect(Vec2 pos, std::string filePath, int pixelSize)
{
	Effects.push_back(new image1Effect(pos, filePath, pixelSize));
}

EffectSystem::~EffectSystem()
{
	for (Effect* e : Effects)
	{
		delete e;
	}
}

void Particle::run()
{
	update();
	draw();
}

bool Particle::isEnded()
{
	if (duration != -1 && (showedTime >= duration)) { return true; }
	if (endCondition == true) { return true; }

	return false;
}

EffectSystem effectSystem;