#include "interface.h"

void InfoInterface::run()
{
	update();
	draw();
}

void InfoInterface::setup()
{
	positionX = static_cast<float>(-doodle::Width / 2);
	positionY = static_cast<float>(doodle::Height / 2);

	lastEffectIndex = static_cast<int>(effectSystem.Effects.size() - 1);
	EffectCount = static_cast<int>(effectSystem.Effects.size());
	for (Effect* e : effectSystem.Effects)
	{
		ParticleCount += e->getParticlesSize();
	}

	isSetup = true;
}

void InfoInterface::update()
{
	if (!isSetup) { setup(); }

	positionX = static_cast<float>(-doodle::Width / 2);
	positionY = static_cast<float>(doodle::Height / 2) - fontSize * 2.0f;

	EffectCount = static_cast<int>(effectSystem.Effects.size());
	ParticleCount = 0;
	for (Effect* e : effectSystem.Effects)
	{
		ParticleCount += e->getParticlesSize();
	}
}

void InfoInterface::draw()
{
	doodle::push_settings();
	doodle::set_fill_color(doodle::Color4ub{255, 255, 255, 255});
	doodle::set_font_size(fontSize);
	doodle::draw_text("Current Effect Count : " + std::to_string(EffectCount), positionX, positionY);
	doodle::draw_text("Current Particle Count : " + std::to_string(ParticleCount), positionX, positionY - fontSize);
	doodle::pop_settings();
}

void InfoInterface::setEffectType(std::string EffectTo)
{
	curEffectType = EffectTo;
}

std::string InfoInterface::getEffectType()
{
	return curEffectType;
}

InfoInterface infoInterface;