#include "click1Effect.h"

void Click1Circle::setup()
{
	generatedTime = doodle::ElapsedTime;

	fillColor = doodle::Color4ub{0, 0, 0, 0};
	outlineColor = doodle::Color4ub{13, 130, 180, 255};
	outlineWidth = 5.0f;
	size_w = 0.0f;
	isSetup = true;
}

void Click1Circle::update()
{
	if (!isSetup) { setup(); }
	pshowedTime = showedTime;
	showedTime = doodle::ElapsedTime - generatedTime;

	float delta = -showedTime * 4.0f;
	if (delta <= -2.0f) { delta = -2.0f ; }
	size_w += (3.0f + delta) * 120 * (showedTime - pshowedTime);
	outlineWidth -= 0.1f * 120 * (showedTime - pshowedTime);

	float alpha = 255.0f - showedTime * 200.0f;
	if (alpha <= 0) { alpha = 0; }
	outlineColor.alpha = static_cast<doodle::Color4ub::unsigned_byte>(alpha);

	float green = 255.0f - showedTime * 300.0f;
	if (green <= 0) { green = 0; }
	outlineColor.green = static_cast<doodle::Color4ub::unsigned_byte>(green);

	float blue = showedTime * 500.0f;
	if (blue >= 255) { blue = 255; }
	outlineColor.blue = static_cast<doodle::Color4ub::unsigned_byte>(blue);

	if (outlineWidth <= 0) 
	{
		outlineWidth = 0.0f;
		endCondition = true;
	}
}

void Click1Circle::draw()
{
	doodle::push_settings();

	doodle::set_fill_color(fillColor);
	doodle::set_outline_color(outlineColor);
	if (outlineWidth != 0) { doodle::set_outline_width(outlineWidth); }
	else { doodle::no_outline(); }
	doodle::draw_ellipse(position.x, position.y, size_w, size_w);

	doodle::pop_settings();
}

void Click1Line::setup()
{
	generatedTime = doodle::ElapsedTime;

	outPos.x = position.x + length * cos(angle);
	outPos.y = position.y + length * sin(angle);

	fillColor = doodle::Color4ub{ 13, 255, 255, 155 };
	outlineColor = doodle::Color4ub{ 0, 255, 111, 100 };
	isSetup = true;
}

void Click1Line::update()
{
	if (!isSetup) { setup(); }
	pshowedTime = showedTime;
	showedTime = doodle::ElapsedTime - generatedTime;

	length -= length / 15.0f * 120 * (showedTime - pshowedTime);

	outPos.x = position.x + length * cos(angle);
	outPos.y = position.y + length * sin(angle);

	if (length <= 20) { endCondition = true; }
}

void Click1Line::draw()
{
	doodle::push_settings();

	doodle::set_fill_color(fillColor);

	doodle::set_outline_width(2);
	doodle::set_outline_color(outlineColor);
	doodle::draw_line(position.x, position.y, outPos.x, outPos.y);

	doodle::set_outline_width(1);
	doodle::set_outline_color(fillColor);
	doodle::draw_line(position.x, position.y, outPos.x, outPos.y);

	doodle::pop_settings();
}

void Click1Twinkle::setup()
{
	generatedTime = doodle::ElapsedTime;

	fillColor = doodle::Color4ub{ 255, 255, 255, 255 };
	outlineColor = doodle::Color4ub{ 255, 255, 255, 150 };
	size_w = 10.0f;
	size_h = 13.0f;

	duration = 0.3f;

	isSetup = true;
}

void Click1Twinkle::update()
{
	if (!isSetup) { setup(); }
	pshowedTime = showedTime;
	showedTime = doodle::ElapsedTime - generatedTime;

	size_w = map(showedTime, 0.0f, 0.3f, 10.0f, 0.0f);
	size_h = map(showedTime, 0.0f, 0.3f, 15.0f, 0.0f);
}

void Click1Twinkle::draw()
{
	doodle::push_settings();

	doodle::set_fill_color(fillColor);
	doodle::set_outline_color(outlineColor);
	float x = position.x;
	float y = position.y;
	float w = size_w;
	float h = size_h;

	for (int h_sign = -1; h_sign <= 1; h_sign += 2)
	{
		for (int w_sign = -1; w_sign <= 1; w_sign += 2)
		{
			doodle::draw_quad(x, y, x, y + h_sign * h, x + w_sign * w / 4.0f, y + h_sign * h / 4.0f, x + w_sign * w, y);
		}
	}

	doodle::pop_settings();
}

void Click1Effect::generateParticle()
{
	float mouseX = static_cast<float>(doodle::get_mouse_x());
	float mouseY = static_cast<float>(doodle::get_mouse_y());

	Particles.push_back(new Click1Circle(Vec2{ mouseX, mouseY }));
	for (int i = 0; i <= 15; ++i)
	{
		float angle = doodle::TWO_PI / 15.0f * static_cast<float>(i) + static_cast<float>(doodle::random(-10, 10));
		Particles.push_back(new Click1Line(Vec2{ mouseX, mouseY }, angle));
	}

	for (int i = 0; i <= 7; ++i)
	{
		float angle = doodle::TWO_PI / 10.0f * static_cast<float>(i) + static_cast<float>(doodle::random(-10, 10));
		float distance = static_cast<float>(doodle::random(5, 20));
		Particles.push_back(new Click1Twinkle(Vec2{ mouseX + distance * cos(angle), mouseY + distance * sin(angle) }));
	}
}

void Click1Effect::generateTwinkle(Vec2 pos)
{
	Particles.push_back(new Click1Twinkle(pos));
}

void Click1Effect::setup()
{
	generatedTime = doodle::ElapsedTime;
	if (duration != -1) { endTime = generatedTime + duration; }

	isSetup = true;
}

void Click1Effect::run()
{
	if (!isSetup) { setup(); }
	pshowedTime = showedTime;
	showedTime = doodle::ElapsedTime - generatedTime;

	if (pshowedTime <= 0.1f && showedTime >= 0.1f)
	{

		for (int i = 0; i <= 6; ++i)
		{
			float angle = doodle::TWO_PI / 10.0f * static_cast<float>(i) + static_cast<float>(doodle::random(-10, 10));
			float distance = static_cast<float>(doodle::random(10, 30));
			Particles.push_back(new Click1Twinkle(Vec2{ origin.x + distance * cos(angle), origin.y + distance * sin(angle) }));
		}
	}
	
	if (pshowedTime <= 0.2f && showedTime >= 0.2f)
	{

		for (int i = 0; i <= 9; ++i)
		{
			float angle = doodle::TWO_PI / 10.0f * static_cast<float>(i) + static_cast<float>(doodle::random(-10, 10));
			float distance = static_cast<float>(doodle::random(20, 35));
			Particles.push_back(new Click1Twinkle(Vec2{ origin.x + distance * cos(angle), origin.y + distance * sin(angle) }));
		}
	}

	if (pshowedTime <= 1.0f && showedTime >= 1.0f)
	{
		endCondition = true;
	}

	for (int i = static_cast<int>(Particles.size()) - 1; i >= 0; --i)
	{
		Particles[i]->run();

		if (Particles[i]->isEnded())
		{
			Particles.erase(Particles.begin() + i);
		}
	}
}

Click1Effect::~Click1Effect()
{
	for (Particle* p : Particles)
	{
		delete p;
	}
}