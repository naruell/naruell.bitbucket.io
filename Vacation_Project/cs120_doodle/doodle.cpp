#include <iostream>
#include <doodle/doodle.hpp>
#include "ParticleSystem.h"
#include "interface.h"

using namespace doodle;

doodle::HexColor BackgroundColor = 0x000000FF;

int main(void)
{
    create_window();
	doodle::show_cursor(true);
	// effectSystem.generateTestEffect(Vec2{0, 300});

	// doodle::Texture cursorImage;
	// cursorImage.LoadFromPNG("Graphics/cursor.png");
	doodle::Texture logoImage;
	logoImage.LoadFromPNG("Graphics/System/logo(org).png");

    while (!is_window_closed())
    {
		update_window();
		clear_background(BackgroundColor);

		effectSystem.run();
		infoInterface.run();

		// draw_texture(cursorImage, static_cast<float>(get_mouse_x()), static_cast<float>(get_mouse_y() - cursorImage.GetHeight()));
		if (doodle::ElapsedTime <= 2.0f) { draw_texture(logoImage, -512, -123); }

		if (doodle::MouseIsPressed && doodle::MouseButton == doodle::MouseButtons::Right)
		{
			effectSystem.generateClick1Effect();
		}
    }

    return 0;
}

void on_mouse_pressed(MouseButtons button)
{
	if (button == MouseButtons::Left)
	{
		effectSystem.generateClick1Effect();
	}
	if (button == MouseButtons::Right)
	{
		effectSystem.generateImage1Effect(Vec2{ -512, 123 }, "Graphics/System/logo(org).png", 2);
	}
}