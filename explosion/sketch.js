// wriiten by Duhwan.Kim
// Exercise Gravity Particles Array
// Course CS099
// Spring 2019

let particles = [];
const partnum = [100,50,50,10];

function setup() {
  createCanvas(1000, 1000);
  noStroke();

}

function draw() {
  background(220);
  
  for(let i = 0; i<particles.length; i++)
  {
    particles[i].update();
    particles[i].accelerate();
    particles[i].draw();
  }
  
}

function mouseReleased()
{ 
  explooooostion();
}

function explooooostion()
{
  for(let i = 0; i<partnum[0]; i++)
  {
    particles.push(new particle(mouseX,mouseY,random(3,5),random(TWO_PI),10,'red'));
  }
  for(let i = 0; i<partnum[1]; i++)
  {
    particles.push(new particle(mouseX,mouseY,random(2,4),random(TWO_PI),10,'orange'));
  }
  for(let i = 0; i<partnum[2]; i++)
  {
    particles.push(new particle(mouseX,mouseY,random(2,3),random(TWO_PI),10,'yellow'));
  }
    for(let i = 0; i<partnum[3]; i++)
  {
    particles.push(new particle(mouseX,mouseY,random(1,2),random(TWO_PI),10,'black'));
  }
}
