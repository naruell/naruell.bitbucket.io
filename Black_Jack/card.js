let Deck = new Array(13*4);

let Card_Shape;
let Card_Number;
for(let i = 1; i < 14; i++)
{
  for(let j = 0; j < 4; j++)
  {
    switch(i)
    {
      case 1: Card_Number = "A"; break;
      case 11: Card_Number = "J"; break;
      case 12: Card_Number = "Q"; break;
      case 13: Card_Number = "K"; break;
      default: Card_Number = i; break;
    }
    
    switch(j)
    {
      case 0: Card_Shape = "Spade"; break;
      case 1: Card_Shape = "Diamond"; break;
      case 2: Card_Shape = "Heart"; break;
      case 3: Card_Shape = "Clover"; break;
    }
  }
}