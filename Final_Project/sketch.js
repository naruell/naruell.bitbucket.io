// written by Duhwan.Kim
// Assignment Final Project
// Course CS099
// Spring 2019

// NOW LIST June 15.
// 튜토리얼

let time = 0;
let start = -1;
let blink = 1;
let state = 0; // 0: mainmenu | 1: play | 2:edit

let Spring;
let FunkRider;
let Star;
let StrawberrySoldier;

let Springtxt;
let FunkRidertxt;
let Startxt;
let StrawberrySoldiertxt;

function preload() {
  Spring = loadSound("Songs/Spring.mp3");
  Springtxt = loadStrings("Songs/Spring.txt");
  
  FunkRider = loadSound("Songs/FunkRider.mp3");
  FunkRidertxt = loadStrings("Songs/FunkRider.txt");
  
  Star = loadSound("Songs/Star.mp3");
  Startxt = loadStrings("Songs/Star.txt");
  
  StrawberrySoldier = loadSound("Songs/StrawberrySoldier.mp3");
  StrawberrySoldiertxt = loadStrings("Songs/StrawberrySoldier.txt");

  allist = [{name:"Spring [tutorial]", song:Spring, sheet:Spring_sheet, txt:Springtxt, play:-1}, 
        {name:"FunkRider", song:FunkRider, sheet:FunkRider_sheet, txt:FunkRidertxt, play:-1},
        {name:"Star", song:Star, sheet:Star_sheet, txt:Startxt, play:-1}, 
        {name:"StrawberrySoldier", song:StrawberrySoldier, sheet:StrawberrySoldier_sheet, txt:StrawberrySoldiertxt, play:-1}];

  bg = loadImage("Image/bg.png");
  noteline = loadImage("Image/noteline.png");
  tutoimage = loadImage("Image/howtoplay.png");

  pdo = loadSound("Sound_Effect/do.wav");
  pddo = loadSound("Sound_Effect/ddo.wav");
  pre = loadSound("Sound_Effect/re.wav");
  prre = loadSound("Sound_Effect/rre.wav");
  pme = loadSound("Sound_Effect/me.wav");
  pfa = loadSound("Sound_Effect/fa.wav");
  pffa = loadSound("Sound_Effect/ffa.wav");
  psol = loadSound("Sound_Effect/sol.wav");
  pssol = loadSound("Sound_Effect/ssol.wav");
  pla = loadSound("Sound_Effect/la.wav");
  plla = loadSound("Sound_Effect/lla.wav");
  psi = loadSound("Sound_Effect/si.wav");
  p_do = loadSound("Sound_Effect/_do.wav");

  mainclick = loadSound("Sound_Effect/mainclick.wav");
}

function setup() {
  amplitude = new p5.Amplitude();

  createCanvas(2000, 900);
  // when song is playing, 
  setInterval( function() { if(start == 1 && time/10 < nowsong.duration()-0.1) { time+=1;} }, 100);
  // show blink in 0.8 seconds
  setInterval(() => blink *= -1, 800);

  // setting mainmenu song's volume
  setInterval( function() { if(sound < 0.16 && state == 0 && nowsong != undefined) { sound += 0.01; nowsong.setVolume(sound)}} , 300);

  setInterval( function() { if(state == 0 && mainplay == 1) {maintime +=1;}}, 100 );

  setInterval( function() { if(nowsong != undefined && playplay == 1 && nowsheet.length == 0) {showup+=1;}}, 100)
}

function draw() {
  state_check();
}

function mouseReleased()
{
  if(state == 2)
  {
    // when pressed notenum button
    if(sqrt(pow(editbutton.notenum.x-mouseX,2) + pow(editbutton.notenum.y-mouseY,2)) <= editbutton.notenum.s)
    {
      editbutton.notenum.m *= -1;
      notenum_show *= -1;
    }
    
    // when pressed play/pause button
    if(sqrt(pow(editbutton.toggle.x-mouseX,2) + pow(editbutton.toggle.y-mouseY,2)) <= editbutton.toggle.s && (editnote == 0  || editnote == 2))
    {
      start *= -1;
      if(editplay == -1) {editplay =0;}
      else if(editplay == 1) {editplay = -1;}
    }
    
    // when pressed jump button
    if(sqrt(pow(editbutton.jump.x-mouseX,2) + pow(editbutton.jump.y-mouseY,2)) <= editbutton.jump.s && editplay == -1 && editnote == 0)
    {
      let inp = float(prompt("##  Input the time to jump in SECOND  ##\n" + "## Now : " + time/10 + " sec  ##\n" +"##  The end of the song : " + nf(nowsong.duration()-0.1,0,1) + " sec  ##" ));
      if(inp >= 0 && inp <= nowsong.duration()-0.1) {time = nf(inp,0,1)*10;}
    }

    // when pressed addnote button
    if(sqrt(pow(editbutton.add.x-mouseX,2) + pow(editbutton.add.y-mouseY,2)) <= editbutton.add.s && editplay == -1 && editnote == 0)
    {
      editnote = 1;
    }
    
    // when pressed editnote button
    if(sqrt(pow(editbutton.edit.x-mouseX,2) + pow(editbutton.edit.y-mouseY,2)) <= editbutton.edit.s && editplay == -1 && editnote == 0)
    {
      editnote = 2;
    }
    
    // when pressed deletenote button
    if(sqrt(pow(editbutton.del.x-mouseX,2) + pow(editbutton.del.y-mouseY,2)) <= editbutton.edit.s && editplay == -1 && editnote == 0)
    {
      editnote = 3;
    }
    
    // when pressed change show button 
    if(sqrt(pow(changebutton.show.x-mouseX,2) + pow(changebutton.show.y-mouseY,2)) <= changebutton.show.s && editplay == -1 && editnote == 2)
    {
      nownote.show = float(prompt("When will the note showup? (In SECOND)"));
      if(nownote.show < 0 || nownote.show > nowsong.duration()-0.1) {nownote.show = originalnote.show;}
      else {nownote.converthit();}
    }
    
    // when pressed change color button 
    if(sqrt(pow(changebutton.color.x-mouseX,2) + pow(changebutton.color.y-mouseY,2)) <= changebutton.color.s && editplay == -1 && editnote == 2)
    {
      nownote.color = prompt("What color the note will be? \n(Web color likes skyblue)\n(Hexcod likes #87CEEB)");
    }
    
    // when pressed change key button 
    if(sqrt(pow(changebutton.key.x-mouseX,2) + pow(changebutton.key.y-mouseY,2)) <= changebutton.key.s && editplay == -1 && editnote == 2)
    {
      nownote.key = prompt("What key the note will be?");
      if(Ds.indexOf(nownote.key) == -1 && Fs.indexOf(nownote.key) == -1 && Js.indexOf(nownote.key) == -1 && Ks.indexOf(nownote.key) == -1)
      {nownote.key = originalnote.key; nownote.convertkey();}
      else {nownote.convertkey();}
    }
    
    // when pressed confirm button
    if(sqrt(pow(changebutton.ok.x-mouseX,2) + pow(changebutton.ok.y-mouseY,2)) <= changebutton.ok.s && editplay == -1 && editnote == 2)
    {
      mainclick.play(0,1,0.3);
      edit_changenote_progress = 'select';
      changenote = -1;
      editnote = 0;
    }
    
    // when pressed cancel button
    if(sqrt(pow(changebutton.no.x-mouseX,2) + pow(changebutton.no.y-mouseY,2)) <= changebutton.no.s && editplay == -1 && editnote == 2)
    {
      mainclick.play(0,1,0.3);
      nownote.show = originalnote.show;
      nownote.time = originalnote.time;
      nownote.hit = originalnote.hit;
      nownote.color = originalnote.color;
      nownote.key = originalnote.key;
      nownote.line = originalnote.line;
      
      edit_changenote_progress = 'select';
      changenote = -1;
      editnote = 0;
    }
    // when pressed select button
    if(editnote == 0 && editplay == -1 && mouseX >= editbutton.sel.x - editbutton.sel.s[0]/2 && mouseX <= editbutton.sel.x + editbutton.sel.s[0]/2 && 
        mouseY >= editbutton.sel.y - editbutton.sel.s[1]/2 && mouseY <= editbutton.sel.y + editbutton.sel.s[1]/2)
    {
      mainclick.play(0,1,0.3);
      editplay = -1; editnote = 0; nowsong = undefined; nowsheet = undefined; nowtxt = undefined; notenum_show = 1;
      start = -1; time = 0; state = 2;
    }

    // when pressed back button
    if(editnote == 0 && editplay == -1 && mouseX >= editbutton.back.x - editbutton.back.s[0]/2 && mouseX <= editbutton.back.x + editbutton.back.s[0]/2 && 
        mouseY >= editbutton.back.y - editbutton.back.s[1]/2 && mouseY <= editbutton.back.y + editbutton.back.s[1]/2)
    {
      mainclick.play(0,1,0.3);
      editplay = -1; editnote = 0; nowsong = undefined; nowsheet = undefined; nowtxt = undefined; notenum_show = 1;
      start = -1; time = 0; state = 0;
    }
  }

  if(mouseButton == CENTER) {rearrange(); save_sheet(nowname); console.log("save!");}
  if(mouseButton == RIGHT) {load_sheet(nowtxt, nowsheet); console.log("load!");}
}

let nowkey = [];

let judge = true;
function keyPressed()
{
  let note;
  if(nowsong != undefined && playplay == 1) {note = nowsheet[0];}
  
  if(note != undefined && playplay == 1 && key == note.key && judge)
    {
      if(note.hit-Prange <= time/10)
      {
          lyric+=note.rawkey;
          nowsheet.splice(0,1);
          combo++;
          if(combo >= maxcombo) {maxcombo = combo;}
          scoreeffect = 'perfect';
          score.perfect++;
          judge = false;
        }

        else if (note.hit-Grange <= time/10)
          {
            lyric+=note.rawkey;
            nowsheet.splice(0,1);
            combo++;
            if(combo >= maxcombo) {maxcombo = combo;}
            scoreeffect = 'good';
            score.good++;
            judge = false;
          }

        else if (note.hit+0.2 >= time/10 && note.hit-Brange <= time/10) 
          {
            lyric+=note.rawkey;
            nowsheet.splice(0,1);
            combo = 0;
            scoreeffect = 'bad';
            score.bad++;
            judge = false;
          }
    }

    if(state == 0) 
    {
      switch(key)
      {
        case 's': pdo.play(0,1,0.2); break;
        case 'e': pddo.play(0,1,0.2); break;
        case 'd': pre.play(0,1,0.2); break;
        case 'r': prre.play(0,1,0.2); break;
        case 'f': pme.play(0,1,0.2); break;
        case 'g': pfa.play(0,1,0.2); break;
        case 'y': pffa.play(0,1,0.2); break;
        case 'h': psol.play(0,1,0.2); break;
        case 'u': pssol.play(0,1,0.2); break;
        case 'j': pla.play(0,1,0.2); break;
        case 'i': plla.play(0,1,0.2); break;
        case 'k': psi.play(0,1,0.2); break;
        case 'l': p_do.play(0,1,0.2); break;
      }
    }
}

/// Temporary function
function keyReleased()
{
  judge = true;
/////////////////////////////////////////////////////////////
  if(state == 1 && start == -1 && key == 'q')
  {
      start *= -1;
      if(playplay == -1) {playplay =0;}
  }
/////////////////////////////////////////////////////////////
}

function mouseDragged()
{
  if(state == 2)
  {
    // Moving tracking bar above
    if(mouseX >= 50 && mouseX <= 1450 && mouseY >=0 && mouseY <50 && editplay == -1) 
    {
      time += (round((pmouseX-mouseX)/2));
      
      // control the time not to be out of song
      if(time/10 <= 0) {time = 0;}
      if(time/10 >= nowsong.duration()) {time = floor(nowsong.duration()*10)-1;}
    }
    
    // editnote - drag to change time
    if(mouseX >= changebutton.time.x && mouseX <= changebutton.time.x + changebutton.time.s[0] &&
       mouseY >= changebutton.time.y && mouseY <= changebutton.time.y + changebutton.time.s[1] && editnote == 2) 
    {
      nownote.time -= round(pmouseX-mouseX)/100;
      if(nownote.time <= 0) {nownote.time = 0;}
      if(nownote.time >= 5) {nownote.time = 5;}
      nownote.converthit();
    }
  }
}