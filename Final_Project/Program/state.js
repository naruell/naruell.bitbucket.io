// written by Duhwan.Kim
// Assignment Final Project
// Course CS099
// Spring 2019

function state_check()
{
	switch(state)
	{
		case 0:
			mainmenu_song();
			mainmenu_draw();
			mainmenu_howtoplay();
			mainmenu_credit();
			break;
		case 1: 
			play_draw();
		 	play_playsong();
		 	play_judgement();
 		 	play_showscore();
 		 	break;
		case 2:
			edit_draw();
  			edit_interface();
  			edit_playsong();
			break;
	}
}