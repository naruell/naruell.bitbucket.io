// written by Duhwan.Kim
// Assignment Final Project
// Course CS099
// Spring 2019

let K;
let playplay = -1; // -1 : pause || 0 : start SONG  || 1 : play
let hitline = 830;
let Prange = 0.08;
let Grange = 0.2;
let Brange = 0.4;

// Temp Data = 0.08 || 0.2 || 0.4
// Change Data = 0.2 || 0.4 || 0.6

let grade;
let fullcombo = 0;
let combo = 0;
let maxcombo = 0;
let score = {perfect:0, good:0, bad:0, miss:0};

let lyric = "";

let showup = 0;
function play_draw()
{
	if(nowsong == undefined || nowsheet == undefined || nowtxt == undefined)
	{
		select_song();
	}

	else
	{
		//////////////////////////////
	    if(keyIsPressed) {K = key_to_line[key];}

		createCanvas(2000, 900);
		background(bg);
		push();

		// background gray rectangle
		fill('gray');
		rect(30, 800, 1140, 100, 50);
		let linecolor = [[265,165,0], [255,255,0], [0,255,255], [0,255,0]];
		image(noteline, 100, 0);
		// vertical lines
		for(let i = -2; i<3; i++) { stroke(255); line(600 + i*100, 0, 600 + i*250, hitline);}
		for(let i = -2; i<2; i++) 
		{
			// We need fill lines with each color if the key is pressed
			noFill();
			if(keyIsPressed && K == i+2) {fill(linecolor[i+2][0], linecolor[i+2][1], linecolor[i+2][2]);}
			// each lines
			quad(600 + i*100, 0, 600 + (i+1)*100, 0, 600 + (i+1)*250, hitline, 600 + i*250, hitline);

			// hitscan rectangle
			fill(50);
			rect(600 + i*250, 800, 250, 60, 30);
		}

		for(let note of nowsheet)
		{
			let v = (hitline-20)/note.time;
			if(note.show <= time/10)
		    {

		      //each notes
		      note.progress+= 1/frameRate();
		      stroke(note.color);
		      strokeWeight(40*note.progress/note.time);
		      line(600+(note.line-2)*100+(note.line-2)*150/note.time*note.progress+20/note.time*note.progress, v*note.progress, 
		      	   600+(note.line-1)*100+(note.line-1)*150/note.time*note.progress-20/note.time*note.progress, v*note.progress);
		      fill(0);
		      textSize(20+60*note.progress/note.time);
		      strokeWeight(10);
		      textStyle(BOLDITALIC);
		      textAlign(CENTER, CENTER);
		      text(note.key, 600+(note.line-1.55)*100+(note.line-1.6)*150/note.time*note.progress+20/note.time*note.progress, v*note.progress);
		    }
		}
		pop();
		textSize(50);
		fill(255);
		textAlign(CENTER);
		if(combo >= 5) {text(combo, 600, 300);}

		// show next key
		if(playplay == 1 && nowsheet.length != 0) 
		{
			push();
			// fill background black to be easily seen
			textStyle(BOLD);
			stroke('turquoise');
			strokeWeight(6);
			fill(0, 200);
			rect(1000, 110, 450, 180, 30);

			noStroke();
			fill(255);
			text("Next Key : ", 1200, 220); 
			// stroke(0);
			// strokeWeight(5);
			fill(nowsheet[0].color); 
			textSize(80);
			text(nowsheet[0].key, 1350, 220);
			pop();
		}

		// if song isn't started, show text to start
		if(playplay == -1) 
		{
			push();
			rectMode(CENTER);
			textAlign(CENTER, CENTER);
			if(blink == 1) {fill('red'); textSize(35); rect(1200, 200, 60, 60, 12);}
			else {fill('white'); textSize(45); rect(1200, 200, 70, 70, 12);}
			fill(0);
			text("Q", 1200, 200);
			pop();
			text("Press \"q\" to start", 1470, 220);
		}

		// if song is ended
		if(nowsheet.length == 0)
		{
			push();
			let totalscore = 900000*score.perfect/fullcombo + 630000*score.good/fullcombo + 270000*score.bad/fullcombo + 100000*maxcombo/fullcombo;
			// scoreboard img
			if( totalscore == 1000000 ) {grade = "SS";}
			else if( totalscore >= 900000) {grade = "S";}
			else if( totalscore >= 850000) {grade = "A";}
			else if( totalscore >= 700000) {grade = "B";}
			else if( totalscore >= 500000) {grade = "C";}
			else {grade = "D";}

			fill('pink');
			stroke('hotpink');
			strokeWeight(15);
			rect(100, 100, 1800, 700, 50);

			fill(0);
			stroke('gray');
			strokeWeight(3);
			textStyle(ITALIC);
			textAlign(CENTER, TOP);
			textSize(60);
			text("Perfect : ", 400, 150);
			text("good : ", 1000, 150);
			text("bad : ", 400, 250);
			text("miss : ", 1000, 250);
			text("maxcombo : ", 400, 400);
			text("SCORE : ", 400, 500);

			noStroke();
			textStyle(BOLD);
			textSize(80);
			fill('darkviolet');
			if(showup >= 5) {text(score.perfect, 650, 150);}
			if(showup >= 10) {text(score.good, 1200, 150);}
			if(showup >= 15) {text(score.bad, 650, 250);}
			if(showup >= 20) {text(score.miss, 1200, 250);}
			if(showup >= 25) {text(maxcombo, 650, 400);}
			if(showup >= 30) {let N = (showup-20)*46557; if(N >= totalscore){N = round(totalscore);} text(N, 750, 500);}

			fill(255, 20, 147, (showup-45)*15);
			textSize(300);
			strokeWeight(20);
			stroke(128, 0, 128, (showup-35)*15);
			if(showup >= 40) {text(grade, 1500, 250);}
			if(showup >= 45) {let SX = 1800-(showup-60)*50; let SY = 100+(showup-60)*70; if(SX<=1750) {SX = 1750;} if(SY >=150) {SY = 150;}text('+', SX, SY);}
			
			fill(0);
			stroke('gray');
			strokeWeight(2);
			textStyle(ITALIC);
			textAlign(CENTER, TOP);
			textSize(45);
			if(showup >= 50) { text(lyric, 200, 600, 1600, 300);}


			if(blink == 1 && showup >= 52) {fill(255,0,0); noStroke(); text("Press \"R\" to go back to mainmenu", 1500, 530);}
			if(keyIsPressed && key == 'r') 
				{
					nowsong.stop();
					playplay = -1; start = -1; time = 0; fullcombo = 0; combo = 0; maxcombo = 0;
					score.perfect = 0; score.good = 0; score.bad = 0; score.miss = 0; lyric = "";
					showup = 0; nowsong = undefined; nowsheet = undefined; nowtxt = undefined;
					state = 0; scoreeffect = undefined;
				}
			pop();
		}
			
	}
}

function play_playsong()
{
  if(playplay == 0 && nowsong != undefined) {nowsong.play(0,1,0.1,time/10); playplay = 1;}
  if(playplay == -1 && nowsong != undefined) {nowsong.stop();}
}

// Just suppose key is pressed
function play_judgement()
{
	if(playplay == 1)
	{
		let note = nowsheet[0];

		if(note != undefined && time/10 > note.hit+0.1)
		{
			lyric+='□';
			nowsheet.splice(0,1);
			combo = 0;
			score.miss++;
			scoreeffect = 'miss';
		}
	}
}

let scoreeffect;

function play_showscore()
{
	if(nowsong != undefined && nowsheet.length != 0)
	{
		push();
		textAlign(CENTER);
		fill(255);
		switch(scoreeffect)
		{
			case 'perfect': text("Perfect!",600,380); break;
			case 'good': text("Good!",600,380); break;
			case 'bad': text("Bad!",600,380); break;
			case 'miss': text("miss!",600,380); break;
			default: break;
		}
		pop();
	}
}
// when playmode is conclude, PLEASE reset the each note's progress, combo, time