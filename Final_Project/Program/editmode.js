// written by Duhwan.Kim
// Assignment Final Project
// Course CS099
// Spring 2019

let gridx = [];
for(let i=-14; i<15; i++) {gridx.push(i);}

let editplay = -1; // -1 : pause || 0 : start SONG  || 1 : play
let editnote = 0;  // 0: nothing || 1 : adding note || 2 : editing note  || 3 : delete note

let nowsong;
let nowsheet;
let nownote;
let nowtxt;
let nowname;

let notenum_show = 1;   // -1 : don't show the note numbers on the board
                         // 1 : show the note numbers on the board

function select_song()
{
  push();
  background(bg);
  fill('hotpink');
  textSize(50);
  textStyle(BOLDITALIC);
  textAlign(LEFT);
  text("<< SONGS >>", 430, 150);

  for(let i = 0; i < allist.length; i++)
  {
    // When mouse is on song, display song's info, preview song play
    if(mouseX >= 100 && mouseX <= 1100 && mouseY >=200+i*100 && mouseY < 300+i*100) 
    {
      push();
      if(allist[i].play == -1) {allist[i].play = 0;} 
      fill('pink');
      stroke('hotpink');
      strokeWeight(8);
      rect(1140, 200, 600, 600, 50);

      fill('black');
      square(1340, 300, 200);
      fill('snow');
      noStroke();
      circle(1440, 400, 90);
      fill('black');
      circle(1440, 400, 20);

      // Show song's info
      textAlign(CENTER, CENTER);
      textStyle(BOLD);
      fill(0);
      textSize(47);
      noStroke();
      text("Name : "+allist[i].name, 1440, 600);
      text("Artist : CUBE", 1440, 660);
      text("Duration : "+nf(allist[i].song.duration(),0,1)+" Sec", 1440, 720);

      translate(1440, 400);
      rotate(millis()/700);
      textSize(20);
      text('CUBE',0, -60);
      pop();
      if(mouseIsPressed) 
        {mainclick.play(0,1,0.3); nowsong = allist[i].song; nowsheet = allist[i].sheet, nowtxt = allist[i].txt; nowname = allist[i].name; 
         allist[i].song.stop(); load_sheet(nowtxt, nowsheet); fullcombo = nowsheet.length;}
    }
    else {allist[i].play = -1;}

    if(allist[i].play == 0) 
    {
      switch(allist[i].song)
      {
        case FunkRider: allist[i].song.loop(0,1,0.1,29.58); break;
        case Spring: allist[i].song.loop(0,1,0.1,49); break;
        case Star: allist[i].song.loop(0,1,0.1,38.6); break;
        case StrawberrySoldier:allist[i].song.loop(0,1,0.1,40.5); break;
      }
      allist[i].play = 1;
    }
    else if (allist[i].play == -1) {allist[i].song.stop();}

    stroke('hotpink');
    strokeWeight(7);
    fill('lightpink');
    if(mouseX >= 100 && mouseX <= 1100 && mouseY >=200+i*100 && mouseY < 300+i*100) {fill('plum');}
    rect(100, 200+i*100, 1000, 100, 30);

    fill('hotpink');
    strokeWeight(3);
    stroke('gray');
    text(allist[i].name, 150, 260+i*100);
  }

  // back button
  fill('pink');
  if(mouseX >= 150 && mouseX <= 370 && mouseY >= 700 && mouseY <= 780) 
  {
    fill('plum');
    if(mouseIsPressed) {mainclick.play(0,1,0.3); state = 0;}
  }
  stroke('hotpink');
  strokeWeight(7);
  rect(150, 700, 220, 80, 30);

  fill('hotpink');
  stroke('gray');
  strokeWeight(4);
  text('▶ Back', 170, 755);
  pop();
}

function edit_draw()
{
  if(nowsong == undefined || nowsheet == undefined || nowtxt == undefined)
  {
    select_song();
  }

  else
  {
    background(bg);
    
    push();
    let size = {x:1400, y:400};
    let tracking_bar = 750;

    textAlign(CENTER, CENTER);
    textSize(15);
    
    // white board and horizontal lines separate note's lines
    fill(255,210);
    rect(50,50,size.x,size.y);
    line(50,150,50+size.x,150);
    line(50,250,50+size.x,250);
    line(50,350,50+size.x,350);
    
    // tracking bar(red line) will be the center
    // translated (750, 50) to (0, 0)
    translate(tracking_bar,50);
    
    // draw vertical grid on the board
    for(let i=0; i<28; i++)
    {
      push();
      
      let grid = -time/10*100;
      
      if(gridx[i]*50 + grid < -700) {gridx[i]+=28;}
      if(gridx[i]*50 + grid > 700) {gridx[i]-=28;}
      
      text(gridx[i]*0.5, gridx[i]*50 + grid, -10);
      
      if(i%2 == 0) {strokeWeight(3);}
      if(gridx[i]*0.5 == 0) {strokeWeight(6);}
      
      if(gridx[i]*0.5 < 0 || gridx[i]*0.5 > nowsong.duration()) {noStroke();}
      line(gridx[i]*50 + grid, 0, gridx[i]*50 + grid, 400);
      pop();
    }
    
    // draw notes on the board
    for(let i = nowsheet.length-1; i >= 0; i--)
    {
      push();
      let S = nowsheet[i];
      let grid = -time/10*100;
      if(S.show >= time/10-7 && S.hit <= time/10+7)
      {
        if(S.show+S.time == time/10) {strokeWeight(20);}
        fill(S.color);
        if(i == changenote && blink == -1) {noFill(); noStroke();}
        rect(S.show*100+grid,S.line*100+20,S.time*100,30);
        fill(0);
        textSize(30);
        textStyle(BOLD);
        if(i == changenote && blink == -1) {noFill(); noStroke();}
        text(S.key,(S.show+S.time)*100+grid-10,S.line*100+40);
        
        if(notenum_show == 1) {textSize(15); text(i,(S.show+S.time)*100+grid-10,S.line*100+13);}
      }
      pop();
    }
     
    // tracking bar (Red line middle of the sheet)
    push();
    strokeWeight(5);
    stroke(255,0,0);
    fill(255,0,0);
    line(0,-10,0,size.y+10);
    triangle(0,-10,-15,-30,15,-30);
    pop();
    
    // cheking time
    textSize(30);
    fill(255);
    stroke(0);
    strokeWeight(10);
    text("Time : "+time/10,0,450);
   
    
    // editing note modes
    switch(editnote)
    {
      case 0: break;
      case 1: edit_addnote(); break;
      case 2: edit_editnote(); break;
      case 3: edit_deletetnote(); break;
    }

    
    let noteSize = 100;
    let judge = 700;
    translate(800,0);
    
    // preview box
    fill(0);
    strokeWeight(2);
    stroke(255);
    rect(0,0,400,800);
    line(noteSize,0,noteSize,800);
    line(noteSize*2,0,noteSize*2,800);
    line(noteSize*3,0,noteSize*3,800);
    
    // Judgement timing (box)
    fill(255,100);
    rect(0,judge,noteSize*4,30);
    
    // draw preview next to the board
    noStroke();
    for(let note of nowsheet)
    {
      let grid = time/10*judge/note.time;
      let delay = note.show*(judge)*1/note.time;
      
      if(grid-delay-30 >= 0 && grid-delay-30 <= judge)
      {
        fill(note.color);
        if(nowsheet.indexOf(note) == changenote && blink == -1) {noFill(); noStroke();}
        rect(noteSize*note.line, grid-delay, noteSize, 30, 8);
        fill(0);
        noStroke();
        textSize(30);
        textStyle(BOLD);
        if(nowsheet.indexOf(note) == changenote && blink == -1) {noFill(); noStroke();}
        text(note.key,noteSize*note.line+(noteSize/2),grid-delay+20);
        
        if(notenum_show == 1) 
        {
          textSize(15);
          text(nowsheet.indexOf(note),noteSize*note.line+(noteSize/2)-40,grid-delay+20);
        }
      }
    }
    pop();
  }
}

// x,y is location of button, s is size, m is toggle (on/off)
let editbutton = 
    {  
      notenum:{x:1400, y:480, s:20, m:1},
      toggle:{x:100, y:500, s:40, m:1}, jump:{x:200, y:500, s:40}, 
      add:{x:100, y:600, s:43}, edit:{x:200, y:600, s:43}, del:{x:300, y:600, s:43},
      back:{x:1400, y:800, s:[200, 60]}, sel:{x:1400, y:700, s:[200, 60]}
    };

// only DRAW buttons (Really interface)
function edit_interface()
{
  if(nowsong != undefined)
  {
    push();
    rectMode(CENTER);
    let notenum = editbutton.notenum;// show on/off the note num
    let toggle = editbutton.toggle;  // play, pause song
    let jump = editbutton.jump;      // jump to target location
    let add = editbutton.add;        // adding note
    let edit = editbutton.edit;      // editing note
    let del = editbutton.del;        // deleting note
    let back = editbutton.back;      // back to main menu
    let sel = editbutton.sel;        // select song
    
    // << BUTTON CIRCLE >>
    // notenum
    if(notenum.m == -1) {fill('red');} else {fill('lime');}
    circle(notenum.x, notenum.y, notenum.s);
    stroke('hotpink');
    strokeWeight(5);

    // toggle
    fill('pink');
    if(sqrt(pow(toggle.x-mouseX,2) + pow(toggle.y-mouseY,2)) <= toggle.s) {fill('plum');}
    circle(toggle.x, toggle.y ,toggle.s);
    
    // jump
    fill('pink');
    if(sqrt(pow(jump.x-mouseX,2) + pow(jump.y-mouseY,2)) <= jump.s) {fill('plum');}
    if(editplay == 1 || editnote != 0) {fill('dimgray'); stroke('black');} // if song is playing, disable the buttons
    circle(jump.x,jump.y,jump.s);

    // add
    fill('pink');
    if(sqrt(pow(add.x-mouseX,2) + pow(add.y-mouseY,2)) <= add.s) {fill('plum');}
    if(editplay == 1 || editnote != 0) {fill('dimgray'); stroke('black');} // if song is playing, disable the buttons
    circle(add.x,add.y,add.s);
    
    // edit
    fill('pink');
    if(sqrt(pow(edit.x-mouseX,2) + pow(edit.y-mouseY,2)) <= edit.s) {fill('plum');}
    if(editplay == 1 || editnote != 0) {fill('dimgray'); stroke('black');} // if song is playing, disable the buttons
    circle(edit.x,edit.y,edit.s);
    
    // delete
    fill('pink');
    if(sqrt(pow(del.x-mouseX,2) + pow(del.y-mouseY,2)) <= del.s) {fill('plum');}
    if(editplay == 1 || editnote != 0) {fill('dimgray'); stroke('black');} // if song is playing, disable the buttons
    circle(del.x,del.y,del.s);

    // back to menu
    fill('pink');
    if(mouseX >= back.x - back.s[0]/2 && mouseX <= back.x + back.s[0]/2 && mouseY >= back.y - back.s[1]/2 && mouseY <= back.y + back.s[1]/2) {fill('plum');}
    if(editplay == 1 || editnote != 0) {fill('dimgray'); stroke('black');} // if song is playing, disable the buttons
    rect(back.x, back.y, back.s[0], back.s[1],20);

    // select song again
    fill('pink');
    if(mouseX >= sel.x - sel.s[0]/2 && mouseX <= sel.x + sel.s[0]/2 && mouseY >= sel.y - sel.s[1]/2 && mouseY <= sel.y + sel.s[1]/2) {fill('plum');}
    if(editplay == 1 || editnote != 0) {fill('dimgray'); stroke('black');} // if song is playing, disable the buttons
    rect(sel.x, sel.y, sel.s[0], sel.s[1],20);
    
    // << BUTTON DESIGN >>
    //notenum
    fill(0);
    noStroke();
    textAlign(CENTER,CENTER);
    textStyle(BOLD);
    textSize(17);
    text('NUM',notenum.x,notenum.y);
    
    // toggle
    fill(0);
    if(editplay == -1) // if edit play is on
    {
      triangle(toggle.x+20, toggle.y, toggle.x-15, toggle.y-20, toggle.x-15, toggle.y+20);
    }
    else // if edit play is off
    {
      rect(toggle.x-10, toggle.y, 10, 50);
      rect(toggle.x+10, toggle.y, 10, 50);
    }
    
    // jump
    fill(0);
    textAlign(CENTER,CENTER);
    textStyle(BOLD);
    textSize(25);
    text('JUMP',jump.x,jump.y);
    
    // add
    fill(0);
    textAlign(CENTER,CENTER);
    textStyle(BOLD);
    textSize(25);
    text('ADD\nNOTE',add.x,add.y);
    
    // edit
    fill(0);
    textAlign(CENTER,CENTER);
    textStyle(BOLD);
    textSize(25);
    text('EDIT\nNOTE',edit.x,edit.y);
    
    // delete
    fill(0);
    textAlign(CENTER,CENTER);
    textStyle(BOLD);
    textSize(25);
    text('DEL\nNOTE',del.x,del.y);

    // back to menu
    fill(0);
    textAlign(CENTER,CENTER);
    textStyle(BOLD);
    textSize(40);
    text("▷Back", back.x, back.y);

    // select song
    fill(0);
    textAlign(CENTER,CENTER);
    textStyle(BOLD);
    textSize(40);
    text("▷Select", sel.x, sel.y);
    pop();
  }
}

function edit_playsong()
{
  if(editplay == 0 && nowsong != undefined) {nowsong.play(0,1,0.1,time/10); editplay = 1;}
  if(editplay == -1 && nowsong != undefined) {nowsong.stop();}
}


let edit_addnote_progress = 'select';
let inpnote = {};

// adding new note on the sheet
function edit_addnote()
{
  push();
  switch(edit_addnote_progress)
  {
    case 'select':
    {
      inpnote.show = float(prompt("When will the note showup? (In SECOND) \n -1 to cancel"));
      if(inpnote.show == -1) {edit_addnote_progress = 'select'; editnote = 0;}
      else if(inpnote.show >=0 && inpnote.show <= nowsong.duration()-0.1) {edit_addnote_progress = 'setlength';}
      break;
    }
      
    case 'setlength':
    {
      inpnote.hit = float(prompt("When will the note hit? (In SECOND)\n -1 to cancel"));
      inpnote.time = inpnote.hit - inpnote.show; 
      if(inpnote.time == -1) {edit_addnote_progress = 'select'; editnote = 0;}
      else if(inpnote.show + inpnote.time <= nowsong.duration()) {edit_addnote_progress = 'setcolor';}
      break;
    }
      
    case 'setcolor':
    {
      if(inpnote.time <= 0.3) {inpnote.color = 'magenta';}
      else if(inpnote.time > 0.3 && inpnote.time <= 0.6) {inpnote.color = 'red';}
      else if(inpnote.time > 0.6 && inpnote.time <= 1) {inpnote.color = 'darkorange';}
      else if(inpnote.time > 1 && inpnote.time <= 1.5) {inpnote.color = 'yellow';}
      else if(inpnote.time > 1.5 && inpnote.time <= 2) {inpnote.color = 'lime';}
      else if(inpnote.time > 2 && inpnote.time <= 3) {inpnote.color = 'skyblue';}
      else {inpnote.color = 'violet';}
      
      edit_addnote_progress = 'setkey';
      break;
    }
      
    case 'setkey':
    {
      inpnote.key = prompt("What key the note will be?\n ! to cancel");
      if(inpnote.key == '!') {edit_addnote_progress = 'select'; editnote = 0;}
      else if(Ds.indexOf(inpnote.key) != -1 || Fs.indexOf(inpnote.key) != -1 || Js.indexOf(inpnote.key) != -1 || Ks.indexOf(inpnote.key) != -1)
      {
        nowsheet.push(new note(inpnote.show,inpnote.time,inpnote.key,inpnote.color));
        edit_addnote_progress = 'select';
        editnote = 0;
      }
      break;
    }
  }
  pop();
}


let edit_changenote_progress = 'select';
let changenote;
let originalnote;

let changebutton = 
    {
      time:{x:480, y:610, s:[500, 40]},
      show:{x:460, y:770, s:40}, color:{x:610, y:770, s:40}, key:{x:760, y:770, s:40},
      ok:{x:1150, y:630, s:40}, no:{x:1150, y:730, s:40}
    };

// editing the note on sheet
function edit_editnote()
{
  push();
  translate(-750, -50);
  switch(edit_changenote_progress)
  {
    case 'select':
    {
      changenote = int(prompt("Input the note's number what you want to edit\n -1 to cancel"));
      if(changenote == -1) {edit_changenote_progress = 'select'; editnote = 0;}
      if(changenote >= 0 && changenote <= nowsheet.length-1) 
      {
        nownote = nowsheet[changenote];
        
        // if the change canceled, push original
        originalnote = new note(nownote.show, nownote.time, nownote.key, nownote.color);
        edit_changenote_progress = 'change';
      }
      break;
    }
    
    // draw NOTE editing interface
    case 'change':
    {
      push();
      let time = changebutton.time;
      let show = changebutton.show;
      let color = changebutton.color;
      let key = changebutton.key;
      let ok = changebutton.ok;
      let no = changebutton.no;
      
      strokeWeight(1);
      fill(255, 192, 203, 220);
      rect(400, 530, 800, 300, 20);
      fill(0);
      textSize(30);
      text("Note #"+changenote, 480, 570);
      
      // << BUTTON CIRCLE >>
      // time
      fill(255);
      rect(time.x, time.y, time.s[0], time.s[1]);
      for(let i = 0; i <= 50; i++)
      {
        line(time.x + i*(time.s[0]/50), time.y, time.x + i*(time.s[0]/50), time.y+time.s[1]);
        if(i% 5 == 0) { fill(0); textSize(15); text(i/10, time.x + i*(time.s[0]/50), time.y-10);}
      }
            
      // show
      fill('yellow');
      if(editplay == 1) {fill('dimgray');} // if song is playing, disable the buttons
      circle(show.x, show.y, show.s);
      
      // color
      circle(color.x, color.y, color.s);
      
      // key
      circle(key.x, key.y, key.s);
      
      // confirm change
      fill('lime');
      if(editplay == 1) {fill('dimgray');} // if song is playing, disable the buttons
      circle(ok.x, ok.y, ok.s);
      
      // cancel change
      fill('red');
      if(editplay == 1) {fill('dimgray');} // if song is playing, disable the buttons
      circle(no.x, no.y, no.s);
      
      // << BUTTON DESIGN & NOTE INFORMATION >>
      // time
      fill(0);
      textAlign(CENTER,CENTER);
      textStyle(BOLD);
      textSize(25);
      text('LEN',time.x-35,time.y+20);
      
      rect(time.x + (nownote.time*10) * (time.s[0]/50), time.y,10, time.s[1]);
      
      // show
      fill(0);
      textAlign(CENTER,CENTER);
      textStyle(BOLD);
      textSize(25);
      text('EDIT',show.x,show.y);
      
      text("Show\n"+nownote.show,show.x,show.y-70);
      
      // color
      fill(0);
      textAlign(CENTER,CENTER);
      textStyle(BOLD);
      textSize(25);
      text('EDIT',color.x,color.y);
      
      text("Color",color.x,color.y-70-10);
      fill(nownote.color);
      noStroke();
      text(nownote.color,color.x,color.y-70+15);
      
      // key
      fill(0);
      textAlign(CENTER,CENTER);
      textStyle(BOLD);
      textSize(25);
      text('EDIT',key.x,key.y);
      
      text("Key\n"+nownote.key,key.x,key.y-70);
      
      // confirm
      fill(0);
      textAlign(CENTER,CENTER);
      textStyle(BOLD);
      textSize(25);
      text('SAVE',ok.x, ok.y);
      
      // cancel
      fill(0);
      textAlign(CENTER,CENTER);
      textStyle(BOLD);
      textSize(25);
      text('NO\nSAVE',no.x, no.y);
      
      pop();
      break;
    }
  }
  pop();
}

let deletenote;

function edit_deletetnote()
{
  deletenote = int(prompt("Input the note's number what you want to delete\n -1 to cancel"));
  if(deletenote == -1) {editnote = 0;}
  else if(deletenote >= 0 && deletenote <= nowsheet.length-1) 
  {
    nowsheet.splice(deletenote,1);
    deletenote = -1;
    editnote = 0;
  }
}

// when editmode is ended, reset the time of songs