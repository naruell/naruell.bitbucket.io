// written by Duhwan.Kim
// Assignment Final Project
// Course CS099
// Spring 2019

let mainplay = 0; // -1 : pause || 0 : start SONG  || 1 : play
let sound = 0;
let maintime = 0;

let mainbutton = {
					play:{x:1200,y:250, s:[550,100]},
					edit:{x:1200,y:400, s:[550,100]},
					howtoplay:{x:1200,y:550, s:[550,100]},
					credit:{x:1200,y:700, s:[550,100]},
					bgm:{x:1900, y:850}
				  };

let inmaincircle;
let inmainplay;
let inmainedit;
let inmaincredit;
let inmainhowtoplay;

function mainmenu_draw()
{
	push();
	background(bg);
	// circle center of the mainmenu
	mainsong_level = amplitude.getLevel();
	mainsong_size1 = map(mainsong_level, 0, 1, 400, 3000);
	mainsong_size2 = map(mainsong_level, 0, 1, 430, 3500);

	inmaincircle = sqrt(pow(width/2-400-mouseX,2)+pow(height/2-mouseY,2)) <= mainsong_size2/2; 
	noStroke();
	fill('hotpink');
	ellipse(width/2-400, height/2, mainsong_size2, mainsong_size2);
	fill('pink');
	if(inmaincircle) {fill('plum');}
	ellipse(width/2-400, height/2, mainsong_size1, mainsong_size1);
	fill('hotpink');
	textSize(map(mainsong_level, 0, 1, 110, 800));
	textAlign(CENTER,CENTER);
	textStyle(BOLD);
	text('K-Sab',width/2-400, height/2);
	pop();

	push();
	let play = mainbutton.play;
	let edit = mainbutton.edit;
	let credit = mainbutton.credit;
	let bgm = mainbutton.bgm;
	let howtoplay = mainbutton.howtoplay;

	inmainplay = mouseX >= play.x && mouseX <= play.x+play.s[0] && mouseY >= play.y && mouseY <= play.y+play.s[1];
	inmainedit = mouseX >= edit.x && mouseX <= edit.x+edit.s[0] && mouseY >= edit.y && mouseY <= edit.y+edit.s[1];
	inmaincredit = mouseX >= credit.x && mouseX <= credit.x+credit.s[0] && mouseY >= credit.y && mouseY <= credit.y+credit.s[1];
	inmainhowtoplay = mouseX >= howtoplay.x && mouseX <= howtoplay.x+howtoplay.s[0] && mouseY >= howtoplay.y && mouseY <= howtoplay.y+howtoplay.s[1];

	// << BUTTON >>
	stroke('hotpink');
	strokeWeight(7);
	// play mode
	fill('pink');
	if(inmainplay) {fill('plum');}
	rect(play.x, play.y, play.s[0], play.s[1], 10);

	// edit mode
	fill('pink');
	if(inmainedit) {fill('plum');}
	rect(edit.x, edit.y, edit.s[0], edit.s[1], 10);

	// credit
	fill('pink');
	if(inmaincredit) {fill('plum');}
	rect(credit.x, credit.y, credit.s[0], credit.s[1], 10);

	// howtoplay
	fill('pink');
	if(inmainhowtoplay) {fill('plum');}
	rect(howtoplay.x, howtoplay.y, howtoplay.s[0], howtoplay.s[1], 10);

	// bgm on/off
	noStroke();
	fill('gray');
	quad(bgm.x, bgm.y, bgm.x+20, bgm.y+10, bgm.x+20, bgm.y-20, bgm.x, bgm.y-10);

	if(mainplay == 1) {rect(bgm.x+22, bgm.y-10, 3, 10); rect(bgm.x+28, bgm.y-15, 3, 20);}
	else if (mainplay == -1) {strokeWeight(5); stroke('gray'); line(bgm.x-5, bgm.y+20, bgm.x+30, bgm.y-25); line(bgm.x-5, bgm.y-25, bgm.x+30, bgm.y+20);}

	// <<DESIGN>>
	textAlign(LEFT);
	textStyle(BOLDITALIC);
	textSize(50);
	stroke('gray');
	strokeWeight(3);
	fill('hotpink');
	text("PLAY", play.x+20, play.y+70);

	textStyle(BOLDITALIC);
	textSize(50);
	stroke('gray');
	strokeWeight(3);
	fill('hotpink');
	text("EDITER MODE", edit.x+20, edit.y+70);

	textStyle(BOLDITALIC);
	textSize(50);
	stroke('gray');
	strokeWeight(3);
	fill('hotpink');
	text("HOW TO PLAY", howtoplay.x+20, howtoplay.y+70);

	textStyle(BOLDITALIC);
	textSize(50);
	stroke('gray');
	strokeWeight(3);
	fill('hotpink');
	text("CREDIT", credit.x+20, credit.y+70);
	pop();
}

function mainmenu_song()
{
	if(mainplay == 0)
	{
		if(nowsong == undefined) {nowsong = Spring;}
		maintime =  ((maintime/10) % nowsong.duration())*10;
		
		switch(nowsong)
		{
			case FunkRider: if(maintime/10 < 28.5) {maintime = 285;} nowsong.loop(0,1,sound,maintime/10); break;
			case Spring: if(maintime/10 < 45) {maintime = 450;} nowsong.loop(0,1,sound,maintime/10); break;
			case Star: if(maintime/10 < 36) {maintime = 360;} nowsong.loop(0,1,sound,maintime/10); break;
			case StrawberrySoldier: if(maintime/10 < 405) {maintime = 40.5;} nowsong.loop(0,1,sound,maintime/10); break;
		}
		mainplay = 1;
	}

	else if(mainplay == -1 && nowsong != undefined)
		{nowsong.stop();}
}

function mainmenu_credit()
{
	if(mouseIsPressed && inmaincredit)
	{
		push();
		fill('pink');
		stroke('hotpink');
		strokeWeight(15);
		rect(100,100,1000,700,100);

		noStroke();
		fill(0);
		textSize(60);
		textStyle(BOLDITALIC);
		text('<< CREDIT >>', 150, 150, 1000, 700);
		textSize(20);
		textStyle(BOLD);
		text(credit, 150, 220, 1000, 1200);
		pop();
	}
}

function mainmenu_howtoplay()
{
	if(mouseIsPressed && inmainhowtoplay)
	{
		push();
		fill('hotpink');
		noStroke();
		rect(100, 100, 1200, 650, 100);
		image(tutoimage, 200, 200, 1000, 450);

		pop();
	}
}

function mousePressed()
{
	if(mouseX >= mainbutton.bgm.x-30 && mouseX <= mainbutton.bgm.x+50 &&
	   mouseY >= mainbutton.bgm.y-50 && mouseY <= mainbutton.bgm.y+30 && state == 0)
	{
		if(mainplay == 1) { mainplay = -1;}
		else if(mainplay == -1) {mainplay = 0;}
	}

	if(state == 0 && inmaincircle) {mainclick.play(0,1,0.3);}
	if(state == 0 && inmainplay) {mainclick.play(0,1,0.3); nowsong.stop(); mainplay = 0; sound = 0; maintime = 0; state = 1; nowsong = undefined;}
	if(state == 0 && inmainedit) {mainclick.play(0,1,0.3); nowsong.stop(); mainplay = 0; sound = 0; maintime = 0; state = 2; nowsong = undefined;}
	if(state == 0 && inmaincredit) {mainclick.play(0,1,0.3);}
	if(state == 0 && inmainhowtoplay) {mainclick.play(0,1,0.3);}
}
// have to reset sound, maintime