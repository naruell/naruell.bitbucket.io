// written by Duhwan.Kim
// Assignment Final Project
// Course CS099
// Spring 2019

let key_to_line = {};

let Ds = "qwaszd";
let Fs = "ertfxc";
let Js = "yuighjvb";
let Ks = "opklnm";

for (let i = 0; i < Ds.length; ++i) {
  key_to_line[Ds[i]] = 0;
}for (let i = 0; i < Fs.length; ++i) {
  key_to_line[Fs[i]] = 1;
}for (let i = 0; i < Js.length; ++i) {
  key_to_line[Js[i]] = 2;
}for (let i = 0; i < Ks.length; ++i) {
  key_to_line[Ks[i]] = 3;
}

class note{
  constructor(show,time,key,color)
  {
    this.show = show;
    this.time = time;
    this.hit = this.show+this.time;
    this.color = color || this.convertcolor();
    this.rawkey = key;
    this.key = key.replace(' ','').replace('!','').replace('?','').replace('.','').replace('-','').replace('\'','').toLowerCase();
    this.progress = 0;
    
    this.line = key_to_line[this.key];
  }
  
  convertkey()
  {
    this.line = key_to_line[this.key];
  }
  
  converthit()
  {
    this.hit = this.show+this.time;
  }
  
  converttime(hittime)
  {
    this.time = hittime - this.show;
  }
  
  convertcolor()
  {
    if(this.time <= 0.3) {return 'magenta';}
    else if(this.time > 0.3 && this.time <= 0.6) {return 'red';}
    else if(this.time > 0.6 && this.time <= 1) {return 'darkorange';}
    else if(this.time > 1 && this.time <= 1.5) {return 'yellow';}
    else if(this.time > 1.5 && this.time <= 2) {return 'lime';}
    else if(this.time > 2 && this.time <= 3) {return 'skyblue';}
    else {return 'violet';}
  }
}

let savebox = [];
let rawdata;
function save_sheet(filename)
{
  for(let i = 0; i < nowsheet.length; i++)
  {
    let N = nowsheet[i];
    rawdata = N.show + "|" + N.time + "|" + N.key + "|" + N.color;
    savebox.push(rawdata);
    
  }
  saveStrings(savebox, filename+'.txt');
  savebox.length = 0;
  rawdata = undefined;
}

function load_sheet(STR, loadsheet)
{
  loadsheet.length = 0;
  for(let i =0; i < STR.length; i++)
  {
    if(STR[i] == '') {continue;}
    let N = split(STR[i], '|');
    loadsheet.push(new note(float(N[0]), float(N[1]), N[2]));
  }
}

temp = [];
function rearrange()
{
  for(let i = 0; i < nowsheet.length; i++)
  {
    temp.push(nowsheet[i]);
  }
  nowsheet.length = 0;

  for(let t = 0; t < nowsong.duration()*10; t ++)
  {
    for(let j = 0; j < temp.length; j++)
    {
      if(temp[j].hit*10 == t) {nowsheet.push(temp[j]);}
    }
  }
  temp.length = 0;
}