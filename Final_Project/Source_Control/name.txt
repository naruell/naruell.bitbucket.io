<<< GLOBAL VARIABLE >>>
time
start
blink
state

key_to_line

Ds
Fs 
Js
Ks

savebox
rawdata

gridx

editplay
editnote

nowsong
nowsheet
nownote
notenum_show

editbutton
edit_addnote_progress
inpnote

edit_changenote_progress
changenote
originalnote
changebutton 
deletenote

mainplay
sound
maintime
mainbutton

inmaincircle
inmainplay
inmainedit
inmaincredit

<<< GLOBAL FUNCTION >>>
preload()
draw()
mouseReleased()
keyReleased()
mouseDragged()

save_sheet(filename)
load_sheet(STR, loadsheet)

edit_draw(song, sheet)
  size
    scroll
    tracking_bar
    
      grid
      
      S
      grid
      
        grid
        delay
        
edit_interface()
  notenum
  toggle
  jump
  add
  edit
  del
edit_playsong()
edit_addnote()
    YN
edit_editnote()

mainmenu_draw()
mainmenu_song()
mainmenu_credit()
mouseClicked()

select_song()

<<< GLOBAL CLASS >>>
note
  constructor(show, time, key, color)
  this.show
  this.time
  this.hit
  his.color
  this.key
  this.progress
  convertkey()
  converthit()
  converttime(hittime)
  convertcolor()
    
        

<<< SONG & SHEET >>>
Blessing
Blessing_sheet