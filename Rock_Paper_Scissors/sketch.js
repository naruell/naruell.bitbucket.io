// wriiten by Duhwan.Kim
// Rock Paper Scissors Assignment 
// Cousrse CS099
// Spring 2019

let Target_Key = [];
let State = 0;

let Player1 = { Pressed:"no", Input:0, Result:'none', Key:[97, 115, 100] };
let Player2 = { Pressed:"no", Input:0, Result:'none', Key:[104, 106, 107] };
let RPS = { Result:'none', Matchup:'none' };
let Log = [];

function preload()
{
  Rock = loadImage("Image/rock.png");
  Scissors = loadImage("Image/scissors.png");
  Paper = loadImage("Image/paper.png");
  Unknown = loadImage("Image/unknown.png");
  Sellected = loadImage("Image/sellected.png");
  Win = loadImage("Image/win.png");
  _Draw = loadImage("Image/draw.png");
  Pressanykey = loadImage("Image/pressanykey.png");
  _Log = loadImage("Image/log.png");
}

function setup() {
  createCanvas(400, 400);
  for(let i=0;i<3;i++)
  {
    Target_Key.push(Player1.Key[i]);
  }
  for(let i=0;i<3;i++)
  {
    Target_Key.push(Player2.Key[i]);
  }  
}

function draw() {
  background(255);
  
  State = State_Check(Player1,Player2,RPS);
  switch(State)
  {
    case 0: Key_Input(); Key_Input_Draw(Player1,Player2); break;
    case 1: Judgement(); break;
    case 2: Show_And_Restart(); Show_And_Restart_Draw(Player1,Player2,RPS); break;
  }
  
  // Console_Check();
}

function Console_Check()
{
  console.log('State: '+State);
  console.log('P1: '+Player1.Input, 'P2: '+Player2.Input);
  console.log(RPS.Result);
  console.log(Target_Key);
  console.log(Log);
}

// Check Program's State; 0=Input | 1=Judgement | 2=Result
function State_Check(Player1,Player2,RPS)
{ 
  if(Player1.Pressed == 'no' || Player2.Pressed == 'no')
  {return 0;}
  
  else if(RPS.Result == 'none')
  {return 1;}
  
  else
  {return 2;}
}

// Input ASCII code to Rock, Paper, Scissors
function Key_Convert(key)
{
  if(key == Target_Key[0] || key == Target_Key[3])
  {return 'Rock';}
  
  else if(key == Target_Key[1] || key == Target_Key[4])
  { return 'Paper';}
  
  else if(key == Target_Key[2] || key == Target_Key[5])
  {return 'Scissors';}
}

// Wating for input each player's key
function Key_Input()
{
  setTimeout(function()
  {
    if(keyIsPressed && Target_Key.indexOf(keyCode) != -1)
    {
      if(Player1.Pressed == 'no' && Player1.Key.indexOf(keyCode) != -1)
      {
        Player1.Pressed = 'yes';
        Player1.Input = Key_Convert(keyCode);
      }
    
      else if(Player2.Pressed == 'no' && Player2.Key.indexOf(keyCode) != -1)
      {
        Player2.Pressed = 'yes';
        Player2.Input = Key_Convert(keyCode);
      }
    }
  }, 100)
}

// Calculate who is winner
function Judgement()
{
  if(Player1.Input == Player2.Input)
  {
    Player1.Result = 'DRAW';
    Player2.Result = 'DRAW';
    RPS.Result = 'BOTH';
  }
  
  else if(Player1.Input == 'Rock' && Player2.Input == 'Scissors' ||
          Player1.Input == 'Paper' && Player2.Input == 'Rock' ||
          Player1.Input == 'Scissors' && Player2.Input == 'Paper')
  {
    Player1.Result = 'WIN';
    Player2.Result = 'LOSE';
    RPS.Result = 'Player1'
  }
  
  else
  {
    Player1.Result = 'LOSE';
    Player2.Result = 'WIN';
    RPS.Result = 'Player2';
  }
  
  Log.unshift({ Result:RPS.Result, Matchup:[Player1.Input,Player2.Input] });
}

// Show Result and Waiting for pressing any key
function Show_And_Restart()
{
  setTimeout(function()
  {
    if(keyIsPressed)
    {
      Player1.Pressed = Player2.Pressed = 'no';
      Player1.Input = Player2.Input = 0;
      Player1.Result = Player2.Result = RPS.Result = RPS.Matchup = 'none';
    }
  },700);

}

function Key_Input_Draw(Player1,Player2)
{
  let P1 = Player1.Input;
  let P2 = Player2.Input;
  
  image(_Log,0,height-50,100,50);
  
  if(P1 == 0) {image(Unknown,0,0,150,200);}
  else {image(Sellected,0,0,150,200);}
  if(P2 == 0) {image(Unknown,250,0,150,200);}
  else {image(Sellected,250,0,150,200);}
  
  if(keyIsPressed && keyCode == 113) Draw_Log();
}

function Show_And_Restart_Draw(Player1,Player2,RPS)
{
  let P1 = Player1.Input;
  let P2 = Player2.Input;
  
  switch(P1)
  {
    case "Rock": image(Rock,0,0,200,200); break;
    case "Scissors": image(Scissors,0,0,200,200); break;
    case "Paper": image(Paper,0,0,200,200); break;
  }
  
   switch(P2)
  {
    case "Rock": image(Rock,200,0,200,200); break;
    case "Scissors": image(Scissors,200,0,200,200); break;
    case "Paper": image(Paper,200,0,200,200); break;
  }
  
  switch(RPS.Result)
  {
    case "Player1" : image(Win,0,200,200,100); break;
    case "Player2" : image(Win,200,200,200,100); break;
    case "BOTH" : image(_Draw,70,200); break;
  }
  image(Pressanykey,0,height-50,200,50);
}

function Draw_Log()
{
  push();
  rectMode(CENTER);
  textSize(20);
  textAlign(CENTER);
  rect(width/2,height/2,300,400);
  text('< Log >',width/2,40);
  for(i=0; i<Log.length; i++)
  {
    text('[ '+i+' ] '+'Winner: '+Log[i].Result,width/2,80+70*i);
    text('MatchUp: '+Log[i].Matchup,width/2,110+70*i);
  }
  pop();
}
