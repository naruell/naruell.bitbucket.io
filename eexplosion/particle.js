class particle{
  constructor(x,y,speed,direction,radius,color)
  {
    this.position = new Vec2(x,y);
    this.velocity = new Vec2(0,0);
    this.gravity = new Vec2(0,0.1);
    this.radius = radius;
    this.direction = direction;
    this.speed = speed;
    this.color = color || 'red';
    
    this.velocity.setLength(this.speed);
    this.velocity.setAngle(this.direction);
  }
  
  update()
  {
    this.position.addTo(this.velocity);
  }
  
  draw()
  {
    push();
    fill(this.color);
    circle(this.position.x,this.position.y,this.radius);
    pop();
  }
  
  accelerate()
  {
    this.velocity.addTo(this.gravity);
  }

}