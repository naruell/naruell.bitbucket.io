class Vec2{
  constructor(x,y)
  {
    this.x = x;
    this.y = y;
  }
  
  getAngle()
  {
    return atan2(this.y,this.x);
  }
  
  setAngle(angle)
  {
    this.x = this.getLength()*cos(angle);
    this.y = this.getLength()*sin(angle);
  }
    
  getLength()
  {
    return sqrt(pow(this.x,2)+pow(this.y,2));
  }
  
  setLength(length)
  {
    this.x = length*cos(this.getAngle());
    this.y = length*sin(this.getAngle());
  }
  
  add(Vec)
  {
    return new Vec2(this.x+Vec.x, this.y+Vec.y);
  }
  
  addTo(Vec)
  {
    this.x += Vec.x;
    this.y += Vec.y;
  }
  
  subtract(Vec)
  {
    return new Vec2(this.x-Vec.x, this.y-Vec.y);
  }
  
  subtractFrom(Vec)
  {
    this.x -= Vec.x;
    this.y -= Vec.y;
  }
  
  multiply(scalar)
  {
    return new Vec2(scalar*this.x, scalar*this.y);
  }
  
  multiplyBy(scalar)
  {
    this.x *= scalar;
    this.y *= scalar;
  }
  
  divide(scalar)
  {
    return new Vec2(this.x/scalar, this.y/scalar);
  }
  
  divideBy(scalar)
  {
    this.x /= scalar;
    this.y /= scalar;
  }
}