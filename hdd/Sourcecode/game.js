class Game {

    constructor() {

        this.gameState = GAME_STATE.mainMenu; //To be changed later

        this.world = new World;

        this.prevMillis = 0;
        this.curMillis = 0;
        this.deltaTime = 0;

        this.simSpeed = 6;

        this.isBlinked = 1;
    }

    update() {

        this.updateDeltaTime();

        switch (this.gameState) {

            case GAME_STATE.gameover:
                break;
            case GAME_STATE.mainMenu:
                break;
            case GAME_STATE.preparation:
                break;
            case GAME_STATE.mainGame:
                this.world.update();
                break;
            case GAME_STATE.clear:
                break;

        }

    }

    good() {
        COLOR_BACKGROUND = 255;
        let INTERFACE = rosterInterface;
        let survivor = 0;
        let imageList = [];
        push();

        image(goodImage, 0, 0);

        for(let i = 0; i < game.world.roster.currentRoster.length; ++i)
        {
            let member = game.world.roster.currentRoster[i];
        
            if(member.isAlive)
            {
                imageList.push([member.bodyImage]);

                switch (member.highestProperties.length) {
                case 1:
                    switch (member.highestProperties[0]) {
                        case "sword": imageList[i].push(swordArmImage); break;
                        case "shield": imageList[i].push(shieldArmImage); break;
                        case "bow": imageList[i].push(archerArmImage); break;
                    }
                    break;

                case 2:
                    switch (member.highestProperties[0] + member.highestProperties[1]) {
                        case "swordshield": imageList[i].push(swordshieldArmImage); break;
                        case "shieldbow": imageList[i].push(shieldbowArmImage); break;
                        case "swordbow": imageList[i].push(swordarcherArmImage); break;
                    }
                    break;

                case 3:
                    if (member.properties[0].amount == 0) { imageList[i].push(defaultArmImage); }
                    else { imageList[i].push(honjongArmImage); }
                    break;
                }
                ++survivor;
            }
        }

        let correction = 800 / survivor;

        for(let i = 0; i < game.world.roster.currentRoster.length; ++i)
        {
            for (let j = 0; j < imageList[i].length; ++j) {
            image(imageList[i][j],
                correction * i + INTERFACE.imageRelativePosX * width / 100,
                INTERFACE.imageRelativePosY * height / 100);
            }
        }  

        

        pop();
    }

    bad() {
        COLOR_BACKGROUND = 0;
        image(badImage, 0, 0);
    }

    draw() {

        background(COLOR_BACKGROUND);

        switch (this.gameState) {

            case GAME_STATE.gameover:
                this.bad();
                break;
            case GAME_STATE.mainMenu:
                mainMenu.draw();
                break;
            case GAME_STATE.preparation:
                this.world.roster.generateRoster();
                break;
            case GAME_STATE.mainGame:
                this.world.draw();
                break;
            case GAME_STATE.clear:
                this.good();
                break;

        }

    }

    updateDeltaTime() {

        this.curMillis = millis();
        this.deltaTime = (this.curMillis - this.prevMillis) / 16.6;
        this.prevMillis = this.curMillis;

        this.deltaTime *= this.simSpeed;

    }

}



let game;



function setup() {

    game = new Game;
    generateItem();

    backgroundMusic.loop();

    setInterval(() => game.isBlinked *= -1, 500);
    createCanvas(BASE_WINDOW_WIDTH, BASE_WINDOW_HEIGHT);

    createRosterButtons();

    textAlign(LEFT, TOP);
    textFont(bloodcrowFont);
    //noSmooth();

}

function draw() {
    game.update();
    game.draw();
}

// function windowResized() {
//     resizeCanvas(windowWidth, windowHeight);
// }