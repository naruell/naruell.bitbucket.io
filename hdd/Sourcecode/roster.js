class Roster {

	constructor() {

		this.progress = 0;

		this.depth = 0;
		this.maxDepth = 30;

		this.rollBackTimer = 0;
		this.rollBackLength = 30;

		this.currentRoster = [];
		this.rosterState = ROSTER_STATE.walk;

		this.maximumRosterSize = 3;	// 최대 파티 사이즈
		this.currentRosterIndex = 0;// 몇번째 캐릭 편집중인지

		this.selectedHero = -1;    // 절 대 건 들 지 마

		this.showedUpLoots = [];
		this.selectedLoot = null;
		this.selectedLootIndex = -1;
		this.isLootUsed = false;
		this.posChangeDone = false;

		this.currentCastle = 0;

		this.clashInterval = 5;
		this.battleTimer = 0;

	}

	update() {

		switch (this.rosterState) {
			case ROSTER_STATE.walk:
				this.walk();
				break;
			case ROSTER_STATE.battle:
				this.battle();
				break;
			case ROSTER_STATE.rest:
				this.rest();
				break;
			case ROSTER_STATE.rollback:

				// ---------------------------------------------------------------------needs care of
				let isRollBackComplete = true;
				for (let castle of game.world.castles) {
					if (castle.sink > 0 || castle.shake > 0)
						isRollBackComplete = false;
				}

				if (this.progress > 0) {
					this.progress -= game.deltaTime / 20;
					isRollBackComplete = false;
				}

				if (isRollBackComplete && this.rollBackTimer > this.rollBackLength) {
					game.world.rollBacksLeft--;
					game.gameState = GAME_STATE.preparation;
					game.world.initialize();
				} else {
					this.rollBackTimer += game.deltaTime / 60;
				}

				break;
		}

		for (let hero of this.currentRoster) {

			hero.update();

		}

	}

	walk() {

		this.progress += game.deltaTime / 60;
		this.currentRoster[HERO_POSITION.front].isHeroFighting = false;
		this.currentRoster[HERO_POSITION.center].isHeroFighting = false;
		this.currentRoster[HERO_POSITION.back].isHeroFighting = false;
	}

	// Just Calculation, Not animation, etc
	battle() {



		if (this.depth < this.maxDepth * this.currentCastle.size) {
			this.progress += game.deltaTime / 60;
			this.depth += game.deltaTime / (7);
		} else {



			//Real battle goes on here


			if (this.battleTimer > this.clashInterval) {



				switch (this.currentCastle.size) {
					case CASTLE_SIZE.small:
						if (this.currentRoster[HERO_POSITION.front].isAlive) {
							this.currentRoster[HERO_POSITION.front].isHeroFighting = true;
							switch (this.currentCastle.properties) {
								case CASTLE_PROPERTIES.sword:
									this.currentRoster[HERO_POSITION.front].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.front].properties[2].amount / this.currentCastle.maxHealth));
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.front].properties[2].amount;
									break;
								case CASTLE_PROPERTIES.shield:
									this.currentRoster[HERO_POSITION.front].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.front].properties[0].amount / this.currentCastle.maxHealth));
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.front].properties[0].amount;
									break;
								case CASTLE_PROPERTIES.bow:
									this.currentRoster[HERO_POSITION.front].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.front].properties[1].amount / this.currentCastle.maxHealth));
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.front].properties[1].amount;
									break;
								case CASTLE_PROPERTIES.swordShield:
									if (1 - this.currentRoster[HERO_POSITION.front].properties[2].amount *
										this.currentRoster[HERO_POSITION.front].properties[0].amount / 2 / this.currentCastle.maxHealth <= 0) {
										this.currentRoster[HERO_POSITION.front].health -= 1;
									} else {
										this.currentRoster[HERO_POSITION.front].health -= floor(this.currentCastle.damage *
											(1 - this.currentRoster[HERO_POSITION.front].properties[2].amount *
												this.currentRoster[HERO_POSITION.front].properties[0].amount / 2 / this.currentCastle.maxHealth));
									}
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.front].properties[2].amount *
										this.currentRoster[HERO_POSITION.front].properties[0].amount / 2);
									break;
								case CASTLE_PROPERTIES.swordBow:
									if (1 - this.currentRoster[HERO_POSITION.front].properties[2].amount *
										this.currentRoster[HERO_POSITION.front].properties[1].amount / 2 / this.currentCastle.maxHealth <= 0) {
										this.currentRoster[HERO_POSITION.front].health -= 1;
									} else {
										this.currentRoster[HERO_POSITION.front].health -= floor(this.currentCastle.damage *
											(1 - this.currentRoster[HERO_POSITION.front].properties[2].amount *
												this.currentRoster[HERO_POSITION.front].properties[1].amount / 2 / this.currentCastle.maxHealth));
									}
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.front].properties[2].amount *
										this.currentRoster[HERO_POSITION.front].properties[1].amount / 2);
									break;
								case CASTLE_PROPERTIES.shieldBow:
									if (1 - this.currentRoster[HERO_POSITION.front].properties[0].amount *
										this.currentRoster[HERO_POSITION.front].properties[1].amount / 2 / this.currentCastle.maxHealth <= 0) {
										this.currentRoster[HERO_POSITION.front].health -= 1;
									} else {
										this.currentRoster[HERO_POSITION.front].health -= floor(this.currentCastle.damage *
											(1 - this.currentRoster[HERO_POSITION.front].properties[0].amount *
												this.currentRoster[HERO_POSITION.front].properties[1].amount / 2 / this.currentCastle.maxHealth));
									}
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.front].properties[0].amount *
										this.currentRoster[HERO_POSITION.front].properties[1].amount / 2);
									break;
							}

							if (this.currentRoster[HERO_POSITION.front].health <= 0) {
								this.currentRoster[HERO_POSITION.front].health = 0;
								this.currentRoster[HERO_POSITION.front].isAlive = false;
								this.currentRoster[HERO_POSITION.front].isHeroFighting = false;
								if (this.currentRoster[HERO_POSITION.center].isAlive) {
									this.currentRoster[HERO_POSITION.center].isHeroFighting = true;
								} else if (this.currentRoster[HERO_POSITION.back].isAlive) {
									this.currentRoster[HERO_POSITION.back].isHeroFighting = true;
								}
							}
						} else if (this.currentRoster[HERO_POSITION.center].isAlive) {
							this.currentRoster[HERO_POSITION.center].isHeroFighting = true;

							switch (this.currentCastle.properties) {
								case CASTLE_PROPERTIES.sword:
									this.currentRoster[HERO_POSITION.center].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.center].properties[2].amount / this.currentCastle.maxHealth));
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.center].properties[2].amount;
									break;
								case CASTLE_PROPERTIES.shield:
									this.currentRoster[HERO_POSITION.center].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.center].properties[0].amount / this.currentCastle.maxHealth));
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.center].properties[0].amount;
									break;
								case CASTLE_PROPERTIES.bow:
									this.currentRoster[HERO_POSITION.center].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.center].properties[1].amount / this.currentCastle.maxHealth));
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.center].properties[1].amount;
									break;
								case CASTLE_PROPERTIES.swordShield:
									if (1 - this.currentRoster[HERO_POSITION.center].properties[2].amount *
										this.currentRoster[HERO_POSITION.center].properties[0].amount / 2 / this.currentCastle.maxHealth <= 0) {
										this.currentRoster[HERO_POSITION.center].health -= 1;
									} else {
										this.currentRoster[HERO_POSITION.center].health -= floor(this.currentCastle.damage *
											(1 - this.currentRoster[HERO_POSITION.center].properties[2].amount *
												this.currentRoster[HERO_POSITION.center].properties[0].amount / 2 / this.currentCastle.maxHealth))
									}
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.center].properties[2].amount *
										this.currentRoster[HERO_POSITION.center].properties[0].amount / 2);
									break;
								case CASTLE_PROPERTIES.swordBow:
									if (1 - this.currentRoster[HERO_POSITION.center].properties[2].amount *
										this.currentRoster[HERO_POSITION.center].properties[1].amount / 2 / this.currentCastle.maxHealth <= 0) {
										this.currentRoster[HERO_POSITION.center].health -= 1;
									} else {
										this.currentRoster[HERO_POSITION.center].health -= floor(this.currentCastle.damage *
											(1 - this.currentRoster[HERO_POSITION.center].properties[2].amount *
												this.currentRoster[HERO_POSITION.center].properties[1].amount / 2 / this.currentCastle.maxHealth));
									}
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.center].properties[2].amount *
										this.currentRoster[HERO_POSITION.center].properties[1].amount / 2);
									break;
								case CASTLE_PROPERTIES.shieldBow:
									if (1 - this.currentRoster[HERO_POSITION.center].properties[0].amount *
										this.currentRoster[HERO_POSITION.center].properties[1].amount / 2 / this.currentCastle.maxHealth <= 0) {
										this.currentRoster[HERO_POSITION.center].health -= 1;
									} else {
										this.currentRoster[HERO_POSITION.center].health -= floor(this.currentCastle.damage *
											(1 - this.currentRoster[HERO_POSITION.center].properties[0].amount *
												this.currentRoster[HERO_POSITION.center].properties[1].amount / 2 / this.currentCastle.maxHealth));
									}
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.center].properties[0].amount *
										this.currentRoster[HERO_POSITION.center].properties[1].amount / 2);
									break;
							}
							if (this.currentRoster[HERO_POSITION.center].health <= 0) {
								this.currentRoster[HERO_POSITION.center].health = 0;
								this.currentRoster[HERO_POSITION.center].isAlive = false;
								this.currentRoster[HERO_POSITION.center].isHeroFighting = false;
								if (this.currentRoster[HERO_POSITION.back].isAlive) {
									this.currentRoster[HERO_POSITION.back].isHeroFighting = true;
								}
							}
						} else {
							this.currentRoster[HERO_POSITION.back].isHeroFighting = true;
							switch (this.currentCastle.properties) {
								case CASTLE_PROPERTIES.sword:
									this.currentRoster[HERO_POSITION.back].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.back].properties[2].amount / this.currentCastle.maxHealth));
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.back].properties[2].amount;
									break;
								case CASTLE_PROPERTIES.shield:
									this.currentRoster[HERO_POSITION.back].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.back].properties[0].amount / this.currentCastle.maxHealth));
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.back].properties[0].amount;
									break;
								case CASTLE_PROPERTIES.bow:
									this.currentRoster[HERO_POSITION.back].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.back].properties[1].amount / this.currentCastle.maxHealth));
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.back].properties[1].amount;
									break;
								case CASTLE_PROPERTIES.swordShield:
									if (1 - this.currentRoster[HERO_POSITION.back].properties[2].amount *
										this.currentRoster[HERO_POSITION.back].properties[0].amount / 2 / this.currentCastle.maxHealth <= 0) {
										this.currentRoster[HERO_POSITION.back].health -= 1;
									} else {
										this.currentRoster[HERO_POSITION.back].health -= floor(this.currentCastle.damage *
											(1 - this.currentRoster[HERO_POSITION.back].properties[2].amount *
												this.currentRoster[HERO_POSITION.back].properties[0].amount / 2 / this.currentCastle.maxHealth));
									}
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.back].properties[2].amount *
										this.currentRoster[HERO_POSITION.back].properties[0].amount / 2);
									break;
								case CASTLE_PROPERTIES.swordBow:
									if (1 - this.currentRoster[HERO_POSITION.back].properties[2].amount *
										this.currentRoster[HERO_POSITION.back].properties[1].amount / 2 / this.currentCastle.maxHealth <= 0) {
										this.currentRoster[HERO_POSITION.back].health -= 1;
									} else {
										this.currentRoster[HERO_POSITION.back].health -= floor(this.currentCastle.damage *
											(1 - this.currentRoster[HERO_POSITION.back].properties[2].amount *
												this.currentRoster[HERO_POSITION.back].properties[1].amount / 2 / this.currentCastle.maxHealth));
									}
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.back].properties[2].amount *
										this.currentRoster[HERO_POSITION.back].properties[1].amount / 2);
									break;
								case CASTLE_PROPERTIES.shieldBow:
									if (1 - this.currentRoster[HERO_POSITION.back].properties[0].amount *
										this.currentRoster[HERO_POSITION.back].properties[1].amount / 2 / this.currentCastle.maxHealth <= 0) {
										this.currentRoster[HERO_POSITION.back].health -= 1;
									} else {
										this.currentRoster[HERO_POSITION.back].health -= floor(this.currentCastle.damage *
											(1 - this.currentRoster[HERO_POSITION.back].properties[0].amount *
												this.currentRoster[HERO_POSITION.back].properties[1].amount / 2 / this.currentCastle.maxHealth));
									}
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.back].properties[0].amount *
										this.currentRoster[HERO_POSITION.back].properties[1].amount / 2);
									break;
							}
							if (this.currentRoster[HERO_POSITION.back].health <= 0) {
								this.currentRoster[HERO_POSITION.back].health = 0;
								this.currentRoster[HERO_POSITION.back].isAlive = false;
								this.currentRoster[HERO_POSITION.back].isHeroFighting = false;
							}
						}
						break;
					case CASTLE_SIZE.big:
						if (this.currentRoster[HERO_POSITION.front].isAlive) {
							if (this.currentRoster[HERO_POSITION.center].isAlive == false) {
								this.currentRoster[HERO_POSITION.front].isHeroFighting = true;
								this.currentRoster[HERO_POSITION.back].isHeroFighting = true;

								switch (this.currentCastle.properties) {
									case CASTLE_PROPERTIES.sword:
										this.currentRoster[HERO_POSITION.front].health -= floor((this.currentCastle.damage *
											(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
												this.currentRoster[HERO_POSITION.back].properties[2].amount) / this.currentCastle.maxHealth)) / 2);
										this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
											(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
												this.currentRoster[HERO_POSITION.back].properties[2].amount) / this.currentCastle.maxHealth)) / 2);
										this.currentCastle.health -= this.currentRoster[HERO_POSITION.front].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount;
										break;
									case CASTLE_PROPERTIES.shield:
										this.currentRoster[HERO_POSITION.front].health -= floor((this.currentCastle.damage *
											(1 - (this.currentRoster[HERO_POSITION.front].properties[0].amount +
												this.currentRoster[HERO_POSITION.back].properties[0].amount) / this.currentCastle.maxHealth)) / 2);
										this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
											(1 - (this.currentRoster[HERO_POSITION.front].properties[0].amount +
												this.currentRoster[HERO_POSITION.back].properties[0].amount) / this.currentCastle.maxHealth)) / 2);
										this.currentCastle.health -= this.currentRoster[HERO_POSITION.front].properties[0].amount +
											this.currentRoster[HERO_POSITION.back].properties[0].amount;
										break;
									case CASTLE_PROPERTIES.bow:
										this.currentRoster[HERO_POSITION.front].health -= floor((this.currentCastle.damage *
											(1 - (this.currentRoster[HERO_POSITION.front].properties[1].amount +
												this.currentRoster[HERO_POSITION.back].properties[1].amount) / this.currentCastle.maxHealth)) / 2);
										this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
											(1 - (this.currentRoster[HERO_POSITION.front].properties[1].amount +
												this.currentRoster[HERO_POSITION.back].properties[1].amount) / this.currentCastle.maxHealth)) / 2);
										this.currentCastle.health -= this.currentRoster[HERO_POSITION.front].properties[1].amount +
											this.currentRoster[HERO_POSITION.back].properties[1].amount;
										break;
									case CASTLE_PROPERTIES.swordShield:
										if (1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[0].amount +
												this.currentRoster[HERO_POSITION.back].properties[0].amount) / 8 / this.currentCastle.maxHealth <= 0) {
											this.currentRoster[HERO_POSITION.front].health -= 1;
											this.currentRoster[HERO_POSITION.back].health -= 1;
										} else {
											this.currentRoster[HERO_POSITION.front].health -= floor((this.currentCastle.damage *
												(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
													this.currentRoster[HERO_POSITION.back].properties[2].amount) *
													(this.currentRoster[HERO_POSITION.front].properties[0].amount +
														this.currentRoster[HERO_POSITION.back].properties[0].amount) / 8 / this.currentCastle.maxHealth)) / 2);
											this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
												(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
													this.currentRoster[HERO_POSITION.back].properties[2].amount) *
													(this.currentRoster[HERO_POSITION.front].properties[0].amount +
														this.currentRoster[HERO_POSITION.back].properties[0].amount) / 8 / this.currentCastle.maxHealth)) / 2);
										}
										this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.front].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[0].amount +
												this.currentRoster[HERO_POSITION.back].properties[0].amount) / 8;
										break;
									case CASTLE_PROPERTIES.swordBow:
										if (1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[1].amount +
												this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8 / this.currentCastle.maxHealth <= 0) {
											this.currentCastle.damage = 1;
											this.currentRoster[HERO_POSITION.front].health -= 1;
											this.currentRoster[HERO_POSITION.back].health -= 1;
										} else {
											this.currentRoster[HERO_POSITION.front].health -= floor((this.currentCastle.damage *
												(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
													this.currentRoster[HERO_POSITION.back].properties[2].amount) *
													(this.currentRoster[HERO_POSITION.front].properties[1].amount +
														this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8 / this.currentCastle.maxHealth)) / 2);
											this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
												(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
													this.currentRoster[HERO_POSITION.back].properties[2].amount) *
													(this.currentRoster[HERO_POSITION.front].properties[1].amount +
														this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8 / this.currentCastle.maxHealth)) / 2);
										}
										this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.front].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[1].amount +
												this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8;
										break;
									case CASTLE_PROPERTIES.shieldBow:
										if (1 - (this.currentRoster[HERO_POSITION.front].properties[0].amount +
											this.currentRoster[HERO_POSITION.back].properties[0].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[1].amount +
												this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8 / this.currentCastle.maxHealth <= 0) {
											this.currentCastle.damage = 1;
											this.currentRoster[HERO_POSITION.front].health -= 1;
											this.currentRoster[HERO_POSITION.back].health -= 1;
										} else {
											this.currentRoster[HERO_POSITION.front].health -= floor((this.currentCastle.damage *
												(1 - (this.currentRoster[HERO_POSITION.front].properties[0].amount +
													this.currentRoster[HERO_POSITION.back].properties[0].amount) *
													(this.currentRoster[HERO_POSITION.front].properties[1].amount +
														this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8 / this.currentCastle.maxHealth)) / 2);
											this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
												(1 - (this.currentRoster[HERO_POSITION.front].properties[0].amount +
													this.currentRoster[HERO_POSITION.back].properties[0].amount) *
													(this.currentRoster[HERO_POSITION.front].properties[1].amount +
														this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8 / this.currentCastle.maxHealth)) / 2);
										}
										this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.front].properties[0].amount +
											this.currentRoster[HERO_POSITION.back].properties[0].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[1].amount +
												this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8;
										break;
								}
								if (this.currentRoster[HERO_POSITION.front].health <= 0) {
									this.currentRoster[HERO_POSITION.front].health = 0;
									this.currentRoster[HERO_POSITION.front].isAlive = false;
									this.currentRoster[HERO_POSITION.front].isHeroFighting = false;
								}
								if (this.currentRoster[HERO_POSITION.back].health <= 0) {
									this.currentRoster[HERO_POSITION.back].health = 0;
									this.currentRoster[HERO_POSITION.back].isAlive = false;
									this.currentRoster[HERO_POSITION.back].isHeroFighting = false;
								}
							} else {
								this.currentRoster[HERO_POSITION.front].isHeroFighting = true;
								this.currentRoster[HERO_POSITION.center].isHeroFighting = true;
								switch (this.currentCastle.properties) {
									case CASTLE_PROPERTIES.sword:
										this.currentRoster[HERO_POSITION.front].health -= floor((this.currentCastle.damage *
											(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
												this.currentRoster[HERO_POSITION.center].properties[2].amount) / this.currentCastle.maxHealth)) / 2);
										this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
											(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
												this.currentRoster[HERO_POSITION.center].properties[2].amount) / this.currentCastle.maxHealth)) / 2);
										this.currentCastle.health -= this.currentRoster[HERO_POSITION.front].properties[2].amount +
											this.currentRoster[HERO_POSITION.center].properties[2].amount;
										break;
									case CASTLE_PROPERTIES.shield:
										this.currentRoster[HERO_POSITION.front].health -= floor((this.currentCastle.damage *
											(1 - (this.currentRoster[HERO_POSITION.front].properties[0].amount +
												this.currentRoster[HERO_POSITION.center].properties[0].amount) / this.currentCastle.maxHealth)) / 2);
										this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
											(1 - (this.currentRoster[HERO_POSITION.front].properties[0].amount +
												this.currentRoster[HERO_POSITION.center].properties[0].amount) / this.currentCastle.maxHealth)) / 2);
										this.currentCastle.health -= this.currentRoster[HERO_POSITION.front].properties[0].amount +
											this.currentRoster[HERO_POSITION.center].properties[0].amount;
										break;
									case CASTLE_PROPERTIES.bow:
										this.currentRoster[HERO_POSITION.front].health -= floor((this.currentCastle.damage *
											(1 - (this.currentRoster[HERO_POSITION.front].properties[1].amount +
												this.currentRoster[HERO_POSITION.center].properties[1].amount) / this.currentCastle.maxHealth)) / 2);
										this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
											(1 - (this.currentRoster[HERO_POSITION.front].properties[1].amount +
												this.currentRoster[HERO_POSITION.center].properties[1].amount) / this.currentCastle.maxHealth)) / 2);
										this.currentCastle.health -= this.currentRoster[HERO_POSITION.front].properties[1].amount +
											this.currentRoster[HERO_POSITION.center].properties[1].amount;
										break;
									case CASTLE_PROPERTIES.swordShield:
										if (1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
											this.currentRoster[HERO_POSITION.center].properties[2].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[0].amount +
												this.currentRoster[HERO_POSITION.center].properties[0].amount) / 8 / this.currentCastle.maxHealth <= 0) {
											this.currentRoster[HERO_POSITION.front].health -= 1;
											this.currentRoster[HERO_POSITION.center].health -= 1;
										} else {
											this.currentRoster[HERO_POSITION.front].health -= floor((this.currentCastle.damage *
												(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
													this.currentRoster[HERO_POSITION.center].properties[2].amount) *
													(this.currentRoster[HERO_POSITION.front].properties[0].amount +
														this.currentRoster[HERO_POSITION.center].properties[0].amount) / 8 / this.currentCastle.maxHealth)) / 2);
											this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
												(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
													this.currentRoster[HERO_POSITION.center].properties[2].amount) *
													(this.currentRoster[HERO_POSITION.front].properties[0].amount +
														this.currentRoster[HERO_POSITION.center].properties[0].amount) / 8 / this.currentCastle.maxHealth)) / 2);
										}
										this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.front].properties[2].amount +
											this.currentRoster[HERO_POSITION.center].properties[2].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[0].amount +
												this.currentRoster[HERO_POSITION.center].properties[0].amount) / 8;
										break;
									case CASTLE_PROPERTIES.swordBow:
										if (1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
											this.currentRoster[HERO_POSITION.center].properties[2].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[1].amount +
												this.currentRoster[HERO_POSITION.center].properties[1].amount) / 8 / this.currentCastle.maxHealth <= 0) {
											this.currentRoster[HERO_POSITION.front].health -= 1;
											this.currentRoster[HERO_POSITION.center].health -= 1;
										} else {
											this.currentRoster[HERO_POSITION.front].health -= floor((this.currentCastle.damage *
												(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
													this.currentRoster[HERO_POSITION.center].properties[2].amount) *
													(this.currentRoster[HERO_POSITION.front].properties[1].amount +
														this.currentRoster[HERO_POSITION.center].properties[1].amount) / 8 / this.currentCastle.maxHealth)) / 2);
											this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
												(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
													this.currentRoster[HERO_POSITION.center].properties[2].amount) *
													(this.currentRoster[HERO_POSITION.front].properties[1].amount +
														this.currentRoster[HERO_POSITION.center].properties[1].amount) / 8 / this.currentCastle.maxHealth)) / 2);
										}
										this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.front].properties[2].amount +
											this.currentRoster[HERO_POSITION.center].properties[2].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[1].amount +
												this.currentRoster[HERO_POSITION.center].properties[1].amount) / 8;
										break;
									case CASTLE_PROPERTIES.shieldBow:
										if (1 - (this.currentRoster[HERO_POSITION.front].properties[0].amount +
											this.currentRoster[HERO_POSITION.center].properties[0].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[1].amount +
												this.currentRoster[HERO_POSITION.center].properties[1].amount) / 8 / this.currentCastle.maxHealth <= 0) {
											this.currentRoster[HERO_POSITION.front].health -= 1;
											this.currentRoster[HERO_POSITION.center].health -= 1;
										} else {
											this.currentRoster[HERO_POSITION.front].health -= floor((this.currentCastle.damage *
												(1 - (this.currentRoster[HERO_POSITION.front].properties[0].amount +
													this.currentRoster[HERO_POSITION.center].properties[0].amount) *
													(this.currentRoster[HERO_POSITION.front].properties[1].amount +
														this.currentRoster[HERO_POSITION.center].properties[1].amount) / 8 / this.currentCastle.maxHealth)) / 2);
											this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
												(1 - (this.currentRoster[HERO_POSITION.front].properties[0].amount +
													this.currentRoster[HERO_POSITION.center].properties[0].amount) *
													(this.currentRoster[HERO_POSITION.front].properties[1].amount +
														this.currentRoster[HERO_POSITION.center].properties[1].amount) / 8 / this.currentCastle.maxHealth)) / 2);
										}
										this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.front].properties[0].amount +
											this.currentRoster[HERO_POSITION.center].properties[0].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[1].amount +
												this.currentRoster[HERO_POSITION.center].properties[1].amount) / 8;
										break;
								}
								if (this.currentRoster[HERO_POSITION.front].health <= 0) {
									this.currentRoster[HERO_POSITION.front].health = 0;
									this.currentRoster[HERO_POSITION.front].isAlive = false;
									this.currentRoster[HERO_POSITION.front].isHeroFighting = false;
									if (this.currentRoster[HERO_POSITION.back].isAlive) {
										this.currentRoster[HERO_POSITION.back].isHeroFighting = true;
									}
								}
								if (this.currentRoster[HERO_POSITION.center].health <= 0) {
									this.currentRoster[HERO_POSITION.center].health = 0;
									this.currentRoster[HERO_POSITION.center].isAlive = false;
									this.currentRoster[HERO_POSITION.center].isHeroFighting = false;
									if (this.currentRoster[HERO_POSITION.back].isAlive) {
										this.currentRoster[HERO_POSITION.back].isHeroFighting = true;
									}
								}
							}
						} else if (this.currentRoster[HERO_POSITION.center].isAlive) {
							this.currentRoster[HERO_POSITION.center].isHeroFighting = true;
							this.currentRoster[HERO_POSITION.back].isHeroFighting = true;
							switch (this.currentCastle.properties) {
								case CASTLE_PROPERTIES.sword:
									this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) / this.currentCastle.maxHealth)) / 2);
									this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) / this.currentCastle.maxHealth)) / 2);
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.center].properties[2].amount +
										this.currentRoster[HERO_POSITION.back].properties[2].amount;
									break;
								case CASTLE_PROPERTIES.shield:
									this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[0].amount +
											this.currentRoster[HERO_POSITION.back].properties[0].amount) / this.currentCastle.maxHealth)) / 2);
									this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[0].amount +
											this.currentRoster[HERO_POSITION.back].properties[0].amount) / this.currentCastle.maxHealth)) / 2);
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.center].properties[0].amount +
										this.currentRoster[HERO_POSITION.back].properties[0].amount;
									break;
								case CASTLE_PROPERTIES.bow:
									this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[1].amount +
											this.currentRoster[HERO_POSITION.back].properties[1].amount) / this.currentCastle.maxHealth)) / 2);
									this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[1].amount +
											this.currentRoster[HERO_POSITION.back].properties[1].amount) / this.currentCastle.maxHealth)) / 2);
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.center].properties[1].amount +
										this.currentRoster[HERO_POSITION.back].properties[1].amount;
									break;
								case CASTLE_PROPERTIES.swordShield:
									if (1 - (this.currentRoster[HERO_POSITION.center].properties[2].amount +
										this.currentRoster[HERO_POSITION.back].properties[2].amount) *
										(this.currentRoster[HERO_POSITION.center].properties[0].amount +
											this.currentRoster[HERO_POSITION.back].properties[0].amount) / 8 / this.currentCastle.maxHealth <= 0) {
										this.currentRoster[HERO_POSITION.center].health -= 1;
										this.currentRoster[HERO_POSITION.back].health -= 1;
									} else {
										this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
											(1 - (this.currentRoster[HERO_POSITION.center].properties[2].amount +
												this.currentRoster[HERO_POSITION.back].properties[2].amount) *
												(this.currentRoster[HERO_POSITION.center].properties[0].amount +
													this.currentRoster[HERO_POSITION.back].properties[0].amount) / 8 / this.currentCastle.maxHealth)) / 2);
										this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
											(1 - (this.currentRoster[HERO_POSITION.center].properties[2].amount +
												this.currentRoster[HERO_POSITION.back].properties[2].amount) *
												(this.currentRoster[HERO_POSITION.center].properties[0].amount +
													this.currentRoster[HERO_POSITION.back].properties[0].amount) / 8 / this.currentCastle.maxHealth)) / 2);
									}
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.center].properties[2].amount +
										this.currentRoster[HERO_POSITION.back].properties[2].amount) *
										(this.currentRoster[HERO_POSITION.center].properties[0].amount +
											this.currentRoster[HERO_POSITION.back].properties[0].amount) / 8;
									break;
								case CASTLE_PROPERTIES.swordBow:
									if (1 - (this.currentRoster[HERO_POSITION.center].properties[2].amount +
										this.currentRoster[HERO_POSITION.back].properties[2].amount) *
										(this.currentRoster[HERO_POSITION.center].properties[1].amount +
											this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8 / this.currentCastle.maxHealth <= 0) {
										this.currentRoster[HERO_POSITION.center].health -= 1;
										this.currentRoster[HERO_POSITION.back].health -= 1;
									} else {
										this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
											(1 - (this.currentRoster[HERO_POSITION.center].properties[2].amount +
												this.currentRoster[HERO_POSITION.back].properties[2].amount) *
												(this.currentRoster[HERO_POSITION.center].properties[1].amount +
													this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8 / this.currentCastle.maxHealth)) / 2);
										this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
											(1 - (this.currentRoster[HERO_POSITION.center].properties[2].amount +
												this.currentRoster[HERO_POSITION.back].properties[2].amount) *
												(this.currentRoster[HERO_POSITION.center].properties[1].amount +
													this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8 / this.currentCastle.maxHealth)) / 2);
									}
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.center].properties[2].amount +
										this.currentRoster[HERO_POSITION.back].properties[2].amount) *
										(this.currentRoster[HERO_POSITION.center].properties[1].amount +
											this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8;
									break;
								case CASTLE_PROPERTIES.shieldBow:
									if (1 - (this.currentRoster[HERO_POSITION.center].properties[0].amount +
										this.currentRoster[HERO_POSITION.back].properties[0].amount) *
										(this.currentRoster[HERO_POSITION.center].properties[1].amount +
											this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8 / this.currentCastle.maxHealth <= 0) {
										this.currentRoster[HERO_POSITION.center].health -= 1;
										this.currentRoster[HERO_POSITION.back].health -= 1;
									} else {
										this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
											(1 - (this.currentRoster[HERO_POSITION.center].properties[0].amount +
												this.currentRoster[HERO_POSITION.back].properties[0].amount) *
												(this.currentRoster[HERO_POSITION.center].properties[1].amount +
													this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8 / this.currentCastle.maxHealth)) / 2);
										this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
											(1 - (this.currentRoster[HERO_POSITION.center].properties[0].amount +
												this.currentRoster[HERO_POSITION.back].properties[0].amount) *
												(this.currentRoster[HERO_POSITION.center].properties[1].amount +
													this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8 / this.currentCastle.maxHealth)) / 2);
									}
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.center].properties[0].amount +
										this.currentRoster[HERO_POSITION.back].properties[0].amount) *
										(this.currentRoster[HERO_POSITION.center].properties[1].amount +
											this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8;
									break;
							}
							if (this.currentRoster[HERO_POSITION.center].health <= 0) {
								this.currentRoster[HERO_POSITION.center].health = 0;
								this.currentRoster[HERO_POSITION.center].isAlive = false;
								this.currentRoster[HERO_POSITION.center].isHeroFighting = false;
							}
							if (this.currentRoster[HERO_POSITION.back].health <= 0) {
								this.currentRoster[HERO_POSITION.back].health = 0;
								this.currentRoster[HERO_POSITION.back].isAlive = false;
								this.currentRoster[HERO_POSITION.back].isHeroFighting = false;
							}
						} else {
							this.currentRoster[HERO_POSITION.back].isHeroFighting = true;
							switch (this.currentCastle.properties) {
								case CASTLE_PROPERTIES.sword:
									this.currentRoster[HERO_POSITION.back].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.back].properties[2].amount / this.currentCastle.maxHealth));
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.back].properties[2].amount;
									break;
								case CASTLE_PROPERTIES.shield:
									this.currentRoster[HERO_POSITION.back].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.back].properties[0].amount / this.currentCastle.maxHealth));
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.back].properties[0].amount;
									break;
								case CASTLE_PROPERTIES.bow:
									this.currentRoster[HERO_POSITION.back].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.back].properties[1].amount / this.currentCastle.maxHealth));
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.back].properties[1].amount;
									break;
								case CASTLE_PROPERTIES.swordShield:
									this.currentRoster[HERO_POSITION.back].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.back].properties[2].amount *
											this.currentRoster[HERO_POSITION.back].properties[0].amount / 2 / this.currentCastle.maxHealth));
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.back].properties[2].amount *
										this.currentRoster[HERO_POSITION.back].properties[0].amount / 2);
									break;
								case CASTLE_PROPERTIES.swordBow:
									this.currentRoster[HERO_POSITION.back].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.back].properties[2].amount *
											this.currentRoster[HERO_POSITION.back].properties[1].amount / 2 / this.currentCastle.maxHealth));
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.back].properties[2].amount *
										this.currentRoster[HERO_POSITION.back].properties[1].amount / 2);
									break;
								case CASTLE_PROPERTIES.shieldBow:
									this.currentRoster[HERO_POSITION.back].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.back].properties[0].amount *
											this.currentRoster[HERO_POSITION.back].properties[1].amount / 2 / this.currentCastle.maxHealth));
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.back].properties[0].amount *
										this.currentRoster[HERO_POSITION.back].properties[1].amount / 2);
									break;
							}
							if (this.currentRoster[HERO_POSITION.back].health <= 0) {
								this.currentRoster[HERO_POSITION.back].health = 0;
								this.currentRoster[HERO_POSITION.back].isAlive = false;
								this.currentRoster[HERO_POSITION.back].isHeroFighting = false;
							}
						}
						break;
					case CASTLE_SIZE.giant:
						if (this.currentRoster[HERO_POSITION.front].isAlive) {
							this.currentRoster[HERO_POSITION.front].isHeroFighting = true;
							this.currentRoster[HERO_POSITION.center].isHeroFighting = true;
							this.currentRoster[HERO_POSITION.back].isHeroFighting = true;
							switch (this.currentCastle.properties) {
								case CASTLE_PROPERTIES.sword:
									this.currentRoster[HERO_POSITION.front].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
											this.currentRoster[HERO_POSITION.center].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) / this.currentCastle.maxHealth)) / 3);
									this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
											this.currentRoster[HERO_POSITION.center].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) / this.currentCastle.maxHealth)) / 3);
									this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) / this.currentCastle.maxHealth)) / 3);
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.front].properties[2].amount +
										this.currentRoster[HERO_POSITION.center].properties[2].amount +
										this.currentRoster[HERO_POSITION.back].properties[2].amount;
									break;
								case CASTLE_PROPERTIES.shield:
									this.currentRoster[HERO_POSITION.front].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.front].properties[0].amount +
											this.currentRoster[HERO_POSITION.center].properties[0].amount +
											this.currentRoster[HERO_POSITION.back].properties[0].amount) / this.currentCastle.maxHealth)) / 3);
									this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.front].properties[0].amount +
											this.currentRoster[HERO_POSITION.center].properties[0].amount +
											this.currentRoster[HERO_POSITION.back].properties[0].amount) / this.currentCastle.maxHealth)) / 3);
									this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[0].amount +
											this.currentRoster[HERO_POSITION.back].properties[0].amount) / this.currentCastle.maxHealth)) / 3);
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.front].properties[0].amount +
										this.currentRoster[HERO_POSITION.center].properties[0].amount +
										this.currentRoster[HERO_POSITION.back].properties[0].amount;
									break;
								case CASTLE_PROPERTIES.bow:
									this.currentRoster[HERO_POSITION.front].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.front].properties[1].amount +
											this.currentRoster[HERO_POSITION.center].properties[1].amount +
											this.currentRoster[HERO_POSITION.back].properties[1].amount) / this.currentCastle.maxHealth)) / 3);
									this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.front].properties[1].amount +
											this.currentRoster[HERO_POSITION.center].properties[1].amount +
											this.currentRoster[HERO_POSITION.back].properties[1].amount) / this.currentCastle.maxHealth)) / 3);
									this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[1].amount +
											this.currentRoster[HERO_POSITION.back].properties[1].amount) / this.currentCastle.maxHealth)) / 3);
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.front].properties[1].amount +
										this.currentRoster[HERO_POSITION.center].properties[1].amount +
										this.currentRoster[HERO_POSITION.back].properties[1].amount;
									break;
								case CASTLE_PROPERTIES.swordShield:
									this.currentRoster[HERO_POSITION.front].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
											this.currentRoster[HERO_POSITION.center].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[0].amount +
												this.currentRoster[HERO_POSITION.center].properties[0].amount +
												this.currentRoster[HERO_POSITION.back].properties[0].amount) / 18 / this.currentCastle.maxHealth)) / 3);
									this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
											this.currentRoster[HERO_POSITION.center].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[0].amount +
												this.currentRoster[HERO_POSITION.center].properties[0].amount +
												this.currentRoster[HERO_POSITION.back].properties[0].amount) / 18 / this.currentCastle.maxHealth)) / 3);
									this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
											this.currentRoster[HERO_POSITION.center].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[0].amount +
												this.currentRoster[HERO_POSITION.center].properties[0].amount +
												this.currentRoster[HERO_POSITION.back].properties[0].amount) / 18 / this.currentCastle.maxHealth)) / 3);
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.front].properties[2].amount +
										this.currentRoster[HERO_POSITION.center].properties[2].amount +
										this.currentRoster[HERO_POSITION.back].properties[2].amount) *
										(this.currentRoster[HERO_POSITION.front].properties[0].amount +
											this.currentRoster[HERO_POSITION.center].properties[0].amount +
											this.currentRoster[HERO_POSITION.back].properties[0].amount) / 18;
									break;
								case CASTLE_PROPERTIES.swordBow:
									this.currentRoster[HERO_POSITION.front].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
											this.currentRoster[HERO_POSITION.center].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[1].amount +
												this.currentRoster[HERO_POSITION.center].properties[1].amount +
												this.currentRoster[HERO_POSITION.back].properties[1].amount) / 18 / this.currentCastle.maxHealth)) / 3);
									this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
											this.currentRoster[HERO_POSITION.center].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[1].amount +
												this.currentRoster[HERO_POSITION.center].properties[1].amount +
												this.currentRoster[HERO_POSITION.back].properties[1].amount) / 18 / this.currentCastle.maxHealth)) / 3);
									this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.front].properties[2].amount +
											this.currentRoster[HERO_POSITION.center].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[1].amount +
												this.currentRoster[HERO_POSITION.center].properties[1].amount +
												this.currentRoster[HERO_POSITION.back].properties[1].amount) / 18 / this.currentCastle.maxHealth)) / 3);
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.front].properties[2].amount +
										this.currentRoster[HERO_POSITION.center].properties[2].amount +
										this.currentRoster[HERO_POSITION.back].properties[2].amount) *
										(this.currentRoster[HERO_POSITION.front].properties[1].amount +
											this.currentRoster[HERO_POSITION.center].properties[1].amount +
											this.currentRoster[HERO_POSITION.back].properties[1].amount) / 18;
									break;
								case CASTLE_PROPERTIES.shieldBow:
									this.currentRoster[HERO_POSITION.front].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.front].properties[0].amount +
											this.currentRoster[HERO_POSITION.center].properties[0].amount +
											this.currentRoster[HERO_POSITION.back].properties[0].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[1].amount +
												this.currentRoster[HERO_POSITION.center].properties[1].amount +
												this.currentRoster[HERO_POSITION.back].properties[1].amount) / 18 / this.currentCastle.maxHealth)) / 3);
									this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.front].properties[0].amount +
											this.currentRoster[HERO_POSITION.center].properties[0].amount +
											this.currentRoster[HERO_POSITION.back].properties[0].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[1].amount +
												this.currentRoster[HERO_POSITION.center].properties[1].amount +
												this.currentRoster[HERO_POSITION.back].properties[1].amount) / 18 / this.currentCastle.maxHealth)) / 3);
									this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.front].properties[0].amount +
											this.currentRoster[HERO_POSITION.center].properties[0].amount +
											this.currentRoster[HERO_POSITION.back].properties[0].amount) *
											(this.currentRoster[HERO_POSITION.front].properties[1].amount +
												this.currentRoster[HERO_POSITION.center].properties[1].amount +
												this.currentRoster[HERO_POSITION.back].properties[1].amount) / 18 / this.currentCastle.maxHealth)) / 3);
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.front].properties[0].amount +
										this.currentRoster[HERO_POSITION.center].properties[0].amount +
										this.currentRoster[HERO_POSITION.back].properties[0].amount) *
										(this.currentRoster[HERO_POSITION.front].properties[1].amount +
											this.currentRoster[HERO_POSITION.center].properties[1].amount +
											this.currentRoster[HERO_POSITION.back].properties[1].amount) / 18;
									break;
							}
							if (this.currentRoster[HERO_POSITION.front].health <= 0) {
								this.currentRoster[HERO_POSITION.front].health = 0;
								this.currentRoster[HERO_POSITION.front].isAlive = false;
								this.currentRoster[HERO_POSITION.front].isHeroFighting = false;
							}
							if (this.currentRoster[HERO_POSITION.center].health <= 0) {
								this.currentRoster[HERO_POSITION.center].health = 0;
								this.currentRoster[HERO_POSITION.center].isAlive = false;
								this.currentRoster[HERO_POSITION.center].isHeroFighting = false;
							}
							if (this.currentRoster[HERO_POSITION.back].health <= 0) {
								this.currentRoster[HERO_POSITION.back].health = 0;
								this.currentRoster[HERO_POSITION.back].isAlive = false;
								this.currentRoster[HERO_POSITION.back].isHeroFighting = false;
							}
						} else if (this.currentRoster[HERO_POSITION.center].isAlive) {
							this.currentRoster[HERO_POSITION.center].isHeroFighting = true;
							this.currentRoster[HERO_POSITION.back].isHeroFighting = true;
							switch (this.currentCastle.properties) {
								case CASTLE_PROPERTIES.sword:
									this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) / this.currentCastle.maxHealth)) / 2);
									this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) / this.currentCastle.maxHealth)) / 2);
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.center].properties[2].amount +
										this.currentRoster[HERO_POSITION.back].properties[2].amount;
									break;
								case CASTLE_PROPERTIES.shield:
									this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[0].amount +
											this.currentRoster[HERO_POSITION.back].properties[0].amount) / this.currentCastle.maxHealth)) / 2);
									this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[0].amount +
											this.currentRoster[HERO_POSITION.back].properties[0].amount) / this.currentCastle.maxHealth)) / 2);
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.center].properties[0].amount +
										this.currentRoster[HERO_POSITION.back].properties[0].amount;
									break;
								case CASTLE_PROPERTIES.bow:
									this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[1].amount +
											this.currentRoster[HERO_POSITION.back].properties[1].amount) / this.currentCastle.maxHealth)) / 2);
									this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[1].amount +
											this.currentRoster[HERO_POSITION.back].properties[1].amount) / this.currentCastle.maxHealth)) / 2);
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.center].properties[1].amount +
										this.currentRoster[HERO_POSITION.back].properties[1].amount;
									break;
								case CASTLE_PROPERTIES.swordShield:
									this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) *
											(this.currentRoster[HERO_POSITION.center].properties[0].amount +
												this.currentRoster[HERO_POSITION.back].properties[0].amount) / 8 / this.currentCastle.maxHealth)) / 2);
									this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) *
											(this.currentRoster[HERO_POSITION.center].properties[0].amount +
												this.currentRoster[HERO_POSITION.back].properties[0].amount) / 8 / this.currentCastle.maxHealth)) / 2);
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.center].properties[2].amount +
										this.currentRoster[HERO_POSITION.back].properties[2].amount) *
										(this.currentRoster[HERO_POSITION.center].properties[0].amount +
											this.currentRoster[HERO_POSITION.back].properties[0].amount) / 8;
									break;
								case CASTLE_PROPERTIES.swordBow:
									this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) *
											(this.currentRoster[HERO_POSITION.center].properties[1].amount +
												this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8 / this.currentCastle.maxHealth)) / 2);
									this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[2].amount +
											this.currentRoster[HERO_POSITION.back].properties[2].amount) *
											(this.currentRoster[HERO_POSITION.center].properties[1].amount +
												this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8 / this.currentCastle.maxHealth)) / 2);
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.center].properties[2].amount +
										this.currentRoster[HERO_POSITION.back].properties[2].amount) *
										(this.currentRoster[HERO_POSITION.center].properties[1].amount +
											this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8;
									break;
								case CASTLE_PROPERTIES.shieldBow:
									this.currentRoster[HERO_POSITION.center].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[0].amount +
											this.currentRoster[HERO_POSITION.back].properties[0].amount) *
											(this.currentRoster[HERO_POSITION.center].properties[1].amount +
												this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8 / this.currentCastle.maxHealth)) / 2);
									this.currentRoster[HERO_POSITION.back].health -= floor((this.currentCastle.damage *
										(1 - (this.currentRoster[HERO_POSITION.center].properties[0].amount +
											this.currentRoster[HERO_POSITION.back].properties[0].amount) *
											(this.currentRoster[HERO_POSITION.center].properties[1].amount +
												this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8 / this.currentCastle.maxHealth)) / 2);
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.center].properties[0].amount +
										this.currentRoster[HERO_POSITION.back].properties[0].amount) *
										(this.currentRoster[HERO_POSITION.center].properties[1].amount +
											this.currentRoster[HERO_POSITION.back].properties[1].amount) / 8;
									break;
							}
							if (this.currentRoster[HERO_POSITION.center].health <= 0) {
								this.currentRoster[HERO_POSITION.center].health = 0;
								this.currentRoster[HERO_POSITION.center].isAlive = false;
								this.currentRoster[HERO_POSITION.center].isHeroFighting = false;
							}
							if (this.currentRoster[HERO_POSITION.back].health <= 0) {
								this.currentRoster[HERO_POSITION.back].health = 0;
								this.currentRoster[HERO_POSITION.back].isAlive = false;
								this.currentRoster[HERO_POSITION.back].isHeroFighting = false;
							}
						} else {
							this.currentRoster[HERO_POSITION.back].isHeroFighting = true;
							switch (this.currentCastle.properties) {
								case CASTLE_PROPERTIES.sword:
									this.currentRoster[HERO_POSITION.back].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.back].properties[2].amount / this.currentCastle.maxHealth));
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.back].properties[2].amount;
									break;
								case CASTLE_PROPERTIES.shield:
									this.currentRoster[HERO_POSITION.back].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.back].properties[0].amount / this.currentCastle.maxHealth));
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.back].properties[0].amount;
									break;
								case CASTLE_PROPERTIES.bow:
									this.currentRoster[HERO_POSITION.back].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.back].properties[1].amount / this.currentCastle.maxHealth));
									this.currentCastle.health -= this.currentRoster[HERO_POSITION.back].properties[1].amount;
									break;
								case CASTLE_PROPERTIES.swordShield:
									this.currentRoster[HERO_POSITION.back].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.back].properties[2].amount *
											this.currentRoster[HERO_POSITION.back].properties[0].amount / 2 / this.currentCastle.maxHealth));
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.back].properties[2].amount *
										this.currentRoster[HERO_POSITION.back].properties[0].amount / 2);
									break;
								case CASTLE_PROPERTIES.swordBow:
									this.currentRoster[HERO_POSITION.back].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.back].properties[2].amount *
											this.currentRoster[HERO_POSITION.back].properties[1].amount / 2 / this.currentCastle.maxHealth));
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.back].properties[2].amount *
										this.currentRoster[HERO_POSITION.back].properties[1].amount / 2);
									break;
								case CASTLE_PROPERTIES.shieldBow:
									this.currentRoster[HERO_POSITION.back].health -= floor(this.currentCastle.damage *
										(1 - this.currentRoster[HERO_POSITION.back].properties[0].amount *
											this.currentRoster[HERO_POSITION.back].properties[1].amount / 2 / this.currentCastle.maxHealth));
									this.currentCastle.health -= floor(this.currentRoster[HERO_POSITION.back].properties[0].amount *
										this.currentRoster[HERO_POSITION.back].properties[1].amount / 2);
									break;
							}
							if (this.currentRoster[HERO_POSITION.back].health <= 0) {
								this.currentRoster[HERO_POSITION.back].health = 0;
								this.currentRoster[HERO_POSITION.back].isAlive = false;
								this.currentRoster[HERO_POSITION.back].isHeroFighting = false;
							}
						}
						break;
				}



				game.world.shake = 4;

				game.world.thunder = 0.1;
				game.world.isThunder = true;

				hitSounds[round(random(0, 1))].play(); //2 is destroy sound



				//Check once, triggers the 'battle-rattle'

				//play sound once, and set some values to be decremented

				//game.world.shake = this.currentCastle.size;

				this.battleTimer = 0;
			} else {
				this.battleTimer += game.deltaTime / 60;
			}



			if (this.currentCastle.health <= 0 &&
				this.currentRoster[HERO_POSITION.front].health != 0 &&
				this.currentRoster[HERO_POSITION.center].health != 0 &&
				this.currentRoster[HERO_POSITION.back].health != 0)
				this.currentCastle.shake = 1;

			if (this.currentCastle.health <= 0 && game.world.shake <= 0) {

				if (this.currentCastle.size == CASTLE_SIZE.giant) {
					game.gameState = GAME_STATE.clear;
					return;
				}

				sinkSound.play();

				this.rosterState = ROSTER_STATE.rest;

			}

		}

		if (this.currentRoster[HERO_POSITION.front].health == 0 &&
			this.currentRoster[HERO_POSITION.center].health == 0 &&
			this.currentRoster[HERO_POSITION.back].health == 0) {

			if (game.world.rollBacksLeft == 0)
				game.gameState = GAME_STATE.gameover;

			for (let castle of game.world.castles) {
				if (castle.health <= 0 && castle.sink > 0) {
					castle.health = castle.maxHealth;
					castle.shake = 1;
				}
			}
			this.rosterState = ROSTER_STATE.rollback;

		}

	}

	drawBattleCharacterInfo() {
		//console.log(game.world.castles[0].properties, game.world.castles[1].properties, game.world.castles[2].properties, game.world.castles[3].properties);
		const INTERFACE = rosterBattleInterface;

		for (let i = 0; i < this.maximumRosterSize; ++i) {
			let member = this.currentRoster[i];
			let correction = 0;

			member.updateProperties();

			switch (member.position) {
				case HERO_POSITION.back:
					correction = 0;
					break;

				case HERO_POSITION.center:
					correction = INTERFACE.intervalRelativeWidth;
					break;

				case HERO_POSITION.front:
					correction = 2 * INTERFACE.intervalRelativeWidth;
					break;
			}

			push();

			/* Draw Character Image Here!!!!!!! */
			image(member.faceImage,
				correction + INTERFACE.imageRelativePosX * width / 100,
				INTERFACE.imageRelativePosY * height / 100,
				INTERFACE.imageRelativeWidth,
				INTERFACE.imageRelativeHeight);

			if (member.isAlive == false) {
				image(deathImage,
					correction + INTERFACE.imageRelativePosX * width / 100,
					INTERFACE.imageRelativePosY * height / 100,
					INTERFACE.imageRelativeWidth,
					INTERFACE.imageRelativeHeight);
			}
			else if (game.world.roster.rosterState == ROSTER_STATE.battle && member.isHeroFighting == true) {
				image(battleImage,
					correction + INTERFACE.imageRelativePosX * width / 100,
					INTERFACE.imageRelativePosY * height / 100,
					INTERFACE.imageRelativeWidth,
					INTERFACE.imageRelativeHeight);
			}

			//////////////////////////////////////////////////////////////

			fill(COLOR_BACKGROUND);
			stroke(COLOR_BACKGROUND);
			strokeWeight(3);
			rect(correction + INTERFACE.healthRelativePosX * width / 100,
				INTERFACE.healthRelativePosY * height / 100,
				INTERFACE.healthRelativeWidth,
				INTERFACE.healthRelativeHeight);

			fill(COLOR_IMAGES);
			rect(correction + INTERFACE.healthRelativePosX * width / 100,
				INTERFACE.healthRelativePosY * height / 100,
				map(member.health, 0, member.maxHealth, 0, INTERFACE.healthRelativeWidth),
				INTERFACE.healthRelativeHeight);

			noStroke();

			if (game.world.roster.rosterState != ROSTER_STATE.battle) {
				fill(COLOR_BACKGROUND);
				for (let i = 0; i < 4; ++i) {
					rect(correction + INTERFACE.healthRelativePosX * width / 100 + i * INTERFACE.healthRelativeWidth / 4,
						INTERFACE.healthRelativePosY * height / 100,
						2, 8);
				}
			}

			if (game.world.roster.rosterState == ROSTER_STATE.battle && member.isAlive) {
				if (game.isBlinked == 1)
					fill(COLOR_IMAGES);
				else
					fill(COLOR_BACKGROUND);
				textSize(INTERFACE.helathRelativeTextSize);
				textAlign(CENTER, CENTER);
				text("IN BATTLE",
					correction + INTERFACE.healthRelativePosX * width / 100 + INTERFACE.healthRelativeWidth / 2,
					INTERFACE.healthRelativePosY * height / 100 + INTERFACE.healthRelativeHeight / 2 - 2);
			}

			if (member.isAlive == false) {
				fill(COLOR_IMAGES);
				textSize(INTERFACE.helathRelativeTextSize);
				textAlign(CENTER, CENTER);
				text("DEAD",
					correction + INTERFACE.healthRelativePosX * width / 100 + INTERFACE.healthRelativeWidth / 2,
					INTERFACE.healthRelativePosY * height / 100 + INTERFACE.healthRelativeHeight / 2 - 2);
			}

			/* Draw Icon Image Here!!!!!!!!!!!! *////////////////////////
			for (let i = 0; i < 3; ++i) {

				let auraIndex = -1;
				if (member.properties[i].amount <= 2) { }
				else if (member.properties[i].amount <= 5) { auraIndex = 0; }
				else if (member.properties[i].amount <= 8) { auraIndex = 1; }
				else if (member.properties[i].amount <= 11) { auraIndex = 2; }
				else { auraIndex = 3; }

				let iconImage;
				switch (i) {
					case 0:
						iconImage = swordIconImage;
						break;

					case 1:
						iconImage = shieldIconImage;
						break;

					case 2:
						iconImage = bowIconImage;
						break;
				}
				push();
				tint(255, map(member.properties[i].amount, 0, 12, 0, 255));
				image(iconImage,
					correction + INTERFACE.iconRelativePosX * width / 100 + i * INTERFACE.iconRelativeWidth,
					INTERFACE.iconRelativePosY * height / 100,
					INTERFACE.iconRelativeWidth,
					INTERFACE.iconRelativeHeight);
				pop();
			}
			//////////////////////////////////////////////////////////////

			pop();

		}
	}



	rest() {

		if (this.depth > 0) {

			this.progress += game.deltaTime / (180);
			this.depth -= game.deltaTime / (30);

		} else {

			if (keyIsPressed && keyCode == 106) {
				this.showedUpLoots = [];
			}

			if (this.selectedLoot == null && this.showedUpLoots.length < 3) {
				while (this.showedUpLoots.length < 3) {
					this.generateRandomLoot();
				}
			}

			else if (this.selectedLoot == null && this.showedUpLoots.length == 3) {
				for (let i = 0; i < this.showedUpLoots.length; ++i) {
					this.showedUpLoots[i].draw(i);
				}

				if (mouseIsPressed) {
					if (mouseY >= itemInterface.boxRelativePosY * height / 100 &&
						mouseY <= itemInterface.boxRelativePosY * height / 100 + itemInterface.boxRelativeHeight) {
						if (mouseX >= itemInterface.boxRelativePosX * width / 100 &&
							mouseX <= itemInterface.boxRelativePosX * width / 100 + itemInterface.boxRelativeWidth) {
							this.selectedLoot = this.showedUpLoots[0];
							this.selectedLootIndex = 0;
						}

						else if (mouseX >= itemInterface.boxRelativePosX * width / 100 + itemInterface.intervalRelativeWidth &&
							mouseX <= itemInterface.boxRelativePosX * width / 100 + itemInterface.boxRelativeWidth + itemInterface.intervalRelativeWidth) {
							this.selectedLoot = this.showedUpLoots[1];
							this.selectedLootIndex = 1;
						}

						else if (mouseX >= itemInterface.boxRelativePosX * width / 100 + 2 * itemInterface.intervalRelativeWidth &&
							mouseX <= itemInterface.boxRelativePosX * width / 100 + itemInterface.boxRelativeWidth + 2 * itemInterface.intervalRelativeWidth) {
							this.selectedLoot = this.showedUpLoots[2];
							this.selectedLootIndex = 2;
						}
					}
				}
			}

			else if (this.isLootUsed == false && this.selectedLoot != null) {
				this.showedUpLoots = [];
				this.selectedLoot.draw(this.selectedLootIndex);
				this.isLootUsed = this.selectedLoot.use();
			}

			else if (this.isLootUsed == true) {

				if (this.posChangeDone == false) {
					let INTERFACE = changeHeroPosition;
					let selectedAmount = 0;

					for (let i = 0; i < INTERFACE.selectButtons.length; ++i) {
						INTERFACE.selectButtons[i].show();
						INTERFACE.selectButtons[i].position(i * INTERFACE.intervalRelativeWidth + INTERFACE.buttonRelativePosX * width / 100,
							INTERFACE.buttonRelativePosY * height / 100);
						INTERFACE.selectButtons[i].size(INTERFACE.buttonRelativeWidth, INTERFACE.buttonRelativeHeight);
						if (INTERFACE.isSelected[i]) {
							INTERFACE.selectButtons[i].style("background-color", "#000");
							INTERFACE.selectButtons[i].style("color", "#fff");
							selectedAmount++;
						}
						else {
							INTERFACE.selectButtons[i].style("background-color", "#fff");
							INTERFACE.selectButtons[i].style("color", "#000");
						}
						INTERFACE.selectButtons[i].mousePressed(function () {
							if (INTERFACE.isSelected[i]) {
								INTERFACE.isSelected[i] = false;
							}
							else {
								INTERFACE.isSelected[i] = true;
							}
						})
					}

					INTERFACE.confirmButton[0].show();
					INTERFACE.confirmButton[0].position(INTERFACE.buttonConfirmRelativePosX * width / 100,
						INTERFACE.buttonConfirmRelativePosY * height / 100);
					INTERFACE.confirmButton[0].size(INTERFACE.buttonConfirmRelativeWidth, INTERFACE.buttonConfirmRelativeHeight);
					INTERFACE.confirmButton[0].mousePressed(function () {
						game.world.roster.posChangeDone = true;
					})

					if (selectedAmount == 2) {
						let selectedIndex = [];
						for (let i = 0; i < INTERFACE.selectButtons.length; ++i) {
							if (INTERFACE.isSelected[i] == true) { selectedIndex.push(i); }
						}
						selectedIndex.sort();

						[this.currentRoster[selectedIndex[0]], this.currentRoster[selectedIndex[1]]] =
							[this.currentRoster[selectedIndex[1]], this.currentRoster[selectedIndex[0]]];

						for (let i = 0; i < this.currentRoster.length; ++i) {
							switch (i) {
								case 0:
									this.currentRoster[i].position = HERO_POSITION.back;
									break;

								case 1:
									this.currentRoster[i].position = HERO_POSITION.center;
									break;

								case 2:
									this.currentRoster[i].position = HERO_POSITION.front;
									break;
							}
						}

						INTERFACE.isSelected = [false, false, false];
					}
				}

				if (this.posChangeDone) {
					this.showedUpLoots = [];
					this.selectedLoot = null;
					this.selectedLootIndex = -1;
					this.isLootUsed = false;
					this.posChangeDone = false;

					hideRosterButtons();

					this.rosterState = ROSTER_STATE.walk;
				}
			}

		}

	}

	selectHero(allowedTargetState) {
		const INTERFACE = rosterInterface;

		for (let i = 0; i < 3; ++i) {
			if ((allowedTargetState == "ALIVE" && this.currentRoster[i].isAlive) ||
				(allowedTargetState == "DEAD" && !this.currentRoster[i].isAlive)) {
				INTERFACE.selectButtons[i].show();
			}
			INTERFACE.selectButtons[i].position(i * INTERFACE.buttonStartIntervalRelativeWidth + INTERFACE.buttonSelectRelativePosX * width / 100,
				INTERFACE.buttonSelectRelativePosY * height / 100);
			INTERFACE.selectButtons[i].size(INTERFACE.buttonSelectRelativeWidth, INTERFACE.buttonSelectRelativeHeight);
			INTERFACE.selectButtons[i].mousePressed(function () {
				game.world.roster.selectedHero = i;
			})
		}

		if (this.selectedHero != -1) {
			let retrunValue = this.selectedHero;
			for (let i = 0; i < 3; ++i) {
				INTERFACE.selectButtons[i].hide();
			}
			this.selectedHero = -1;
			return retrunValue;
		}
	}

	// Contain Interfaces, buttons
	generateRoster() {
		const INTERFACE = rosterInterface;

		for (let i = 0; i < this.maximumRosterSize; ++i) {
			let member = this.currentRoster[i];
			let displayName = member.name;
			let correction = 0;
			let buttonIndex = 0;
			let imageList = [member.bodyImage];

			member.updateProperties();

			// Position Check
			switch (member.position) {
				case HERO_POSITION.back:
					displayName += " (Back)";
					correction = 0;
					buttonIndex = 0;
					break;

				case HERO_POSITION.center:
					displayName += " (Center)";
					correction = INTERFACE.intervalRelativeWidth;
					buttonIndex = 3;
					break;

				case HERO_POSITION.front:
					displayName += " (Front)";
					correction = 2 * INTERFACE.intervalRelativeWidth;
					buttonIndex = 6;
					break;
			}

			switch (member.highestProperties.length) {
				case 1:
					switch (member.highestProperties[0]) {
						case "sword": imageList.push(swordArmImage); break;
						case "shield": imageList.push(shieldArmImage); break;
						case "bow": imageList.push(archerArmImage); break;
					}
					break;

				case 2:
					switch (member.highestProperties[0] + member.highestProperties[1]) {
						case "swordshield": imageList.push(swordshieldArmImage); break;
						case "shieldbow": imageList.push(shieldbowArmImage); break;
						case "swordbow": imageList.push(swordarcherArmImage); break;
					}
					break;

				case 3:
					if (member.properties[0].amount == 0) { imageList.push(defaultArmImage); }
					else { imageList.push(honjongArmImage); }
					break;
			}

			// Button Show/Hide Check
			if (member.propertyLeft == 0) {
				for (let i = 0; i < 3; ++i) {
					INTERFACE.upButtons[i + buttonIndex].hide();
				}
			}
			else {
				for (let i = 0; i < 3; ++i) {
					INTERFACE.upButtons[i + buttonIndex].show();
				}
			}

			for (let i = 0; i < member.properties.length; ++i) {
				if (member.properties[i].amount == 0) {
					INTERFACE.downButtons[i + buttonIndex].hide();
				}
				else {
					INTERFACE.downButtons[i + buttonIndex].show();
				}
			}

			INTERFACE.startButton[0].show();

			// Draw INTERFACE
			push();
			textSize(INTERFACE.spareRelativeTextSize);
			text("Rollbacks Left x " + game.world.rollBacksLeft,
				INTERFACE.spareRelativePosX * width / 100,
				INTERFACE.spareRelativePosY * height / 100);

			textSize(INTERFACE.nameRelativeTextSize);
			text(displayName,
				correction + INTERFACE.nameRelativePosX * width / 100,
				INTERFACE.nameRelativePosY * height / 100);

			/* Draw Character Image Here!!!!!!! */
			rect(correction + INTERFACE.imageRelativePosX * width / 100,
				INTERFACE.imageRelativePosY * height / 100,
				INTERFACE.imageRelativeWidth,
				INTERFACE.imageRelativeHeight)

			for (let i = 0; i < imageList.length; ++i) {
				image(imageList[i],
					correction + INTERFACE.imageRelativePosX * width / 100,
					INTERFACE.imageRelativePosY * height / 100)
			}
			//////////////////////////////////////////////////////////////

			/* Draw Icon Image Here!!!!!!!!!!!! *////////////////////////
			for (let i = 0; i < 3; ++i) {
				let iconImage;
				switch (i) {
					case 0:
						iconImage = swordIconImage;
						break;

					case 1:
						iconImage = shieldIconImage;
						break;

					case 2:
						iconImage = bowIconImage;
						break;
				}

				image(iconImage,
					correction + INTERFACE.iconRelativePosX * width / 100 + i * INTERFACE.iconRelativeWidth,
					INTERFACE.iconRelativePosY * height / 100,
					INTERFACE.iconRelativeWidth,
					INTERFACE.iconRelativeHeight);
			}
			//////////////////////////////////////////////////////////////

			for (let i = 0; i < 3; ++i) {
				INTERFACE.upButtons[i + buttonIndex].position(correction + INTERFACE.buttonUpRelativePosX * width / 100 + i * INTERFACE.buttonUpRelativeWidth,
					INTERFACE.buttonUpRelativePosY * height / 100);
				INTERFACE.upButtons[i + buttonIndex].size(INTERFACE.buttonUpRelativeWidth, INTERFACE.buttonUpRelativeHeight);
				INTERFACE.upButtons[i + buttonIndex].mousePressed(function () {
					++member.properties[i].amount;
					--member.propertyLeft;
				});
			}

			textAlign(CENTER, CENTER);
			textSize(INTERFACE.propertyRleativeTextSize);
			for (let i = 0; i < member.properties.length; ++i) {
				rect(correction + INTERFACE.propertyRelativePosX * width / 100 + i * INTERFACE.propertyRleativeTextSize,
					INTERFACE.propertyRelativePosY * height / 100,
					INTERFACE.propertyRelativeWidth,
					INTERFACE.propertyRelativeHeight);
				text(member.properties[i].amount,
					correction + INTERFACE.propertyRelativePosX * width / 100 + i * INTERFACE.propertyRleativeTextSize + INTERFACE.propertyRleativeTextSize / 2,
					INTERFACE.propertyRelativePosY * height / 100 + INTERFACE.propertyRleativeTextSize / 2 - 8);
			}

			for (let i = 0; i < 3; ++i) {
				INTERFACE.downButtons[i + buttonIndex].position(correction + INTERFACE.buttonDownRelativePosX * width / 100 + i * INTERFACE.buttonDownRelativeWidth,
					INTERFACE.buttonDownRelativePosY * height / 100);
				INTERFACE.downButtons[i + buttonIndex].size(INTERFACE.buttonDownRelativeWidth, INTERFACE.buttonDownRelativeHeight);
				INTERFACE.downButtons[i + buttonIndex].mousePressed(function () {
					--member.properties[i].amount;
					++member.propertyLeft;
				});
			}

			INTERFACE.startButton[0].position(INTERFACE.buttonStartRelativePosX * width / 100,
				INTERFACE.buttonStartRelativePosY * height / 100);
			INTERFACE.startButton[0].size(INTERFACE.buttonStartRelativeWidth, INTERFACE.buttonStartRelativeHeight);
			INTERFACE.startButton[0].mousePressed(function () {

				hideRosterButtons();
				game.gameState = GAME_STATE.mainGame;
			})

			pop();
		}
	}

	generateRandomLoot() {

		let minRarity = 0;
		let maxRarity = 0;
		let brokenCastle = 0;
		let castles = game.world.castles

		// 성 파괴된 갯수에 따른 드랍템 레어도 설정
		for (let i = 0; i < castles.length; ++i) {
			if (castles[i].health <= 0) {
				++brokenCastle;
			}
		}
		switch (brokenCastle) {
			case 1:
				minRarity = 0;
				maxRarity = 1;
				break;

			case 2:
				minRarity = 1;
				maxRarity = 2;
				break;

			case 3:
				minRarity = 2;
				maxRarity = 2;
				break;
		}

		let rarity = floor(random(minRarity, maxRarity + 1));
		let lootNum = floor(random(0, lootList.length));
		let genItem = lootList[lootNum][rarity];

		// 성수 등장 조건
		let isHolyNeeded = false;
		if (genItem.name == "Holy Water") {
			for (let i = 0; i < 3; ++i) {
				if (this.currentRoster[i].isAlive == false) { isHolyNeeded = true; }
			}
			if (isHolyNeeded == false) {
				return;
			}
		}

		// 중복 체크
		for (let i = 0; i < this.showedUpLoots.length; ++i) {
			if (genItem.name == this.showedUpLoots[i].name &&
				genItem.rarity == this.showedUpLoots[i].rarity) { return; }
		}

		this.showedUpLoots.push(genItem);

	}

}