const GAME_STATE = {
	gameover: -1,
	mainMenu: 0,
	howtoPlay: 0.5,
	preparation: 1,
	mainGame: 2,
	clear: 3
}

const HERO_POSITION =
{
	back: 0,
	center: 1,
	front: 2
}

const CASTLE_SIZE = {
	small: 1,
	big: 2,
	giant: 3,
}

const ROSTER_STATE =
{
	walk: 0,
	battle: 1,
	rest: 2,
	rollback: 3
}

const LOOT_RARITY =
{
	makeshift: 0,
	standard: 1,
	quality: 2
}

const CASTLE_PROPERTIES =
{
	sword: 0,
	shield: 1,
	bow: 2,
	swordShield: 3,
	swordBow: 4,
	shieldBow: 5
}

let COLOR_BACKGROUND = 255; //MAY be changed in the future
let COLOR_IMAGES = 0; //MAY be changed in the future

const COLOR_BACKGROUND_DEFAULT = 255;
const COLOR_IMAGES_DEFAULT = 0;

let BASE_WINDOW_WIDTH = 800; //MAY be changed in the future
let BASE_WINDOW_HEIGHT = 600; //MAY be changed in the future

let MAX_TRY = 4; //MAY be changed in the future