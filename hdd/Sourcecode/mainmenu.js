class MainMenu {
	constructor() {
		this.isHowtoPlayShow = false;
	}

	draw() {
		let INTERFACE = mainMenuInterface;
		background("black");

		for (let i = 0; i < INTERFACE.titleButtons.length; ++i) {
			INTERFACE.titleButtons[i].show();
			INTERFACE.titleButtons[i].position(INTERFACE.buttonTitleRelativePosX * width / 100,
				INTERFACE.buttonTitleRelativePosY * height / 100 + i * INTERFACE.buttonTitleRelativeInterval);
			INTERFACE.titleButtons[i].size(INTERFACE.buttonTitleRelativeWidth,
				INTERFACE.buttonRelativeHeight);

			switch (i) {
				case 0:
					INTERFACE.titleButtons[i].mousePressed(function () {
						for (let i = 0; i < INTERFACE.titleButtons.length; ++i) {
							INTERFACE.titleButtons[i].hide();
						}
						game.gameState = GAME_STATE.preparation;

					})
					break;

				case 1:
					INTERFACE.titleButtons[i].mousePressed(function () {
						mainMenu.isHowtoPlayShow = true;
					});
					break;
			}
		}

		if (this.isHowtoPlayShow) {
			this.showHowtoPlay();
			for (let i = 0; i < INTERFACE.titleButtons.length; ++i) {
				INTERFACE.titleButtons[i].hide();
			}

			INTERFACE.howToPlayButtons[0].show();
			INTERFACE.howToPlayButtons[0].position(INTERFACE.buttonBackRelativePosX * width / 100,
				INTERFACE.buttonBackRelativePosY * height / 100);
			INTERFACE.howToPlayButtons[0].size(INTERFACE.buttonBackRelativeWidth,
				INTERFACE.buttonRelativeHeight);
			INTERFACE.howToPlayButtons[0].mousePressed(function () {
				mainMenu.isHowtoPlayShow = false;
			});
		}
		else {
			INTERFACE.howToPlayButtons[0].hide();
			image(iconImage, BASE_WINDOW_WIDTH / 2 - iconImage.width / 2, BASE_WINDOW_HEIGHT / 3 - iconImage.height / 2);
		}

	}

	showHowtoPlay() {
		image(howtoplayImage, 0, 0);
	}

}

let mainMenu = new MainMenu();