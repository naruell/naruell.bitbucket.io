class World {

    constructor() {

        this.rollBacksLeft = MAX_TRY; // IMPORTANT

        this.roster = 0; // just decl

        this.castles = 0; // just decl

        this.shake = 0;
        this.shakeX = 0;
        this.shakeY = 0;

        this.thunder = 0;
        this.isThunder = false;

        //ground image
        this.groundImage = loadImage("Sprites/Backgrounds/ground.png");

        this.castles = [];

        this.initialize();

    }

    initialize() {

        this.roster = new Roster();

        this.shake = 0;
        this.shakeX = 0;
        this.shakeY = 0;

        this.thunder = 0;
        this.isThunder = false;

        let castleOneProperties = floor(random(3));
        switch (castleOneProperties) {
            case 0:
                castleOneProperties = CASTLE_PROPERTIES.sword;
                break;
            case 1:
                castleOneProperties = CASTLE_PROPERTIES.shield;
                break;
            case 2:
                castleOneProperties = CASTLE_PROPERTIES.bow;
                break;
        }
        let castleTwoProperties = floor(random(3));
        switch (castleTwoProperties) {
            case 0:
                castleTwoProperties = CASTLE_PROPERTIES.swordShield;
                break;
            case 1:
                castleTwoProperties = CASTLE_PROPERTIES.swordBow;
                break;
            case 2:
                castleTwoProperties = CASTLE_PROPERTIES.shieldBow;
                break;
        }
        let castleThreeProperties = floor(random(3));
        switch (castleThreeProperties) {
            case 0:
                castleThreeProperties = CASTLE_PROPERTIES.sword;
                break;
            case 1:
                castleThreeProperties = CASTLE_PROPERTIES.shield;
                break;
            case 2:
                castleThreeProperties = CASTLE_PROPERTIES.bow;
                break;
        }
        let castleFourProperties = floor(random(3));
        switch (castleFourProperties) {
            case 0:
                castleFourProperties = CASTLE_PROPERTIES.sword;
                break;
            case 1:
                castleFourProperties = CASTLE_PROPERTIES.shield;
                break;
            case 2:
                castleFourProperties = CASTLE_PROPERTIES.bow;
                break;
        }
        if (this.castles.length <= 0) {
            this.castles.push(new Castle(CASTLE_SIZE.small, 10, 12, castleOneProperties, 7, 0));
            this.castles.push(new Castle(CASTLE_SIZE.small, 30, 15, castleTwoProperties, 10, 1));
            this.castles.push(new Castle(CASTLE_SIZE.big, 50, 25, castleThreeProperties, 13));
            this.castles.push(new Castle(CASTLE_SIZE.giant, 75, 40, castleFourProperties, 15));
        }

        this.roster.currentRoster.push(new Hero("Fred", HERO_POSITION.back, 10, 1, "Sprites/Backgrounds/hero_1.png", default1BodyImage, fredFaceImage));
        this.roster.currentRoster.push(new Hero("Charlie", HERO_POSITION.center, 10, 1, "Sprites/Backgrounds/hero_2.png", default3BodyImage, charlieFaceImage));
        this.roster.currentRoster.push(new Hero("Guillian", HERO_POSITION.front, 10, 1, "Sprites/Backgrounds/hero_3.png", default2BodyImage, guillianFaceImage));

        // this.roster.currentRoster[1].health = 0;
        // this.roster.currentRoster[1].isAlive = false;

        // this.roster.currentRoster[0].health = 7;

        for (let castle of this.castles) {
            castle.initialize();
        }
    }

    update() {

        for (let castle of this.castles) {

            castle.update();

            if (castle.health > 0 &&
                castle.progressToShowOn < this.roster.progress &&
                this.roster.rosterState != ROSTER_STATE.battle &&
                this.roster.rosterState != ROSTER_STATE.rollback) {

                this.roster.rosterState = ROSTER_STATE.battle;
                this.roster.currentCastle = castle;

            }

        }

        this.roster.update();

    }

    draw() {



        push();
        translate(BASE_WINDOW_WIDTH / 2, BASE_WINDOW_HEIGHT / 2);
        rotate(-PI / 2 * (this.roster.progress / 75));
        image(moonImage, BASE_WINDOW_HEIGHT / 4, 0);
        pop();



        if (this.shake > 0) {

            this.shakeX = random(-this.shake, this.shake);
            this.shakeY = random(-this.shake, this.shake);

            this.shake -= game.deltaTime / 60;

        } else {

            this.shake = 0;

        }


        if (this.isThunder) {
            let tempCOLOR = COLOR_BACKGROUND
            COLOR_BACKGROUND = COLOR_IMAGES;
            COLOR_IMAGES = tempCOLOR;
            this.isThunder = false;
        } else {
            if (this.thunder > 0) {

                this.thunder -= game.deltaTime / 60;

            } else {

                COLOR_BACKGROUND = COLOR_BACKGROUND_DEFAULT;
                COLOR_IMAGES = COLOR_IMAGES_DEFAULT;

                this.thunder = 0;

            }
        }



        push();

        tint(COLOR_IMAGES);

        image(this.groundImage, -25 + this.shakeX, (BASE_WINDOW_HEIGHT / 2) - (this.groundImage.height / 2) + this.shakeY);

        for (let castle of this.castles) {

            let tempPosX = lerp(0, BASE_WINDOW_WIDTH, castle.progressToShowOn / 100);

            castle.draw(tempPosX + this.shakeX, (BASE_WINDOW_HEIGHT / 2) + this.shakeY, 0, 0, 0);

        }

        for (let hero of this.roster.currentRoster) {

            let tempPosX = lerp(0, BASE_WINDOW_WIDTH, this.roster.progress / 100) + hero.position * 10;

            hero.draw(tempPosX + this.shakeX, (BASE_WINDOW_HEIGHT / 2) + this.shakeY);

        }

        pop();



        if (game.gameState == GAME_STATE.mainGame)
            this.roster.drawBattleCharacterInfo();

        if (this.roster.rosterState == ROSTER_STATE.rest)
            this.roster.rest();



    }

}