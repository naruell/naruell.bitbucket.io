let rosterInterface =
{
	spareRelativePosX: 5,
	spareRelativePosY: 5,
	spareRelativeTextSize: 40,

	nameRelativePosX: 5,
	nameRelativePosY: 20,
	nameRelativeTextSize: 25,

	imageRelativePosX: 5,
	imageRelativePosY: 25,
	imageRelativeWidth: 150,
	imageRelativeHeight: 250,

	iconRelativePosX: 5,
	iconRelativePosY: 68,
	iconRelativeWidth: 50,
	iconRelativeHeight: 50,

	buttonUpRelativePosX: 5,
	buttonUpRelativePosY: 77,
	buttonUpRelativeWidth: 50,
	buttonUpRelativeHeight: 30,

	propertyRelativePosX: 5,
	propertyRelativePosY: 83,
	propertyRelativeWidth: 50,
	propertyRelativeHeight: 50,
	propertyRleativeTextSize: 48,

	buttonDownRelativePosX: 5,
	buttonDownRelativePosY: 92,
	buttonDownRelativeWidth: 50,
	buttonDownRelativeHeight: 30,

	buttonSelectRelativePosX: 14,
	buttonSelectRelativePosY: 90,
	buttonSelectRelativeWidth: 150,
	buttonSelectRelativeHeight: 30,

	buttonStartRelativePosX: 80,
	buttonStartRelativePosY: 5,
	buttonStartRelativeWidth: 150,
	buttonStartRelativeHeight: 50,
	buttonStartIntervalRelativeWidth : 200,

	intervalRelativeWidth: 250,

	upButtons: [],
	downButtons: [],
	selectButtons: [],
	startButton: []
}

function createRosterButtons() {
	for (let i = 0; i < 9; ++i) {
		rosterInterface.upButtons.push(createButton("▲"));
		rosterInterface.upButtons[i].hide();
	}

	for (let i = 0; i < 9; ++i) {
		rosterInterface.downButtons.push(createButton("▼"));
		rosterInterface.downButtons[i].hide();
	}

	for (let i = 0; i < 3; ++i) {
		rosterInterface.selectButtons.push(createButton("Select"));
		rosterInterface.selectButtons[i].hide();
		rosterInterface.selectButtons[i].style("font-family", "bloodcrow");
		rosterInterface.selectButtons[i].style("font-size", "25px");
	}

	rosterInterface.startButton.push(createButton("Go"));
	rosterInterface.startButton[0].hide();
	rosterInterface.startButton[0].style("font-family", "bloodcrow");
	rosterInterface.startButton[0].style("font-size", "40px");

	for(let i = 0; i < 3; ++i)
	{
		changeHeroPosition.selectButtons.push(createButton("Change"));
		changeHeroPosition.selectButtons[i].hide();
		changeHeroPosition.selectButtons[i].style("font-family", "bloodcrow");
		changeHeroPosition.selectButtons[i].style("font-size", "25px");
	}

	changeHeroPosition.confirmButton.push(createButton("Confirm"));
	changeHeroPosition.confirmButton[0].hide();
	changeHeroPosition.confirmButton[0].style("font-family", "bloodcrow");
	changeHeroPosition.confirmButton[0].style("font-size", "40px");

	mainMenuInterface.titleButtons.push(createButton("Start"));
	mainMenuInterface.titleButtons[mainMenuInterface.titleButtons.length - 1].hide();
	mainMenuInterface.titleButtons[mainMenuInterface.titleButtons.length - 1].style("font-family", "bloodcrow");
	mainMenuInterface.titleButtons[mainMenuInterface.titleButtons.length - 1].style("font-size", "40px");

	mainMenuInterface.titleButtons.push(createButton("How to Play"));
	mainMenuInterface.titleButtons[mainMenuInterface.titleButtons.length - 1].hide();
	mainMenuInterface.titleButtons[mainMenuInterface.titleButtons.length - 1].style("font-family", "bloodcrow");
	mainMenuInterface.titleButtons[mainMenuInterface.titleButtons.length - 1].style("font-size", "30px");

	mainMenuInterface.howToPlayButtons.push(createButton("Back"));
	mainMenuInterface.howToPlayButtons[mainMenuInterface.howToPlayButtons.length - 1].hide();
	mainMenuInterface.howToPlayButtons[mainMenuInterface.howToPlayButtons.length - 1].style("font-family", "bloodcrow");
	mainMenuInterface.howToPlayButtons[mainMenuInterface.howToPlayButtons.length - 1].style("font-size", "30px");
}

function hideRosterButtons()
{
	for(button of rosterInterface.upButtons)
	{
		button.hide();
	}

	for(button of rosterInterface.downButtons)
	{
		button.hide();
	}

	for(button of rosterInterface.selectButtons)
	{
		button.hide();
	}

	for(button of rosterInterface.startButton)
	{
		button.hide();
	}

	for(button of changeHeroPosition.selectButtons)
	{
		button.hide();
	}

	for(button of changeHeroPosition.confirmButton)
	{
		button.hide();
	}
}

let itemInterface =
{
	boxRelativePosX : 6,
	boxRelativePosY : 15,
	boxRelativeWidth : 182,
	boxRelativeHeight : 300,

	nameRelativePosX: 8,
	nameRelativePosY: 20,
	nameRelativeTextSize: 20,

	imageRelativePosX: 8,
	imageRelativePosY: 25,
	imageRelativeWidth: 150,
	imageRelativeHeight: 150,

	explanationRelativePosX: 8,
	explanationRelativePosY: 52,
	explanationRelativeTextSize: 15,
	explanationRelativeWidth: 150,
	explanationRelativeHeight: 200,

	intervalRelativeWidth: 250
}

let rosterBattleInterface = 
{
	imageRelativePosX: 17.5,
	imageRelativePosY: 57,
	imageRelativeWidth: 100,
	imageRelativeHeight: 100,

	healthRelativePosX : 14,
	healthRelativePosY : 76,
	healthRelativeWidth :150 ,
	healthRelativeHeight : 20,
	helathRelativeTextSize : 20,

	iconRelativePosX: 14,
	iconRelativePosY: 81,
	iconRelativeWidth: 50,
	iconRelativeHeight: 50,

	intervalRelativeWidth: 200
}

let changeHeroPosition = 
{
	isSelected: [false, false, false],
	selectButtons: [],
	confirmButton: [],

	buttonRelativePosX: 14,
	buttonRelativePosY: 90,
	buttonRelativeWidth: 150,
	buttonRelativeHeight: 30,

	buttonConfirmRelativePosX: 75,
	buttonConfirmRelativePosY: 5,
	buttonConfirmRelativeWidth: 170,
	buttonConfirmRelativeHeight: 50,

	intervalRelativeWidth: 200
}

let mainMenuInterface = 
{
	buttonTitleRelativePosX: 40,
	buttonTitleRelativePosY: 50,
	buttonTitleRelativeWidth: 150,
	buttonTitleRelativeHeight: 50,

	buttonBackRelativePosX: 80,
	buttonBackRelativePosY: 80,
	buttonBackRelativeWidth: 100,
	buttonBackRelativeHeight: 40,

	buttonTitleRelativeInterval: 100,

	titleButtons:[],
	howToPlayButtons:[]
}