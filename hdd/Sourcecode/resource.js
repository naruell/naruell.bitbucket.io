let backgroundMusic;
let hitSounds = [];
let sinkSound;

let swordIconImage;
let sword1Image;
let sword2Image;
let sword3Image;
let sword4Image;
let swordAura;

let shieldIconImage;
let shield1Image;
let shield2Image;
let shield3Image;
let shield4Image;
let shieldAura;

let bowIconImage;
let bow1Image;
let bow2Image;
let bow3Image;
let bow4Image;
let bowAura;

let auraImage;

let fredFaceImage;
let charlieFaceImage;
let guillianFaceImage;
let default1BodyImage;
let default2BodyImage;
let default3BodyImage;
let swordArmImage;
let shieldArmImage;
let archerArmImage;
let defaultArmImage;
let swordshieldArmImage;
let shieldbowArmImage;
let swordarcherArmImage;
let honjongArmImage;

let arrowItemImage;
let stoneItemImage;
let yoroiItemImage;
let potionItemImage
let potion_mysticItemImage;
let potion_holyItemImage;

let moonImage;

let battleImage;
let deathImage;

let bloodcrowFont;

let badImage;
let goodImage;

let iconImage;
let howtoplayImage;

function preload() {

	backgroundMusic = loadSound("Audio/BGM/crowdroar.wav");

	hitSounds[0] = loadSound("Audio/SE/hit1.wav");
	hitSounds[1] = loadSound("Audio/SE/hit2.wav");
	hitSounds[2] = loadSound("Audio/SE/hit3.wav");

	sinkSound = loadSound("Audio/SE/sink.wav");

	moonImage = loadImage("Sprites/Backgrounds/moon.png");

	badImage = loadImage("Sprites/Backgrounds/bad.png");
	goodImage = loadImage("Sprites/Interface/lines.png");

	iconImage = loadImage("Sprites/Interface/icon.png");

	howtoplayImage = loadImage("Sprites/Interface/HTP.png");

	swordIconImage = loadImage("Sprites/Interface/swordIcon.png");
	sword1Image = loadImage("Sprites/Interface/sword1.png");
	sword2Image = loadImage("Sprites/Interface/sword2.png");
	sword3Image = loadImage("Sprites/Interface/sword3.png");
	sword4Image = loadImage("Sprites/Interface/sword4.png");

	shieldIconImage = loadImage("Sprites/Interface/shieldIcon.png");
	shield1Image = loadImage("Sprites/Interface/shield1.png");
	shield2Image = loadImage("Sprites/Interface/shield2.png");
	shield3Image = loadImage("Sprites/Interface/shield3.png");
	shield4Image = loadImage("Sprites/Interface/shield4.png");

	bowIconImage = loadImage("Sprites/Interface/bowIcon.png");
	bow1Image = loadImage("Sprites/Interface/bow1.png");
	bow2Image = loadImage("Sprites/Interface/bow2.png");
	bow3Image = loadImage("Sprites/Interface/bow3.png");
	bow4Image = loadImage("Sprites/Interface/bow4.png");

	fredFaceImage = loadImage("Sprites/Characters/fredFace.png");
	charlieFaceImage = loadImage("Sprites/Characters/charlieFace.png");
	guillianFaceImage = loadImage("Sprites/Characters/guillianFace.png");
	default1BodyImage = loadImage("Sprites/Characters/default1Body.png");
	default2BodyImage = loadImage("Sprites/Characters/default2Body.png");
	default3BodyImage = loadImage("Sprites/Characters/default3Body.png");
	defaultArmImage = loadImage("Sprites/Characters/defaultArm.png")
	swordArmImage = loadImage("Sprites/Characters/swordArm.png");
	shieldArmImage = loadImage("Sprites/Characters/shieldArm.png");
	archerArmImage = loadImage("Sprites/Characters/archerArm.png");
	swordshieldArmImage = loadImage("Sprites/Characters/swordshieldArm.png");
	shieldbowArmImage = loadImage("Sprites/Characters/shieldbowArm.png");
	swordarcherArmImage = loadImage("Sprites/Characters/swordarcherArm.png");
	honjongArmImage = loadImage("Sprites/Characters/honjongArm.png")


	arrowItemImage = loadImage("Sprites/System/arrowItem.png");
	stoneItemImage = loadImage("Sprites/System/stoneItem.png");
	yoroiItemImage = loadImage("Sprites/System/yoroiItem.png");
	potionItemImage = loadImage("Sprites/System/potionItem.png");
	potion_mysticItemImage = loadImage("Sprites/System/potion_mysticItem.png");
	potion_holyItemImage = loadImage("Sprites/System/potion_holyItem.png");

	battleImage = loadImage("Sprites/Interface/battle.png");
	deathImage = loadImage("Sprites/Interface/death.png");

	bloodcrowFont = loadFont("Fonts/bloodcrow.ttf");

	swordAura = [sword1Image, sword2Image, sword3Image, sword4Image];
	shieldAura = [shield1Image, shield2Image, shield3Image, shield4Image];
	bowAura = [bow1Image, bow2Image, bow3Image, bow4Image];
	auraImage = [swordAura, shieldAura, bowAura];
}