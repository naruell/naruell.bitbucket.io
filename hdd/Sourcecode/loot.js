// Ex) let smallPotion = new loot("Potion(S)", LOOT_RARITY.makeshift, potionEffect)  
class loot {
	constructor(name, explanation, rarity, lootEffect, itemImage, imageWidth, imageHeight) {
		this.name = name;
		this.explanation = explanation;
		this.rarity = rarity;
		this.lootEffect = lootEffect;
		this.itemImage = itemImage;
		this.auraImage;
		this.imageWidth = imageWidth;
		this.imageHeight = imageHeight;
		this.isUsed = false;

		// 이미지 로드/////////////////////////////
		// 레어리티 로드///////////////////////////
		switch (rarity) {
			case LOOT_RARITY.makeshift:
				this.auraImage;
				break;

			case LOOT_RARITY.standard:
				this.auraImage;
				break;

			case LOOT_RARITY.quality:
				this.auraImage;
				break;
		}
		///////////////////////////////////////////
	}

	use() {
		if (this.isUsed == false) {
			this.isUsed = this.lootEffect(this.rarity);
		}

		if (this.isUsed == true) {
			this.isUsed = false;
			return true;
		}
		else {
			return false;
		}

	}

	draw(index) {
		let INTERFACE = itemInterface;
		let correction = index * INTERFACE.intervalRelativeWidth;

		push();

		fill(COLOR_IMAGES);
		stroke(COLOR_BACKGROUND);
		strokeWeight(5);
		rect(correction + INTERFACE.boxRelativePosX * width / 100,
			INTERFACE.boxRelativePosY * height / 100,
			INTERFACE.boxRelativeWidth,
			INTERFACE.boxRelativeHeight,
			10);

		noStroke();
		fill(COLOR_BACKGROUND);
		textSize(INTERFACE.nameRelativeTextSize);
		text(this.name,
			correction + INTERFACE.nameRelativePosX * width / 100,
			INTERFACE.nameRelativePosY * height / 100);

		/* Draw Loot Image Here!!!!!!! *//////////////////////
		rect(correction + INTERFACE.imageRelativePosX * width / 100,
			INTERFACE.imageRelativePosY * height / 100,
			INTERFACE.imageRelativeWidth,
			INTERFACE.imageRelativeHeight);

		image(this.itemImage,
			correction + INTERFACE.imageRelativePosX * width / 100,
			INTERFACE.imageRelativePosY * height / 100,
			this.imageWidth,
			this.imageHeight);
		//////////////////////////////////////////////////////

		textSize(INTERFACE.explanationRelativeTextSize);
		text(this.explanation,
			correction + INTERFACE.explanationRelativePosX * width / 100,
			INTERFACE.explanationRelativePosY * height / 100,
			INTERFACE.explanationRelativeWidth,
			INTERFACE.explanationRelativeHeight);
		pop();
	}
}

// 포션 회복 수치 변경 (현재 3, 5, 7)
HPPotionEffect = function (rarity) {
	let selectedHeroIndex = game.world.roster.selectHero("ALIVE");

	if (selectedHeroIndex != undefined) {
		let selectedHero = game.world.roster.currentRoster[selectedHeroIndex];
		switch (rarity) {
			case LOOT_RARITY.makeshift:
				selectedHero.health += 3;
				if (selectedHero.health >= selectedHero.maxHealth) {
					selectedHero.health = selectedHero.maxHealth;
				}
				break;

			case LOOT_RARITY.standard:
				selectedHero.health += 5;
				if (selectedHero.health >= selectedHero.maxHealth) {
					selectedHero.health = selectedHero.maxHealth;
				}
				break;

			case LOOT_RARITY.quality:
				selectedHero.health += 7;
				if (selectedHero.health >= selectedHero.maxHealth) {
					selectedHero.health = selectedHero.maxHealth;
				}
				break;
		}

		return true;
	}

	else {
		return false;
	}
}

let makeshiftHPPotion;

let standardHPPotion;

let qualityHPPotion;

fireArrowEffect = function (rarity) {
	let selectedHeroIndex = game.world.roster.selectHero("ALIVE");

	if (selectedHeroIndex != undefined) {
		let selectedHero = game.world.roster.currentRoster[selectedHeroIndex];
		switch (rarity) {
			case LOOT_RARITY.makeshift:
				selectedHero.properties[2].amount += 1;
				break;

			case LOOT_RARITY.standard:
				selectedHero.properties[2].amount += 2;
				break;

			case LOOT_RARITY.quality:
				selectedHero.properties[2].amount += 3;
				break;
		}

		return true;
	}

	else {
		return false;
	}
}

let makeshiftFireArrow;

let standardFireArrow;

let qualityFireArrow;

stoneEffect = function (rarity) {
	let selectedHeroIndex = game.world.roster.selectHero("ALIVE");

	if (selectedHeroIndex != undefined) {
		let selectedHero = game.world.roster.currentRoster[selectedHeroIndex];
		switch (rarity) {
			case LOOT_RARITY.makeshift:
				selectedHero.properties[0].amount += 1;
				break;

			case LOOT_RARITY.standard:
				selectedHero.properties[0].amount += 2;
				break;

			case LOOT_RARITY.quality:
				selectedHero.properties[0].amount += 3;
				break;
		}

		return true;
	}

	else {
		return false;
	}
}

let makeshiftStone;

let standardStone;

let qualityStone;

yoroiEffect = function (rarity) {
	let selectedHeroIndex = game.world.roster.selectHero("ALIVE");

	if (selectedHeroIndex != undefined) {
		let selectedHero = game.world.roster.currentRoster[selectedHeroIndex];
		switch (rarity) {
			case LOOT_RARITY.makeshift:
				selectedHero.properties[1].amount += 1;
				break;

			case LOOT_RARITY.standard:
				selectedHero.properties[1].amount += 2;
				break;

			case LOOT_RARITY.quality:
				selectedHero.properties[1].amount += 3;
				break;
		}

		return true;
	}

	else {
		return false;
	}
}

let makeshiftYoroi;

let standardYoroi;

let qualityYoroi;

mysticPotionEffect = function (rarity) {
	let selectedHeroIndex = game.world.roster.selectHero("ALIVE");

	if (selectedHeroIndex != undefined) {
		let selectedHero = game.world.roster.currentRoster[selectedHeroIndex];
		let propertyIndex = floor(random(0, 3));
		let propertyPlus;
		switch (rarity) {
			case LOOT_RARITY.makeshift:
				propertyPlus = floor(random(1, 3));
				selectedHero.properties[propertyIndex].amount += propertyPlus;
				break;

			case LOOT_RARITY.standard:
				propertyPlus = floor(random(2, 4));
				selectedHero.properties[propertyIndex].amount += propertyPlus;
				break;

			case LOOT_RARITY.quality:
				propertyPlus = floor(random(3, 5));
				selectedHero.properties[propertyIndex].amount += propertyPlus;
				break;
		}

		return true;
	}

	else {
		return false;
	}
}

let makeshiftMysticPotion;

let standardMysticPotion;

let qualityMysticPotion;

holyWaterEffect = function (rarity) {
	let selectedHeroIndex = game.world.roster.selectHero("DEAD");

	if (selectedHeroIndex != undefined) {
		let selectedHero = game.world.roster.currentRoster[selectedHeroIndex];
		switch (rarity) {
			case LOOT_RARITY.makeshift:
				selectedHero.isAlive = true;
				selectedHero.health = 1;
				break;

			case LOOT_RARITY.standard:
				selectedHero.isAlive = true;
				selectedHero.health = floor(selectedHero.maxHealth * 0.3);
				break;

			case LOOT_RARITY.quality:
				selectedHero.isAlive = true;
				selectedHero.health = floor(selectedHero.maxHealth * 0.5);
				break;
		}

		return true;
	}

	else {
		return false;
	}
}

let makeshiftHolyWater;

let standardHolyWater;

let qualityHolyWater;

function generateItem() {
	makeshiftHPPotion =
		new loot("HP Potion",
			"Target hero gains 3 HP",
			LOOT_RARITY.makeshift,
			HPPotionEffect,
			potionItemImage, 150, 150);

	standardHPPotion =
		new loot("HP Potion",
			"Target hero gains 5 HP",
			LOOT_RARITY.standard,
			HPPotionEffect,
			potionItemImage, 150, 150);

	qualityHPPotion =
		new loot("HP Potion",
			"Target hero gains 7 HP",
			LOOT_RARITY.quality,
			HPPotionEffect,
			potionItemImage, 150, 150);

	makeshiftFireArrow =
		new loot("Fire Arrow",
			"Target hero gets 1 \"Bow\" skillpoints",
			LOOT_RARITY.makeshift,
			fireArrowEffect,
			arrowItemImage, 150, 150);

	standardFireArrow =
		new loot("Fire Arrow",
			"Target hero gets 2 \"Bow\" skillpoints",
			LOOT_RARITY.standard,
			fireArrowEffect,
			arrowItemImage, 150, 150);

	qualityFireArrow =
		new loot("Fire Arrow",
			"Target hero gets 3 \"Bow\" skillpoints",
			LOOT_RARITY.quality,
			fireArrowEffect,
			arrowItemImage, 150, 150);

	makeshiftStone =
		new loot("Hone",
			"Target hero gets 1 \"Sword\" skillpoints",
			LOOT_RARITY.makeshift,
			stoneEffect,
			stoneItemImage, 150, 150);

	standardStone =
		new loot("Hone",
			"Target hero gets 2 \"Sword\" skillpoints",
			LOOT_RARITY.standard,
			stoneEffect,
			stoneItemImage, 150, 150);

	qualityStone =
		new loot("Hone",
			"Target hero gets 3 \"Sword\" skillpoints",
			LOOT_RARITY.quality,
			stoneEffect,
			stoneItemImage, 150, 150);

	makeshiftYoroi =
		new loot("Yoroi",
			"Target hero gets 1 \"Shield\" skillpoints",
			LOOT_RARITY.makeshift,
			yoroiEffect,
			yoroiItemImage, 150, 150);

	standardYoroi =
		new loot("Yoroi",
			"Target hero gets 2 \"Shield\" skillpoints",
			LOOT_RARITY.standard,
			yoroiEffect,
			yoroiItemImage, 150, 150);

	qualityYoroi =
		new loot("Yoroi",
			"Target hero gets 3 \"Shield\" skillpoints",
			LOOT_RARITY.quality,
			yoroiEffect,
			yoroiItemImage, 150, 150);

	makeshiftMysticPotion =
		new loot("Mystic Potion",
			"Target hero gets 1~2 \"Random\" skillpoints",
			LOOT_RARITY.makeshift,
			mysticPotionEffect,
			potion_mysticItemImage, 150, 150);

	standardMysticPotion =
		new loot("Mystic Potion",
			"Target hero gets 2~3 \"Random\" skillpoints",
			LOOT_RARITY.standard,
			mysticPotionEffect,
			potion_mysticItemImage, 150, 150);

	qualityMysticPotion =
		new loot("Mystic Potion",
			"Target hero gets 3~4 \"Random\" skillpoints",
			LOOT_RARITY.quality,
			mysticPotionEffect,
			potion_mysticItemImage, 150, 150);

	makeshiftHolyWater =
		new loot("Holy Water",
			"Revive target dead hero.\nInstant 1 HP healing",
			LOOT_RARITY.makeshift,
			holyWaterEffect,
			potion_holyItemImage, 150, 150);

	standardHolyWater =
		new loot("Holy Water",
			"Revive target dead hero.\nInstant MAX HP x 30% healing",
			LOOT_RARITY.standard,
			holyWaterEffect,
			potion_holyItemImage, 150, 150);

	qualityHolyWater =
		new loot("Holy Water",
			"Revive target dead hero.\nInstant MAX HP x 50% healing",
			LOOT_RARITY.quality,
			holyWaterEffect,
			potion_holyItemImage, 150, 150);

	lootList = [[makeshiftHPPotion, standardHPPotion, qualityHPPotion],
	[makeshiftFireArrow, standardFireArrow, qualityFireArrow],
	[makeshiftStone, standardStone, qualityStone],
	[makeshiftYoroi, standardYoroi, qualityYoroi],
	[makeshiftMysticPotion, standardMysticPotion, qualityMysticPotion],
	[makeshiftHolyWater, standardHolyWater, qualityHolyWater]];
}

let lootList