class Castle {

	constructor(size, progressToShowOn, health, properties, damage, type) {

		this.yOffset = 0;
		this.sink = 0;

		this.size = size;

		this.progressToShowOn = progressToShowOn;

		this.health = health;
		this.maxHealth = health;

		this.properties = properties; //TODO

		this.damage = damage;

		this.castleImage = 0;
		this.castleHolesImage = 0;

		this.shake = 0;
		this.shakeX = 0;
		this.shakeY = 0;

		switch (this.size) { //Randomize later

			case CASTLE_SIZE.small:
				switch (type) {
					case 0:
						this.castleImage = loadImage("Sprites/Backgrounds/castle_small_1.png");
						this.castleHolesImage = loadImage("Sprites/Backgrounds/castle_small_1_holes.png");
						break;
					case 1:
						this.castleImage = loadImage("Sprites/Backgrounds/castle_small_2.png");
						this.castleHolesImage = loadImage("Sprites/Backgrounds/castle_small_2_holes.png");
						break;
				}
				this.yOffset = -this.castleImage.height;
				break;

			case CASTLE_SIZE.big:
				this.castleImage = loadImage("Sprites/Backgrounds/castle_big_1.png");
				this.castleHolesImage = loadImage("Sprites/Backgrounds/castle_big_1_holes.png");
				this.yOffset = -this.castleImage.height;
				break;

			case CASTLE_SIZE.giant:
				this.castleImage = loadImage("Sprites/Backgrounds/castle_giant_1.png");
				this.castleHolesImage = loadImage("Sprites/Backgrounds/castle_giant_1_holes.png");
				this.yOffset = -this.castleImage.height;
				break;

		}
		this.initialize();
	}
	
	initialize(){
		this.yOffset = 0;
		this.sink = 0;
		this.health = this.maxHealth;
	}

	update() {

		if (this.shake > 0) {

			this.shakeX = random(-this.shake, this.shake);
			this.shakeY = random(-this.shake, this.shake);

			if (game.world.roster.rosterState == ROSTER_STATE.rollback) {
				this.shake -= game.deltaTime / 2000;
			} else {
				this.shake -= game.deltaTime / 3000;
			}

		} else {

			this.shake = 0;

		}

		if (game.world.roster.rosterState == ROSTER_STATE.rollback) {

			if (this.sink > 0)
				this.sink -= game.deltaTime / 20;

		} else {

			if (this.health <= 0) {
				if (this.sink < this.castleImage.height)
					this.sink += game.deltaTime / 60;
			}

		}

	}

	draw(posX, posY) {

		push();
		if (game.world.thunder > 0) {
			if (game.world.roster.currentCastle.progressToShowOn == this.progressToShowOn) {
				tint(random(0, 255), random(0, 255), random(0, 255))
			} else {
				tint(COLOR_IMAGES);
			}
		} else {
			tint(COLOR_BACKGROUND);
		}
		image(this.castleHolesImage, posX + this.shakeX, posY + this.shakeY - this.castleImage.height);
		tint(COLOR_IMAGES);
		image(this.castleImage, posX + this.shakeX, posY + this.shakeY + this.sink - this.castleImage.height);
		pop();

	}

}