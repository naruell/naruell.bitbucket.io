class Hero {

	constructor(name, position, propertyLeft, level, imagePath, bodyImage, faceImage) {

		this.isAlive = true;



		this.maxHealth = 20; // 20 is temp value
		this.health = 20;	// 디자인 나오는 대로 수정해야됨!!!

		this.name = name;

		this.position = position;



		this.progress = 0;	// 월드에서 그리는 위치, 0~100 사이 == 진행도



		this.heroImage = loadImage(imagePath);
		this.bodyImage = bodyImage;
		this.faceImage = faceImage;


		this.walkNoise = random(1000, 2000);
		this.walkY = 0;



		this.properties =
			[{ name: "sword", amount: 0 },
			{ name: "shield", amount: 0 },
			{ name: "bow", amount: 0 }];



		this.propertyLeft = propertyLeft;

		this.highestProperties = [];

		this.propertyAmount = 0; //Is this needed?



		this.level = 1 | level;

		this.isHeroFighting = false;

		this.updateProperties();

	}

	updateProperties() {

		let highestProperty = 0;
		this.highestProperties = [];
		this.propertyAmount = 0;

		for (let i = 0; i < this.properties.length; ++i) {
			if (this.properties[i].amount >= highestProperty) { highestProperty = this.properties[i].amount; }
			this.propertyAmount += this.properties[i].amount;
		}

		for (let i = 0; i < this.properties.length; ++i) {
			if (this.properties[i].amount == highestProperty) { this.highestProperties.push(this.properties[i].name); }
		}

	}

	update() {

		if (this.isAlive) {

			switch (game.world.roster.rosterState) {

				case ROSTER_STATE.walk:
					this.walkY = noise(millis() / this.walkNoise) * 6;
					break;
				case ROSTER_STATE.battle:

					break;
				case ROSTER_STATE.rest:
					this.walkY = noise(millis() / this.walkNoise) * 2;
					break;

			}

		} else {



		}

	}

	draw(posX, posY) {

		if (this.isAlive) {

			image(this.heroImage, posX, posY - (this.heroImage.height / 2) - this.walkY);

		} else { //if dead



		}

	}

}